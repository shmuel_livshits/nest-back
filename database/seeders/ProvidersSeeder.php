<?php

namespace Database\Seeders;

use App\Models\Provider;
use Illuminate\Database\Seeder;

class ProvidersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $rawItems = $client->get('providers')->getBody()->getContents();
        $rawItems=json_decode($rawItems);
        foreach ($rawItems as $rawItem){
            $name=[];
            if ($rawItem->name_ru!==null){
                $name['ru']=$rawItem->name_ru;
            }


            $readyItem = new Provider();
            $readyItem->id=$rawItem->id;
            $readyItem->name=$name;
            $readyItem->isOwnImport=$rawItem->isOwnImport;
            $readyItem->save();
        }
    }
}
