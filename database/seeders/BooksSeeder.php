<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $books_categories = $client->get('books_category')->getBody()->getContents();
        $books_categories = collect(json_decode($books_categories));
        $books = $client->get('books')->getBody()->getContents();
        $books = json_decode($books);
        foreach ($books as $rawItem) {
            $categoryId = $this->saveCategory($books_categories->where('id',$rawItem->books_category_id)->first());
            $name = [];
            if ($rawItem->name_ru !== null) {
                $name['ru'] = $rawItem->name_ru;
            }
            if ($rawItem->name_ua != null) {
                $name['ua'] = $rawItem->name_ua;
            }
            $pageTitle = [];
            if ($rawItem->page_title_ru != null) {
                $pageTitle['ru'] = $rawItem->page_title_ru;
            }
            if ($rawItem->page_title_ua != null) {
                $pageTitle['ua'] = $rawItem->page_title_ua;
            }
            $pageDescription = [];
            if ($rawItem->page_description_ru != null) {
                $pageDescription['ru'] = $rawItem->page_description_ru;
            }
            if ($rawItem->page_description_ua != null) {
                $pageDescription['ua'] = $rawItem->page_description_ua;
            }
            $description = [];
            if ($rawItem->description_ru != null) {
                $description['ru'] = $rawItem->description_ru;
            }
            if ($rawItem->description_ua != null) {
                $description['ua'] = $rawItem->description_ua;
            }

            $readyItem = new Product();
            $readyItem->name = $name;
            $readyItem->page_title = $pageTitle;
            $readyItem->page_description = $pageDescription;
            $readyItem->page_slug = $rawItem->page_slug;
            $readyItem->product_type = Product::BOOK_TYPE;
            $readyItem->barcode = $rawItem->barcode;
            $readyItem->sku = $rawItem->sku;
            $readyItem->price = $rawItem->price;
            $readyItem->old_price = $rawItem->old_price;
            $readyItem->end_sale = $rawItem->end_sale;
            $readyItem->quantity = $rawItem->quantity;
            $readyItem->isPresent = false;
            $readyItem->seller = $rawItem->seller;
            $readyItem->translit = $rawItem->translit;
            $readyItem->popularity = $rawItem->popularity;
            $readyItem->image = $rawItem->image;
            $readyItem->description = $description;
            $readyItem->publish_year=$rawItem->publish_year;
            $readyItem->show_in_google_merchant = $rawItem->show_in_google_merchant;
            $readyItem->show_in_search_io = $rawItem->show_in_search_io;
            $readyItem->show_in_allo = $rawItem->show_in_allo;
            $readyItem->show_on_site = $rawItem->show_on_site;
            $readyItem->save();
            $readyItem->categories()->attach($categoryId);
        }
    }

    private function saveCategory($rawCategory)
    {
        $category=Category::whereRaw("JSON_EXTRACT(name, '$.ru') = '".$rawCategory->name_ru."'")->first();
        if ($category===null){
            $name = [];
            if ($rawCategory->name_ru !== null) {
                $name['ru'] = $rawCategory->name_ru;
            }
            if ($rawCategory->name_ua != null) {
                $name['ua'] = $rawCategory->name_ua;
            }
            $pageTitle = [];
            if ($rawCategory->page_title_ru != null) {
                $pageTitle['ru'] = $rawCategory->page_title_ru;
            }
            if ($rawCategory->page_title_ua != null) {
                $pageTitle['ua'] = $rawCategory->page_title_ua;
            }
            $pageDescription = [];
            if ($rawCategory->page_description_ru != null) {
                $pageDescription['ru'] = $rawCategory->page_description_ru;
            }
            if ($rawCategory->page_description_ua != null) {
                $pageDescription['ua'] = $rawCategory->page_description_ua;
            }
            $readyItem = new Category();
            $readyItem->name = $name;
            $readyItem->page_title = $pageTitle;
            $readyItem->page_description = $pageDescription;
            $readyItem->page_slug = $rawCategory->page_slug;
            $readyItem->save();

            return $readyItem->id;
        }


        return $category->id;

    }
}
