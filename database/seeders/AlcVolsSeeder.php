<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class AlcVolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $rawItems = $client->get('alc_vol')->getBody()->getContents();
        $rawItems=json_decode($rawItems);
        foreach ($rawItems as $skuArr){
            foreach ($skuArr as $sku){
                $prod=Product::where('sku',$sku)->first();
                foreach ($skuArr as $sku2){
                    if ($sku!==$sku2){
                        $prod2=Product::where('sku',$sku2)->first();
                        try{
                            $prod->volums()->attach($prod2->id);
                        }
                        catch (\Exception $exception){
                            dump($exception);
                        }
                    }

                }
            }

        }
    }
}
