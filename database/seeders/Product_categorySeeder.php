<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class Product_categorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $rawItems = $client->get('alc_products_categories')->getBody()->getContents();
        $rawItems=json_decode($rawItems);
        foreach ($rawItems as $key=>$id){
            $productId=intval($key);
            $alcoholProduct=Product::find($productId);
            $alcoholProduct->categories()->attach($id);

        }
    }
}
