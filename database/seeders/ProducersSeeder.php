<?php

namespace Database\Seeders;

use App\Models\Producer;
use Illuminate\Database\Seeder;

class ProducersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $rawItems = $client->get('producers')->getBody()->getContents();
        $rawItems=json_decode($rawItems);
        foreach ($rawItems as $rawItem){
            $name=[];
            if ($rawItem->name_ru!==null){
                $name['ru']=$rawItem->name_ru;
            }
            if ($rawItem->name_ua!=null){
                $name['ua']=$rawItem->name_ua;
            }
            $pageTitle=[];
            if ($rawItem->page_title_ru!=null){
                $pageTitle['ru']=$rawItem->page_title_ru;
            }if ($rawItem->page_title_ua!=null){
                $pageTitle['ua']=$rawItem->page_title_ua;
            }
            $pageDescription=[];
            if ($rawItem->page_description_ru!=null){
                $pageDescription['ru']=$rawItem->page_description_ru;
            }if ($rawItem->page_description_ua!=null){
                $pageDescription['ua']=$rawItem->page_description_ua;
            }
            $description=[];
            if ($rawItem->description_ru!=null){
                $description['ru']=$rawItem->description_ru;
            }if ($rawItem->description_ua!=null){
                $description['ua']=$rawItem->description_ua;
            }

            $readyItem = new Producer();
            $readyItem->id=$rawItem->id;
            $readyItem->name=$name;
            $readyItem->page_title=$pageTitle;
            $readyItem->page_description=$pageDescription;
            $readyItem->page_slug=$rawItem->page_slug;
            $readyItem->logo=$rawItem->logo_url;
            $readyItem->cover=$rawItem->img_url;
            $readyItem->description=$description;
            $readyItem->provider_id =$rawItem->provider_id ;
            $readyItem->save();
        }
    }
}
