<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductCombinationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $rawItems = $client->get('alc_products_combinations')->getBody()->getContents();
        $rawItems=json_decode($rawItems);
        foreach ($rawItems as $key=>$idsArr){
            $productId=intval($key);
            $alcoholProduct=Product::find($productId);
            foreach ($idsArr as $id){
                $alcoholProduct->combinations()->attach($id);
            }

        }
    }
}
