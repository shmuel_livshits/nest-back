<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class Alcohol_productseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $rawItems = $client->get('alcoholProducts')->getBody()->getContents();
        $rawItems=json_decode($rawItems);
        foreach ($rawItems as $rawItem){
            $name=[];
            if ($rawItem->name_ru!==null){
                $name['ru']=$rawItem->name_ru;
            }
            if ($rawItem->name_ua!=null){
                $name['ua']=$rawItem->name_ua;
            }
            $pageTitle=[];
            if ($rawItem->page_title_ru!=null){
                $pageTitle['ru']=$rawItem->page_title_ru;
            }if ($rawItem->page_title_ua!=null){
                $pageTitle['ua']=$rawItem->page_title_ua;
            }
            $pageDescription=[];
            if ($rawItem->page_description_ru!=null){
                $pageDescription['ru']=$rawItem->page_description_ru;
            }
            if ($rawItem->page_description_ua!=null){
                $pageDescription['ua']=$rawItem->page_description_ua;
            }
            $description=[];
            if ($rawItem->description_ru!=null){
                $description['ru']=$rawItem->description_ru;
            }if ($rawItem->description_ua!=null){
                $description['ua']=$rawItem->description_ua;
            }
            $sommeliers_note=[];
            if ($rawItem->sommeliers_note_ru!=null){
                $sommeliers_note['ru']=$rawItem->sommeliers_note_ru;
            }if ($rawItem->sommeliers_note_ua!=null){
                $sommeliers_note['ua']=$rawItem->sommeliers_note_ua;
            }
            $aging_type=[];
            if ($rawItem->aging_type!=null){
                $aging_type['ru']=$rawItem->aging_type;
            }
            if ($rawItem->aging_type_ua!=null){
                $aging_type['ua']=$rawItem->aging_type_ua;
            }
            $readyItem = new Product();
            $readyItem->id=$rawItem->id;
            $readyItem->name=$name;
            $readyItem->page_title=$pageTitle;
            $readyItem->page_description=$pageDescription;
            $readyItem->page_slug=$rawItem->page_slug;
            $readyItem->product_type = Product::ALCOHOL_TYPE;
            $readyItem->barcode = $rawItem->barcode ;
            $readyItem->sku = $rawItem->sku ;
            $readyItem->volume = $rawItem->volume ;
            $readyItem->price = $rawItem->price ;
            $readyItem->old_price = $rawItem->old_price ;
            $readyItem->end_sale = $rawItem->end_sale ;
            $readyItem->whole_price = $rawItem->whole_price ;
            $readyItem->whole_quantity = $rawItem->whole_quantity ;
            $readyItem->quantity = $rawItem->quantity ;
            $readyItem->quantity_in_pack = $rawItem->quantity_in_pack ;
            $readyItem->isPresent = $rawItem->isPresent ;
            $readyItem->seller = $rawItem->seller ;
            $readyItem->translit = $rawItem->translit ;
            $readyItem->popularity = $rawItem->popularity ;
            $readyItem->image = $rawItem->image ;
            $readyItem->brand_id  = $rawItem->brand_id  ;
            $readyItem->region_id  = $rawItem->region_id  ;
            $readyItem->country_id  = $rawItem->country_id  ;
            $readyItem->description = $description ;
            $readyItem->sommeliers_note = $sommeliers_note ;
            $readyItem->degree = $rawItem->degree ;
            $readyItem->marking_id  = $rawItem->marking_id  ;
            $readyItem->pack_id  = $rawItem->pack_id  ;
            $readyItem->alcohol_style_id  = $rawItem->alcohol_style_id  ;
            $readyItem->alcohol_aging = $rawItem->alcohol_aging ;
            $readyItem->aging_type = $rawItem->aging_type ;
            $readyItem->harvest_year = $rawItem->harvest_year ;
            $readyItem->serving_temperature_from = $rawItem->serving_temperature_from ;
            $readyItem->serving_temperature_to = $rawItem->serving_temperature_to ;
            $readyItem->wine_type_id  = $rawItem->alcohol_type_id  ;
            $readyItem->show_in_google_merchant = $rawItem->show_in_google_merchant ;
            $readyItem->show_in_search_io = $rawItem->show_in_search_io ;
            $readyItem->show_in_allo = $rawItem->show_in_allo ;
            $readyItem->show_on_site = $rawItem->show_on_site ;
            $readyItem->save();
        }
    }
}
