<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;

class accessoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $dishes_categories = $client->get('dishes_category')->getBody()->getContents();
        $dishes_categories = collect(json_decode($dishes_categories));
        $dishes = $client->get('dishes')->getBody()->getContents();
        $dishes = json_decode($dishes);
        foreach ($dishes as $rawItem) {
            $categoryId = $this->saveCategory($dishes_categories->where('id',$rawItem->dishes_category_id)->first());
            $name = [];
            if ($rawItem->name_ru !== null) {
                $name['ru'] = $rawItem->name_ru;
            }
            if ($rawItem->name_ua != null) {
                $name['ua'] = $rawItem->name_ua;
            }
            $pageTitle = [];
            if ($rawItem->page_title_ru != null) {
                $pageTitle['ru'] = $rawItem->page_title_ru;
            }
            if ($rawItem->page_title_ua != null) {
                $pageTitle['ua'] = $rawItem->page_title_ua;
            }
            $pageDescription = [];
            if ($rawItem->page_description_ru != null) {
                $pageDescription['ru'] = $rawItem->page_description_ru;
            }
            if ($rawItem->page_description_ua != null) {
                $pageDescription['ua'] = $rawItem->page_description_ua;
            }
            $description = [];
            if ($rawItem->description_ru != null) {
                $description['ru'] = $rawItem->description_ru;
            }
            if ($rawItem->description_ua != null) {
                $description['ua'] = $rawItem->description_ua;
            }
            $sommeliers_note = [];
            if ($rawItem->sommeliers_note_ru != null) {
                $sommeliers_note['ru'] = $rawItem->sommeliers_note_ru;
            }
            if ($rawItem->sommeliers_note_ua != null) {
                $sommeliers_note['ua'] = $rawItem->sommeliers_note_ua;
            }

            $readyItem = new Product();
            $readyItem->name = $name;
            $readyItem->page_title = $pageTitle;
            $readyItem->page_description = $pageDescription;
            $readyItem->page_slug = $rawItem->page_slug;
            $readyItem->product_type = Product::ACCESSORIES_TYPE;
            $readyItem->barcode = $rawItem->barcode;
            $readyItem->sku = $rawItem->sku;
            $readyItem->volume = $rawItem->volume;
            $readyItem->price = $rawItem->price;
            $readyItem->old_price = $rawItem->old_price;
            $readyItem->end_sale = $rawItem->end_sale;
            $readyItem->whole_price = $rawItem->whole_price;
            $readyItem->quantity = $rawItem->quantity;
            $readyItem->quantity_in_pack = $rawItem->quantity_in_pack;
            $readyItem->isPresent = $rawItem->isPresent;
            $readyItem->seller = $rawItem->seller;
            $readyItem->translit = $rawItem->translit;
            $readyItem->popularity = $rawItem->popularity;
            $readyItem->image = $rawItem->image;
            $readyItem->brand_id = $rawItem->brand_id;
            $readyItem->country_id = $rawItem->country_id;
            $readyItem->description = $description;
            $readyItem->sommeliers_note = $sommeliers_note;
            $readyItem->show_in_google_merchant = $rawItem->show_in_google_merchant;
            $readyItem->show_in_search_io = $rawItem->show_in_search_io;
            $readyItem->show_in_allo = $rawItem->show_in_allo;
            $readyItem->show_on_site = $rawItem->show_on_site;
            $readyItem->save();
            $readyItem->categories()->attach($categoryId);
        }
    }

    private function saveCategory($rawCategory)
    {
            $category=Category::whereRaw("JSON_EXTRACT(name, '$.ru') = '".$rawCategory->name_ru."'")->first();
        if ($category===null){
            $name = [];
            if ($rawCategory->name_ru !== null) {
                $name['ru'] = $rawCategory->name_ru;
            }
            if ($rawCategory->name_ua != null) {
                $name['ua'] = $rawCategory->name_ua;
            }
            $pageTitle = [];
            if ($rawCategory->page_title_ru != null) {
                $pageTitle['ru'] = $rawCategory->page_title_ru;
            }
            if ($rawCategory->page_title_ua != null) {
                $pageTitle['ua'] = $rawCategory->page_title_ua;
            }
            $pageDescription = [];
            if ($rawCategory->page_description_ru != null) {
                $pageDescription['ru'] = $rawCategory->page_description_ru;
            }
            if ($rawCategory->page_description_ua != null) {
                $pageDescription['ua'] = $rawCategory->page_description_ua;
            }
            $readyItem = new Category();
            $readyItem->name = $name;
            $readyItem->page_title = $pageTitle;
            $readyItem->page_description = $pageDescription;
            $readyItem->page_slug = $rawCategory->page_slug;
            $readyItem->save();

            return $readyItem->id;
        }


        return $category->id;

    }
}
