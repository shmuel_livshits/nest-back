<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class Alcohol_dishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $rawItems = $client->get('alc_dish')->getBody()->getContents();
        $rawItems=json_decode($rawItems);
        foreach ($rawItems as $productSku=>$skuArr){
            $alcoholProduct=Product::where('sku',$productSku)->first();

            foreach ($skuArr as $sku){
                $accessory=Product::where('sku',$sku)->first();
                $alcoholProduct->accessories()->attach($accessory->id);
            }

        }
    }
}
