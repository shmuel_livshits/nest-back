<?php

namespace Database\Seeders;

use App\Models\Wine_type;
use Illuminate\Database\Seeder;

class Wine_typesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \GuzzleHttp\Client((['base_uri' => 'https://stage-back.winelibrary.com.ua/api/all/']));
        $rawItems = $client->get('wine_types')->getBody()->getContents();
        $rawItems=json_decode($rawItems);
        foreach ($rawItems as $rawItem){
            $name=[];
            if ($rawItem->name_ru!==null){
                $name['ru']=$rawItem->name_ru;
            }
            if ($rawItem->name_ua!=null){
                $name['ua']=$rawItem->name_ua;
            }
            $readyItem = new Wine_type();
            $readyItem->id=$rawItem->id;
            $readyItem->name=$name;
            $readyItem->save();
        }
    }
}
