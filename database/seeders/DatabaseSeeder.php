<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([

            CountrySeeder::class,
            Alcohol_stylesSeeder::class,
            AuthorsSeeder::class,
            ProvidersSeeder::class,
            ProducersSeeder::class,
            BrandsSeeder::class,
            CombinationsSeeder::class,
            Grape_sortsSeeder::class,
            MarkingsSeeder::class,
            PacksSeeder::class,
            PublishersSeeder::class,
            Sugar_contentsSeeder::class,
            TagsSeeder::class,
            Wine_typesSeeder::class,
            RegionsSeeder::class,
            Alcohol_category_seeder::class,
            Alcohol_productseeder::class,
            ProductTagSeeder::class,
            ProductCombinationSeeder::class,
            Product_sugarSeeder::class,
            Product_grapeSeeder::class,
            Product_categorySeeder::class,
            accessoriesSeeder::class,
            BooksSeeder::class,
            Alcohol_dishSeeder::class,
        AlcVolsSeeder::class,
        dish_volSeeder::class,
        ]);
    }
}

/*
 *

   Route::get('/categories',[\App\Http\Controllers\InfoController::class,'categories']);

*/