<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->char('role')->default('customer');
            $table->char('last_name')->nullable();
            $table->char('city')->nullable();
            $table->char('phone')->nullable();
            $table->json('shipping_method')->nullable();
            $table->char('shipping_provider')->nullable();
            $table->char('shipping_destination')->nullable();
            $table->char('payment_mask')->nullable();
            $table->char('payment_method')->nullable();
            $table->char('payment_provider')->nullable();
            $table->char('payment_tocken')->nullable();
            $table->boolean('news_subscribe')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
