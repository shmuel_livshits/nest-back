<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_operators', function (Blueprint $table) {
            $table->id();
            $table->json('name');
            $table->text('logo');
            $table->time('dispatch_time');
            $table->json('message')->nullable();
            $table->json('free_message')->nullable();
            $table->json('not_free_message')->nullable();
            $table->json('message_later')->nullable();
            $table->integer('price_for_free')->default(0);
            $table->char('api_name_param')->nullable();
            $table->boolean('isActive')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_operators');
    }
}
