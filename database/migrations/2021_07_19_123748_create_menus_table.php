<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable(); // в меню имя должно быть не обязательно так как если его нет его поднятнет с сущности
	        $table->char('link')->nullable();

	        $table->integer('parent_id')->default(0)->nullable();
	        $table->integer('lft')->unsigned()->nullable();
	        $table->integer('rgt')->unsigned()->nullable();
	        $table->integer('depth')->unsigned()->nullable();
	        $table->boolean('published')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
