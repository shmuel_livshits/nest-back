<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->id();
            $table->json('name');
            $table->text('logo');
            $table->text('cover')->nullable();
            $table->foreignId('producer_id')->constrained();
            $table->char('official_website',80)->nullable();
            $table->json('description')->nullable();

            //SEO
            $table->json('page_title');
            $table->json('page_description')->nullable();;
            $table->char('page_slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}
