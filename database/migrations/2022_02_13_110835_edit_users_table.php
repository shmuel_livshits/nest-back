<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('users', function (Blueprint $table)
		{
			$table->dropColumn('city');
			$table->dropColumn('shipping_method');
			$table->dropColumn('shipping_provider');
			$table->dropColumn('shipping_destination');
			$table->dropColumn('payment_mask');
			$table->dropColumn('payment_provider');
			$table->dropColumn('payment_tocken');
			$table->dropColumn('payment_method');
			$table->json('shippings')->nullable();
			$table->json('payments')->nullable();
			$table->json('cart')->nullable();
			$table->json('favorites')->nullable();
			$table->json('viewed')->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->json('shipping_method')->nullable();
			$table->char('city')->nullable();
			$table->char('shipping_provider')->nullable();
			$table->char('shipping_destination');
			$table->char('payment_mask');
			$table->char('payment_provider');
			$table->char('payment_tocken');
			$table->char('payment_method');
			$table->dropColumn('shippings');
			$table->dropColumn('payments');
			$table->dropColumn('cart');
			$table->dropColumn('favorites');
			$table->dropColumn('viewed');
		});
	}
}
