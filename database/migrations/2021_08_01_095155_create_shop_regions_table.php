<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_regions', function (Blueprint $table) {
            $table->id();
            $table->text('name');

	        $table->integer('parent_id')->default(0)->nullable();
	        $table->integer('lft')->unsigned()->nullable();
	        $table->integer('rgt')->unsigned()->nullable();
	        $table->integer('depth')->unsigned()->nullable();
	        $table->smallInteger('city_type')->unsigned()->default(0); //0 - обычный, 1 - популярный, 2 главный

	        $table->json('page_title');
	        $table->json('page_description')->nullable();
	        $table->char('page_slug');
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_regions');
    }
}
