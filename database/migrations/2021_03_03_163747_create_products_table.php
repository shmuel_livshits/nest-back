<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            //product common
            $table->json('name');
            $table->char('product_type')->default(\App\Models\Product::ALCOHOL_TYPE);
            $table->char('barcode',50)->nullable();
            $table->char('sku',50);
            $table->double('volume',7,3)->nullable();
            $table->double('price',10,2);
            $table->double('old_price',10,2)->nullable();
            $table->date('end_sale')->nullable();
            $table->double('whole_price',10,2)->nullable();
            $table->integer('whole_quantity')->unsigned()->nullable();
            $table->integer('quantity')->unsigned()->default(1);
            $table->integer('quantity_in_pack')->nullable()->unsigned()->default(1);
            $table->boolean('isPresent')->default(false);
            $table->char('seller');
            $table->json('translit')->nullable();
            $table->integer('popularity')->unsigned()->default(0);
            //end 1C
            $table->text('image')->nullable();
            $table->foreignId('brand_id')->nullable()->constrained();
            $table->foreignId('region_id')->nullable()->constrained();//выбор
            $table->foreignId('country_id')->nullable()->constrained();//выбор
            $table->text('description')->nullable();
            $table->text('sommeliers_note')->nullable();
            $table->foreignId('sommelier_id')->nullable()->constrained();
            //alcohol
            $table->double('degree',5,2)->unsigned()->nullable();
            $table->foreignId('marking_id')->nullable()->constrained();
            $table->foreignId('pack_id')->nullable()->constrained();
            $table->foreignId('alcohol_style_id')->nullable()->constrained();
            $table->integer('alcohol_aging')->unsigned()->nullable();
            $table->json('aging_type')->nullable();
            $table->integer('harvest_year')->unsigned()->nullable();
            $table->integer('serving_temperature_from')->nullable();
            $table->integer('serving_temperature_to')->nullable();
            $table->foreignId('wine_type_id')->nullable()->constrained();

            //books
            $table->foreignId('publisher_id')->nullable()->constrained();
            $table->integer('publish_year')->unsigned()->nullable();
            $table->foreignId('language_id')->nullable()->constrained();
            $table->json('preview_pictures')->nullable();

            //integrations
            $table->boolean('show_in_google_merchant')->default(true);
            $table->boolean('show_in_search_io')->default(true);
            $table->boolean('show_in_allo')->default(true);
            $table->boolean('show_on_site')->default(true);

            //SEO
            $table->json('page_title');
            $table->json('page_description')->nullable();
            $table->char('page_slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
