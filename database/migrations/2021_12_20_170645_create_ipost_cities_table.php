<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpostCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ipost_cities', function (Blueprint $table) {
            $table->id();
	        $table->json('name');
	        $table->integer('parent_id')->default(0)->nullable();
	        $table->integer('lft')->unsigned()->nullable();
	        $table->integer('rgt')->unsigned()->nullable();
	        $table->integer('depth')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ipost_cities');
    }
}
