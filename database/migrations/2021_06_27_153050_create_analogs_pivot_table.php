<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalogsPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analogs_pivot', function (Blueprint $table) {
	        $table->unsignedBigInteger('product_id')->index();
	        $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
	        $table->unsignedBigInteger('product_anlg_id')->index();
	        $table->foreign('product_anlg_id')->references('id')->on('products')->onDelete('cascade');
	        $table->primary(['product_id', 'product_anlg_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analogs_pivot');
    }
}
