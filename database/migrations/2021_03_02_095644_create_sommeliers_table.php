<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSommeliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sommeliers', function (Blueprint $table) {
            $table->id();
            $table->json('first_name');
            $table->json('last_name');
            $table->integer('experience')->unsigned()->nullable();
            $table->text('avatar');
            $table->json('description');//особенности данного сомелье
            //SEO
            $table->json('page_title');
            $table->json('page_description')->nullable();
            $table->char('page_slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sommeliers');
    }
}
