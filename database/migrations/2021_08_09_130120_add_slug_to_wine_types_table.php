<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugToWineTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wine_types', function (Blueprint $table) {
	        $table->json('description')->nullable();
	        $table->json('page_title');
	        $table->json('page_description')->nullable();
	        $table->char('page_slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wine_types', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('page_title');
            $table->dropColumn('page_description');
            $table->dropColumn('page_slug');
        });
    }
}
