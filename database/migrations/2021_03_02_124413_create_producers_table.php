<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producers', function (Blueprint $table) {
            $table->id();
            $table->json('name');
            $table->text('logo');
            $table->text('cover')->nullable();
            $table->json('description');
            $table->foreignId('provider_id')->constrained();

            //SEO
            $table->json('page_title');
            $table->json('page_description')->nullable();;
            $table->char('page_slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producers');
    }
}
