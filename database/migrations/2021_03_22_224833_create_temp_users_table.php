<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_users', function (Blueprint $table) {
            $table->id();
            $table->char('name');
            $table->char('email');
            $table->char('phone');
            $table->char('last_name');
            $table->char('city');
            $table->char('shipping_provider')->nullable();
            $table->char('shipping_destination')->nullable();
            $table->char('payment_mask')->nullable();
            $table->char('payment_method')->nullable();
            $table->char('payment_provider')->nullable();
            $table->char('payment_tocken')->nullable();
            $table->char('news_subscribe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_users');
    }
}
