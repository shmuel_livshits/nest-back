<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('sommelier', 'SommelierCrudController');
    Route::crud('provider', 'ProviderCrudController');
    Route::crud('producer', 'ProducerCrudController');
    Route::crud('brand', 'BrandCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('characteristic', 'CharacteristicCrudController');
    Route::crud('sugar_content', 'Sugar_contentCrudController');
    Route::crud('country', 'CountryCrudController');
    Route::crud('region', 'RegionCrudController');
    Route::crud('grape_sort', 'Grape_sortCrudController');
    Route::crud('alcohol_style', 'Alcohol_styleCrudController');
    Route::crud('tag', 'TagCrudController');
    Route::crud('marking', 'MarkingCrudController');
    Route::crud('pack', 'PackCrudController');
    Route::crud('publisher', 'PublisherCrudController');
    Route::crud('author', 'AuthorCrudController');
    Route::crud('combination', 'CombinationCrudController');
    Route::crud('wine_type', 'Wine_typeCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('language', 'LanguageCrudController');
    Route::crud('post_operator', 'Post_operatorCrudController');
    Route::crud('redirect', 'RedirectCrudController');
    Route::crud('main_delivery_city', 'Main_delivery_cityCrudController');
    Route::crud('shop', 'ShopCrudController');
    Route::crud('article', 'ArticleCrudController');
    Route::crud('temp_user', 'Temp_userCrudController');
    Route::crud('order', 'OrderCrudController');
    Route::crud('site_page', 'Site_PageCrudController');
    Route::crud('chars_category', 'Chars_categoryCrudController');
    Route::crud('menu', 'MenuCrudController');
    Route::crud('shop_regions', 'ShopRegionCrudController');
    Route::crud('sign', 'SignCrudController');
    Route::crud('phone', 'PhoneCrudController');
    Route::crud('black-list-ip', 'BlackListIpCrudController');
    Route::crud('ipost-city', 'IpostCityCrudController');
}); // this should be the absolute last line of this file