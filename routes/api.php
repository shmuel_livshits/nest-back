<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('db')->group(function (){
    Route::get('full',[\App\Http\Controllers\ClientController::class,'getDb']);
});
Route::group([
    'prefix' => 'storage'
], function () {
    Route::post('/saveProductsInfo', [\App\Http\Controllers\StorageSynchronizationController::class,'saveProductsInfo']);
});

Route::group([
    'prefix' => 'feeds'
], function () {
    Route::get('/allo', [\App\Http\Controllers\FeedsController::class,'alloFeeds']);
    Route::get('/sales-drive', [\App\Http\Controllers\FeedsController::class,'salesDriveFeeds']);
	Route::get('/alco_biz', [\App\Http\Controllers\FeedsController::class,'alcoBizFeeds']);
    Route::get('/google-merchant', [\App\Http\Controllers\FeedsController::class,'googleMerchantFeeds']);
    Route::get('/facebook-merchant', [\App\Http\Controllers\FeedsController::class,'facebookMerchantFeeds']);
    Route::get('/multisearch', [\App\Http\Controllers\FeedsController::class,'multisearchFeeds']);
});

Route::prefix('post-operators')->group(function () {
    Route::get('get-main-cities-list', [\App\Http\Controllers\PostOperatorsController::class,'getMainCitiesList']);
    Route::get('get-cities-list', [\App\Http\Controllers\PostOperatorsController::class,'getCitiesList']);
    Route::get('get-warehouses/{cityName}/{postOperator}', [\App\Http\Controllers\PostOperatorsController::class,'getWarehouses']);
});

Route::any('/create-order', [\App\Http\Controllers\OrdersController::class,'createNewOrder']);
Route::any('/create-pre-order', [\App\Http\Controllers\OrdersController::class,'productForOrder']);

Route::get('/getRedirects', [\App\Http\Controllers\SeoServiceController::class, 'getRedirects']);
Route::get('/getSitemapArray',[\App\Http\Controllers\SeoServiceController::class,'getSiteMapUrls']);


Route::prefix('test')->group(function(){
    Route::get('test',[\App\Http\Controllers\TestController::class,'test']);
});

Route::prefix('third-party-services')->group(function (){
	Route::any('/sales-drive-webhook/status', [\App\Http\Controllers\ThirdPartyServicesController::class,'salesDriveWebhook']);
});

Route::prefix('regions')->group(function (){
	Route::get('/all',[\App\Http\Controllers\RegionsController::class,'getRegions']);
});

Route::prefix('ajax-search')->group(function (){
	Route::any('shop-regions',[\App\Http\Controllers\AjaxSearchController::class,'shopRegions']);
});

Route::get('/test/email',[\App\Http\Controllers\TestController::class, 'testEmail']);
Route::get('/test',[\App\Http\Controllers\TestController::class, 'test']);
Route::get('/test/cleanImage',[\App\Http\Controllers\TestController::class, 'cleanImage']);

Route::get('/login/{provider}', [\App\Http\Controllers\Authcontroller::class,'redirectToProvider']);
Route::get('/login/{provider}/callback', [\App\Http\Controllers\Authcontroller::class,'handleProviderCallback']);

//->middleware('auth:sanctum')
Route::prefix('user')->group(function (){
	Route::get('/',[\App\Http\Controllers\UserController::class, 'getUser' ]);
	Route::get('set',[\App\Http\Controllers\UserController::class, 'setUser' ]);
	Route::get('sdata',[\App\Http\Controllers\UserController::class, 'getSessionData' ]);
	Route::get('set-sdata',[\App\Http\Controllers\UserController::class, 'setSessionData' ]);
	Route::get('orders',[\App\Http\Controllers\UserController::class, 'getUsersOrders' ]);
});

//Поменять строку для prod Route::middleware('auth:sanctum')->prefix('user')->group(function (){


