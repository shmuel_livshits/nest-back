<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i
                class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon la la-files-o"></i>
        <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-square"></i> Общее</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('sommelier') }}'><i
                        class="las la-user-graduate"></i>Сомелье</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('provider') }}'><i class="las la-truck"></i>
                Поставщики</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('producer') }}'><i class="las la-industry"></i>
                Производители</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('brand') }}'><i class="las la-trademark"></i>
                Бренды</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('country') }}'><i class="las la-map"></i> Страны</a>
        </li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('region') }}'><i
                        class="las la-map-marker-alt"></i> Регионы</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class="las la-hashtag"></i><i
                        class="las la-wine-bottle"></i> Хештеги</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('char') }}'><i class='nav-icon la la-question'></i> Характеристики</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-glass-martini-alt"></i> Свойства алкоголя</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('wine_type') }}'><i
                        class="las la-wine-bottle"></i> Типы Вина</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('sugar_content') }}'><i class="las la-spa"></i>
                Содержание Сахара</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('grape_sort') }}'><i class="las la-seedling"></i>
                Сорта винограда</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('alcohol_style') }}'><i
                        class="las la-cocktail"></i> Стили</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('marking') }}'><i class="las la-lock"></i>
                Маркировка</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('pack') }}'><i class="las la-box"></i>
                Упаковка</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('combination') }}'><i class="las la-cheese"></i>
                Сочетания</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-book"></i> Свойства книг</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('publisher') }}'><i
                        class="las la-book-reader"></i> Издательства</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('author') }}'><i class="las la-user-edit"></i>
                Авторы</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('language') }}'><i
                        class='nav-icon las la-language'></i> Языки</a></li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('chars_category') }}'><i class='nav-icon la la-question'></i> Chars_categories</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('characteristic') }}'>Управление карточкой товара и
        фильтрами</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('sign') }}'><i class='nav-icon la la-question'></i> Признаки</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('menu') }}'><i class="las la-folder-open"></i> Меню</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><b><i class="las la-folder-open"></i></i>
            Категории</b></a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><b><i class="las la-wine-bottle"></i><i
                    class="las la-wine-glass-alt"></i><i class="las la-book"></i> Товары</b></a></li>


<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="las la-truck"></i> Доставка</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('post_operator') }}'><i class="las la-truck"></i>
                Почтовые операторы</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shop_regions') }}'><i class="nav-icon la-city"></i> Регионы доставки</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('main_delivery_city') }}'><i
                        class="las la-map-marker-alt"></i> Главные города для доставки</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shop') }}'><i class="las la-store"></i> Наши
                магазины</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('phone') }}'><i class="nav-icon las la-phone-volume"></i> Телефоны магазинов</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ipost-city') }}'><i class="nav-icon la-city"></i>Города Ipost</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> SEO</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('redirect') }}'><i class="las la-redo"></i>
                Redirects</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('article') }}'><i class="las la-edit"></i> Статьи</a>
        </li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('site_page') }}'><i
                        class='nav-icon lar la-file'></i> Страницы сайта</a></li>
    </ul>
</li>

@if(backpack_user()->hasRole('super_admin'))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Advanced</a>
        <ul class="nav-dropdown-items">
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'><i class='nav-icon la la-cog'></i> <span>Settings</span></a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('black-list-ip') }}'><i class="nav-icon las la-ban"></i>Заблокированные IP адреса </a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('log') }}'><i
                            class='nav-icon la la-terminal'></i> Logs</a></li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Пользователи</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i
                                    class="nav-icon la la-user"></i> <span>Пользователи</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i
                                    class="nav-icon la la-id-badge"></i> <span>Роли</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i
                                    class="nav-icon la la-key"></i> <span>Разрешения</span></a></li>
                    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('temp_user') }}'><i
                                    class='nav-icon la la-question'></i> Незарег. users</a></li>
                    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('order') }}'>Копии заказов</a></li>
                </ul>
            </li>
        </ul>
    </li>
@endif
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('backup') }}'><i class='nav-icon la la-hdd-o'></i> Backups</a></li>