<?php

namespace App\Console;

use App\Models\Product;
use App\Nest\Service\DbToFileWizard;
use App\Nest\Service\Feeds;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
	    $schedule->command('cleanCache')->everyMinute()->name('CleanCache');
        $schedule->call(function (){
            $product= new Product();
            $product->turnOffProductNoImage();
//	        $clientDb->compareItemsInFileAndDb();
        })->everyMinute()->name('generateDbFile');

	    $schedule->command('generateFacebookMerchantFeeds')->hourlyAt(15)->name('generateFacebookMerchantFeeds')->withoutOverlapping(5);
	    $schedule->command('generateSalesDriveFeeds')->hourlyAt(22)->name('generateSalesDriveFeeds')->withoutOverlapping(5);
	    $schedule->command('generateAlloFeeds')->hourlyAt(29)->name('generateAlloFeeds')->withoutOverlapping(5);
       $schedule->command('generateAlcoBizFeeds')->hourlyAt(35)->name('generateAlloFeeds')->withoutOverlapping(5);
        $schedule->command('generateGoogleFeeds')->hourlyAt(39)->name('generateGoogleFeeds')->withoutOverlapping(5);

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
