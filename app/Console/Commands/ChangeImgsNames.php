<?php

namespace App\Console\Commands;

use App\Models\Brand;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ChangeImgsNames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ChangeImgsNames';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Images Names';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $brands=Brand::all();
        foreach ($brands as $brand){
            $newLogo=str_replace('br','b',$brand->logo);
            $brand->logo=$newLogo;
            $brand->save();
        }
        $allFiles=Storage::disk('winelibrary')->allFiles('originals');
        foreach ($allFiles as $file){
            if(strpos($file,'br')!==false){
                try{
                    Storage::disk('winelibrary')->move($file, str_replace('br','b',$file));
                }
                catch (\Exception $ex){
                    dump($ex);
                }
            }
        }
        $allFiles2=Storage::disk('winelibrary')->allFiles();
        foreach ($allFiles2 as $file){
            if(strpos($file,'br')!==false){
                try{
                    Storage::disk('winelibrary')->move($file, str_replace('br','b',$file));
                }
                catch (\Exception $ex){
                    dump($ex);
                }
            }
        }

    }
}
