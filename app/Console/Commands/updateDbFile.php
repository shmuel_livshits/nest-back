<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Nest\Service\DbToFileWizard;
use Illuminate\Console\Command;

class updateDbFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Заново создать db файл';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
	    $product= new Product();
	    $product->turnOffProductNoImage();
	    $clientDb= new DbToFileWizard();
	    $clientDb->getDb(true);
    }
}
