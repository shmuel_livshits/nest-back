<?php

namespace App\Console\Commands;

use App\ThirdPartyServices\NovaPochta;
use Illuminate\Console\Command;

class insertCitiesAndREgionsFromNp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'saveCitiesAndRegionsToDb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Берет базу данных городов и областей с Новой почты и формирует их в нужный нам вид';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $npObj = new NovaPochta();
        $npObj->saveRegionsAndCitiesFromNpDb();
    }
}
