<?php

namespace App\Console\Commands;

use Backpack\Settings\app\Models\Setting;
use Illuminate\Console\Command;

class InsertSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insertSettings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
	    $settingsArr=[
		    [
			    'key'=>'cleanCache',
			    'name'=>'Очистить кеш',
			    'field'=>'{"name":"value","label":"Очистить","type":"checkbox"}',
			    'active'=>1,
			    'value'=>0,
			    'description'=>'Перегружает файлы, обновляет списки категорий',
		    ]

	    ];
	    foreach ($settingsArr as $settingItem){
		    $setting = new Setting();
		    $setting->key=$settingItem['key'];
		    $setting->name=$settingItem['name'];
		    $setting->field=$settingItem['field'];
		    $setting->active=$settingItem['active'];
		    $setting->value=$settingItem['value'];
		    $setting->description=$settingItem['description'];
		    $setting->save();
	    }
    }
}
