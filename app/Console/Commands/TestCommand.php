<?php

namespace App\Console\Commands;

use App\Nest\Images\ImageService;
use App\Nest\Images\TinifyImages;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixImages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
	    $originals=collect(Storage::disk('winelibrary')->allFiles('originals'));
	    $preparedAll=collect(Storage::disk('winelibrary')->allFiles());
	    $neededToFixCollection=collect([]);
	    foreach ($originals as  $original){
		    $isExits=$preparedAll->filter(function ($item) use ($original){
			    if (strpos($original,$item)!==false){
				    return $item;
			    }
		    });
		    if ($isExits->count()===1){
			    $neededToFixCollection->push($original);
		    }
	    }
	    foreach ($neededToFixCollection as $imgToFix){
		    $imagesProps=config('filesystems.disks.winelibrary.product');
		    $watermarkProps=config('filesystems.disks.winelibrary.watermark');
		    $imageName=substr($imgToFix,strpos($imgToFix,'/')+1);
		    $filename=substr($imageName,0,strpos($imageName,'.'));
		    $originalExtension=substr($imageName,strpos($imageName,'.')+1);
		    foreach ($imagesProps as $imageProperty) {
			    $sourceImg=Storage::disk('winelibrary')->get($imgToFix);
			    $rightSizeImage = Image::make($sourceImg)->resize($imageProperty['width'], $imageProperty['height'], function ($constraint) {
				    $constraint->aspectRatio();
				    $constraint->upsize();
			    });
			    if ($watermarkProps !== null) {
				    $waterMarkWidth=$imageProperty['height'] * $watermarkProps['width'];
				    $imagService= new ImageService();
				    $imagService->insertWatermark($waterMarkWidth,$rightSizeImage);
			    }
			    $tinifyImage = new TinifyImages();
			    $minifiedImage = $tinifyImage->compressImage($rightSizeImage);
			    Storage::disk('winelibrary')->put($imageProperty['shortName'].$filename.'.'.$originalExtension, $minifiedImage->stream($originalExtension));
			    Storage::disk('winelibrary')->put($imageProperty['shortName'].$filename.'.webp', $minifiedImage->stream('webp'));
		    }
	    }
    }
}
