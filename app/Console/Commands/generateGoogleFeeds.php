<?php

namespace App\Console\Commands;

use App\Nest\Service\Feeds;
use Illuminate\Console\Command;

class generateGoogleFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generateGoogleFeeds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $locales=config('app.available_locales');
        foreach ($locales as $locale){
            app()->setLocale($locale);
            $feeds=new Feeds();
            $feeds->generateGoogleFeeds();
        }
    }
}
