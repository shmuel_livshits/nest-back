<?php

namespace App\Console\Commands;

use App\Nest\Service\DbToFileWizard;
use Backpack\Settings\app\Models\Setting;
use Illuminate\Console\Command;

class cleanCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanCache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
	    $cleanChashSetting=Setting::where('key','cleanCache')->first();
	    if ($cleanChashSetting->value==='1'){
		    $cleanChashSetting->value="0";
		    $cleanChashSetting->save();
		    $clientDb= new DbToFileWizard();
		    $clientDb->getDb(true);

	    }
    }
}
