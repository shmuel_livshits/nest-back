<?php

namespace App\Http\Controllers;

use App\Models\Alcohol_style;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Producer;
use App\Models\Product;
use App\Models\Redirect;
use App\Models\Tag;
use App\Nest\Service\Slug;
use Illuminate\Http\Request;

class SeoServiceController extends Controller
{
    public function getRedirects(Request $request)
    {
        $redirects = Redirect::get();

        return response($redirects->toJson());
    }
    public function getSiteMapUrls(){
        $products=Product::get(['id','page_slug','updated_at']);
        $brands=Brand::get(['id','page_slug','updated_at']);
        $producers=Producer::get(['id','page_slug','updated_at']);
        $categories=Category::get(['id','page_slug','updated_at']);
        $tags=Tag::get(['id','page_slug','updated_at']);
        $styles=Alcohol_style::get(['id','page_slug','updated_at']);
        $items=collect([]);
        $items= $items->merge($products)->merge($brands)->merge($categories)->merge($producers)->merge($tags)->merge($styles);
        $domain=env('APP_URL');
        $urls=[];
        $slug=new Slug();
        foreach ($items as $item){
            $url= new \stdClass();
            $url->url=$domain.'/'.$slug->translitAndSlugify($item->page_slug).'/'.$item::ID_PREFIX.$item->id;
            $url->lastmod=$item->updated_at->toDateString();
            $url->changefreq='weekly';
            array_push($urls,$url);
        }
        return response($urls);

    }
}
