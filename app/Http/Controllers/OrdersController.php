<?php

namespace App\Http\Controllers;

use App\Models\BlackListIp;
use App\Models\Order;
use App\Models\Post_operator;
use App\Models\Product;
use App\ThirdPartyServices\ESputnik;
use App\ThirdPartyServices\SalesDrive;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use function PHPUnit\Framework\isEmpty;

class OrdersController extends Controller
{
    public function createNewOrder(Request $request)
    {
    	/*$request=json_decode('{"customerToken":"shmuel.livshits@gmail.com","products":[{"id":"i2407","name":"Вино Barbadillo Manzanilla Solear 0.75л","sku":"AWIN0000130","brand":"Barbadillo","category":"Вина","quantity":1,"price":907},{"id":"i2125","name":"Вино игристое Cuvee de Purcari Rose Brut 0.375л","sku":"AWIN0099560","brand":"Purcari","category":"Шампанское и игристое","quantity":1,"price":465}],"total":1372,"payment_method":"cash","shipping_provider":"post1","shipping_destination":"Отделение №1: ул. Маршала Малиновского, 114"}');
    	$requestArr=json_decode('{"customerToken":"shmuel.livshits@gmail.com","products":[{"id":"i2407","name":"Вино Barbadillo Manzanilla Solear 0.75л","sku":"AWIN0000130","brand":"Barbadillo","category":"Вина","quantity":1,"price":907},{"id":"i2125","name":"Вино игристое Cuvee de Purcari Rose Brut 0.375л","sku":"AWIN0099560","brand":"Purcari","category":"Шампанское и игристое","quantity":1,"price":465}],"total":1372,"payment_method":"cash","shipping_provider":"post1","shipping_destination":"Отделение №1: ул. Маршала Малиновского, 114"}',true);*/

        $validation=$this->validateOrder($request);
        if ($validation->fails()){
            return response($validation->errors());
        }
		app()->setLocale('ua');
        if (BlackListIp::where('ip',$request->ip())->first()===null){
	        $salesDrive= new SalesDrive();
	        $order = new Order();
	        $products=$salesDrive->prepareProductsToSalesDrive($request->products);
	        $order->products = json_encode($products);
	        $order->total = $request->total;
	        $requestInfo=$request->all();
	        $order->order_info = json_encode($requestInfo);
	        $order->save();
	        $externalOrderId=$this->makeExternalOrder($order->id);
	        $order->external_id=$externalOrderId;
	        $order->ip=$request->ip();
	        $order->save();
	        $shippingProviderName=Post_operator::getNameFromClientId($request->shipping_provider);
	        $salesDrive->createOrder($products, $request->phone, $request->name,$request->last_name,$request->email,
		        $shippingProviderName,$request->shipping_destination, $request->city, $request->payment_method,$externalOrderId,$request->comment,$request->oneClick,
		        $request->utm_source,$request->utm_campaign,$request->utm_medium,$request->utm_content,$request->utm_term);
	        $esputnik = new ESputnik();
	        $emailMessageId=env('ESPUTNIK_NEW_ORDER_EMAIL');

//        $esputnikResponce = $esputnik->smartSendEmail($externalOrderId,$emailMessageId,$request->name,$request->last_name,$request->email,$order->total,$products,$shippingProviderName,$request->shipping_destination,$request->city);

        }
        else{
	        $externalOrderId=Carbon::now()->timestamp;
        }
	    $response = new \stdClass();
	    $response->status='success';
	    $response->transactionId=$externalOrderId;
	    $response->ip=$request->ip();
        return response(json_encode($response));


    }

    public function productForOrder(Request $request){
    	/*$phone='0698745657';
    	$sku='AWIN0003500';
    	$name='Василий';*/
    	$sku=$request->sku;
    	$name=$request->name;
    	$phone=$request->phone;
    	if (!empty($sku) && !empty($name) && !empty($phone)  ){
		    $product=Product::where('sku',$sku)->first();
		    $requestInfo=$request->all();
		    $order = new Order();
		    $order->products = json_encode([$product]);
		    $order->total = 0;
		    $order->order_info = json_encode($requestInfo);
		    $order->save();
		    $salesDrive = new SalesDrive();
		    $products=$salesDrive->prepareProductsToSalesDrive([$product]);
		    $salesDrive->createOrder($products,$name,'',$phone,'','','','','cash',$order->id,$request->utm_source);
		    return response('success');
	    }
	    return response($request->all());



    }

    public function sendCreateOrderEmail(){

    }

    private function validateOrder(Request $request){
        $validationRules=[
//            'total' => 'required|numeric|min:100|max:1000000',
            'products'=>'required',
//            'customerToken'=>'required',

        ];
        $validationMessages=[];
        $validator = Validator::make($request->all(),
            $validationRules,
            $validationMessages);
        return $validator;
    }

    private function makeExternalOrder(int $orderId){
    	return 10000+$orderId;
    }
}
