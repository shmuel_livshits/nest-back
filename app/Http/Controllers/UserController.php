<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Post_operator;
use App\Models\Temp_user;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
	public function getUser(Request $request){
//        $user=Auth::user();
        $user=User::where('id',2)->first();
        return response()->json($user->prepareForOutDoor(),200,[],JSON_UNESCAPED_UNICODE);
    }

    public function setUser(Request $request){
//	    $user=Auth::user();
	    $user=User::where('id',2)->first();
	    $updatedUserInfo = $request->updatedUserInfo;
	    /*$updatedUserInfo='
	    {
	    "fname": "Shmuel", 
	    "lname":"Livshyts",
	    "phone": "0633509078",
	    "news":true,
	    "shippings": [
        {
          "N":"Отделение в офисе",
          "provider": "Новая почта",
          "street": "ул. Кирова, 12, кв.5 или №14",
          "city": "Днепр",
          "zip": "49000",
          "country": "Украина",
          "state": "Днепроптровская"
        }
  ],
  "payments": [
    { 
      "token": "sdsdsddddddfdd",
      "last4": "5141",
      "provider": "VISA"
    }
  ]
	   
}';*/
	    $user->updateUserInfo(json_decode($updatedUserInfo,true));
	    return response('success');
    }

    public function setSessionData(Request $request){
//	    $user=Auth::user();
	    $user=User::where('id',2)->first();
	    $sessionDataInfo=$request->sessionDataInfo;
	    /*$sessionDataInfo='{
   "cart":[
      {
         "I":"id",
         "Q":"int"
      }
   ],
   "favorites":{
      "default":[
         "id"
      ],
      "custom-collection-name":[
         "id"
      ]
   },
   "viewed":[
      "id"
   ],
   "ordered":[
      "id"
   ]
}';*/
	    return response($user->updateSessionData(json_decode($sessionDataInfo,true)));

    }

    public function getSessionData(Request $request){
//	    $user=Auth::user();
	    $user=User::where('id',2)->first();
	    return response()->json($user->getSessionData(),200,[],JSON_UNESCAPED_UNICODE);

    }

    public function getUsersOrders(Request $request){
//	    $user=Auth::user();
	    $user=User::where('id',2)->first();
	    $skip=($request->skip===null)?0:$request->skip;
	    $take=($request->take===null)?20:$request->take;
	    return response()->json(Order::getUsersOrders($user,$skip,$take),200,[],JSON_UNESCAPED_UNICODE);
    }

    private function validateOrder(Request $request){
        $validationRules=[
            /*'name' => 'required|string|max:50',
            'last_name'=>'required|string|max:50',
            'email'=>'required|email',*/
            'phone'=>'required',
//            'city'=>'required|max:50',
            /*'shipping_provider'=>'required',
            'shipping_destination'=>'required',
            'payment_method'=>'required',*/

        ];
        $validationMessages=[];
        $validator = Validator::make($request->all(),
            $validationRules,
            $validationMessages);
        return $validator;
    }
}
