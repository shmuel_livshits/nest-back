<?php

namespace App\Http\Controllers;

use App\Models\ShopRegion;
use Illuminate\Http\Request;

class RegionsController extends Controller {
	public function getRegions( Request $request ) {
		$regions = ShopRegion::getItems();
		return response( $regions );
	}
}
