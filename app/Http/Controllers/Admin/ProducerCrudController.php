<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProducerRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProducerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProducerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation {
        show as traitShow;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Producer::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/producer');
        CRUD::setEntityNameStrings('producer', 'producers');
        $this->crud->setTitle('Производители'); // set the Title for the create action
        $this->crud->setHeading('Производители'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
                }
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(ProducerRequest::class);

        CRUD::addFields([
            ['name' => 'name', 'label' => 'Название','type'=>'text','wrapper'=>['class'=>'form-group col-md-6'],'tab'=>'Общая информация'],
            [
                'name'=>'provider_id',
                'label'=>'Поставщик',
                'type'=>'relationship',
                'inline_create'=>true,
                'entity'=>'provider',
                'model'=> 'App\Models\Provider',
                'attribute' => 'name',
                'wrapper'=>['class'=>'form-group col-md-6'],
                'tab'=>'Общая информация'
            ],
            [
                'name'=>'logo',
                'label' => 'Лого',
                'type' => 'upload',
                'upload' => true,
                'wrapper'=>['class'=>'form-group col-md-6'],
                'tab'=>'Общая информация'
            ],
            [
                'name'=>'cover',
                'label' => 'Обложка(cover)',
                'type' => 'upload',
                'upload' => true,
                'wrapper'=>['class'=>'form-group col-md-6'],
                'tab'=>'Общая информация'
            ],

            //SEO
            ['name'=>'page_slug','label'=>'Slug','type'=>'text','tab'=>'SEO'],
            ['name'=>'page_title','label'=>'Page title', 'type'=>'text','wrapper'=>['class'=>'form-group col-md-6'],'tab'=>'SEO'],
            ['name'=>'page_description','label'=>'Page description','type'=>'textarea','wrapper'=>['class'=>'form-group col-md-6'],'tab'=>'SEO'],
            ['name'=>'description','label'=>'Описание Страницы (SEO тексты)', 'type' => 'wysiwyg', 'tab' => 'SEO'],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function show($id)
    {
        CRUD::addColumns([
            ['name' => 'name', 'label' => 'Название'],
            ['name'=>'name_ua','label' => 'Название UA'],
            ['name'=>'provider_id','label'=>'Поставщик'],
            ['name'=>'logo','label' => 'Лого'],
            ['name'=>'cover','label' => 'Обложка(cover)'],
            ['name'=>'description','label' => 'Описание'],
            ['name'=>'description_ua','label' => 'Описание UA'],

            //SEO
            ['name' => 'page_slug', 'label' => 'Slug'],
            ['name' => 'page_title', 'label' => 'Page title'],
            ['name' => 'page_title_ua', 'label' => 'Page title UA'],
            ['name' => 'page_description', 'label' => 'Page description'],
            ['name' => 'page_description_ua', 'label' => 'Page description UA'],
        ]);
        $content = $this->traitShow($id);
        return $content;
    }

    public function fetchProvider()
    {
        return $this->fetch(\App\Models\Provider::class);
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}
}
