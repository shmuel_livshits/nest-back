<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\Alcohol_style;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Country;
use App\Models\Grape_sort;
use App\Models\Pack;
use App\Models\Product;
use App\Models\Region;
use App\Models\Sugar_content;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation {
        show as traitShow;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product');
        CRUD::setEntityNameStrings('product', 'products');
        $this->crud->setTitle('Товары'); // set the Title for the create action
        $this->crud->setHeading('Товары'); // set the Heading for the create action
        if (! backpack_user()->hasRole('super_admin')){
	        $this->crud->denyAccess('create');
	        $this->crud->denyAccess('delete');
        }
	    $this->crud->denyAccess('show');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();
        $this->setWidgets();
        $this->setFilters();
        CRUD::setColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
                }
            ],
            [
                'name' => 'brand',
                'label' => 'Бренд',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
                }
            ],
            ['name' => 'sku', 'label' => 'SKU'],
            ['name' => 'price', 'label' => 'Цена'],
            ['name' => 'quantity', 'label' => 'Количество'],
            ['name' => 'categories', 'label' => 'Категория'],
            ['name' => 'pack', 'label' => 'Упаковка'],
            ['name' => 'sugar_contents', 'label' => 'Содержание сахара'],
            ['name' => 'grape_sorts', 'label' => 'Сорта винограда'],
            ['name' => 'region', 'label' => 'Регион'],
            ['name' => 'country', 'label' => 'Страна'],
            ['name' => 'alcohol_style', 'label' => 'Стиль'],
            ['name' => 'harvest_year', 'label' => 'Год сбора урожая'],
            ['name' => 'description', 'label' => 'Описание'],
            ['name' => 'sommeliers_note', 'label' => 'Заметка сомелье'],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(ProductRequest::class);
        $productType = Product::ALCOHOL_TYPE;
        if ($this->crud->getCurrentEntry()) {
            $productType = $this->crud->getCurrentEntry()->product_type;
        }

        $prices = [
            [
                'name' => 'price',
                'label' => 'Цена',
                'type' => 'number',
                'wrapper' => ['class' => 'form-group col-md-2'],
	            'attributes'=>$this->getAttributesAccordingToRole(),
                'tab' => 'Цены',
            ],
            [
                'name' => 'old_price',
                'label' => 'Старая цена',
                'type' => 'number',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'attributes' => $this->getAttributesAccordingToRole(),
                'tab' => 'Цены',
            ],
            [
                'name' => 'end_sale',
                'label' => 'Дата окончания акции',
                'type' => 'text',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'attributes' => $this->getAttributesAccordingToRole(),
                'tab' => 'Цены',
            ],
            [
                'name' => 'whole_price',
                'label' => 'Оптовая цена',
                'type' => 'number',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'attributes' => $this->getAttributesAccordingToRole(),
                'tab' => 'Цены',
            ],
            [
                'name' => 'whole_quantity',
                'label' => 'Оптовое шт.',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'attributes' => $this->getAttributesAccordingToRole(),
                'tab' => 'Цены'
            ],
        ];

        $categoriesAndTags = [
            [
                'name' => 'categories',
                'label' => 'Категории',
                'wrapper' => ['class' => 'form-group col-md-12'],
                'type' => 'relationship',
                'entity' => 'categories',
                'inline_create' => ['entity' => 'category'],
                'attribute' => 'name',
                'model' => "App\Models\Category",
                'tab' => 'Категории и теги',
            ],
            [
                'name' => 'tags',
                'label' => '<i class="las la-hashtag"></i>Хештеги',
                'type' => 'relationship',
                'entity' => 'tags',
                'inline_create' => ['entity' => 'tag'],
                'attribute' => 'name',
                'model' => "App\Models\Tag",
                'tab' => 'Категории и теги',
            ],
        ];

        $accessories = [
            [
                'name' => 'accessories',
                'label' => 'Аксессуары к товару',
                'type' => 'select2_multiple',
                'entity' => 'accessories',
                'attribute' => 'name',
                'model' => "App\Models\Product",
                'tab' => 'Аксессуары'
            ],
            [
                'name' => 'volums',
                'label' => 'Связи по объемам',
                'type' => 'select2_multiple',
                'entity' => 'volums',
                'attribute' => 'name',
                'model' => "App\Models\Product",
                'tab' => 'Аксессуары'
            ],
	        [
                'name' => 'analogs',
                'label' => 'Аналоги',
                'type' => 'select2_multiple',
                'entity' => 'volums',
                'attribute' => 'name',
                'model' => "App\Models\Product",
                'tab' => 'Аксессуары'
            ],
        ];

        $images = [
            [
                'label' => "Фото",
                'name' => "image",
                'type' => 'upload',
                'upload' => true,
                'tab' => 'Картинки'
            ],
        ];

        $manufactoringInfo = [
            [
                'name' => 'brand_id',
                'label' => 'Бренд',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'type' => 'relationship',
                'inline_create' => true,
                'entity' => 'brand',
                'tab' => 'Производитель',
            ],
            [
                'name' => 'country_id',
                'label' => 'Страна',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'type' => 'relationship',
                'inline_create' => true,
                'entity' => 'country',
                'attribute' => 'name',
                'model' => "App\Models\Country",
                'tab' => 'Производитель',
            ],
            [
                'name' => 'region_id',
                'label' => 'Регион',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'type' => 'relationship',
                'inline_create' => true,
                'entity' => 'region',
                'attribute' => 'name',
                'model' => "App\Models\Region",
                'tab' => 'Производитель',
            ],
        ];

        $alcoholProperties = [
            [
                'name' => 'wine_type_id',
                'label' => 'Тип вина',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'type' => 'select2',
                'inline_create' => true,
                'entity' => 'wine_type',
                'attribute' => 'name',
                'model' => "App\Models\Wine_type",
                'tab' => 'Свойства алкоголя',
            ],
            [
                'name' => 'sugar_contents',
                'label' => 'Содержание сахара',
                'wrapper' => ['class' => 'form-group col-md-5'],
                'type' => 'relationship',
                'entity' => 'sugar_contents',
                'inline_create' => ['entity' => 'sugar_content'],
                'attribute' => 'name',
                'model' => "App\Models\Sugar_content",
                'tab' => 'Свойства алкоголя',
            ],
            [
                'name' => 'grape_sorts',
                'label' => 'Сорта винограда',
                'wrapper' => ['class' => 'form-group col-md-5'],
                'type' => 'relationship',
                'entity' => 'grape_sorts',
                'inline_create' => ['entity' => 'grape_sort'],
                'attribute' => 'name',
                'model' => "App\Models\Grape_sort",
                'tab' => 'Свойства алкоголя',
            ],
            [
                'name' => 'alcohol_style_id',
                'label' => 'Стиль',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'type' => 'relationship',
                'inline_create' => true,
                'entity' => 'alcohol_style',
                'attribute' => 'name',
                'model' => "App\Models\Alcohol_style",
                'tab' => 'Свойства алкоголя',
            ],
            [
                'name' => 'degree',
                'type' => 'number',
                'label' => 'Градус',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'tab' => 'Свойства алкоголя',
            ],
            [
                'name' => 'marking_id',
                'label' => 'Маркировка',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'type' => 'relationship',
                'inline_create' => true,
                'entity' => 'marking',
                'attribute' => 'name',
                'model' => "App\Models\Marking",
                'tab' => 'Свойства алкоголя',
            ],
            [
                'name' => 'pack_id',
                'label' => 'Упаковка',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'type' => 'relationship',
                'inline_create' => true,
                'entity' => 'pack',
                'attribute' => 'name',
                'model' => "App\Models\Pack",
                'tab' => 'Свойства алкоголя',
            ],
            [
                'name' => 'serving_temperature_from',
                'label' => 'Температура подачи от',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'type' => 'number',
                'tab' => 'Свойства алкоголя'
            ],
            [
                'name' => 'serving_temperature_to',
                'label' => 'Температура подачи до',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'type' => 'number',
                'tab' => 'Свойства алкоголя'
            ],
            [
                'name' => 'alcohol_aging',
                'label' => 'Выдержка(возраст)',
                'type' => 'number',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'tab' => 'Свойства алкоголя',
            ],
            [
                'name' => 'harvest_year',
                'label' => 'Год сбора урожая',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'tab' => 'Свойства алкоголя',
            ],
            [
                'name' => 'aging_type',
                'label' => 'Тип выдержки',
                'type' => 'text',
//                'wrapper' => ['class' => 'form-group col-md-6'],
                'tab' => 'Свойства алкоголя',
            ],

            [
                'name' => 'combinations',
                'label' => 'Сочетания',
                'type' => 'relationship',
                'entity' => 'combinations',
                'inline_create' => ['entity' => 'combination'],
                'attribute' => 'name',
                'model' => "App\Models\Combination",
                'tab' => 'Свойства алкоголя',
            ],
        ];

        $bookProperties = [
            [
                'name' => 'publisher_id',
                'label' => 'Издательство',
                'wrapper' => ['class' => 'form-group col-md-6'],
                'type' => 'relationship',
                'inline_create' => true,
                'entity' => 'publisher',
                'tab' => 'Свойства книги',
            ],

            [
                'name' => 'authors',
                'label' => 'Авторы',
                'type' => 'select2_multiple',
                'entity' => 'authors',
                'wrapper' => ['class' => 'form-group col-md-6'],
                'attribute' => 'name',
                'model' => "App\Models\Author",
                'tab' => 'Свойства книги'
            ],
            [
                'name' => 'language_id',
                'label' => 'Язык',
                'type' => 'relationship',
                'entity' => 'language',
                'inline_create' => true,
                'attribute' => 'name',
                'model' => "App\Models\Language",
                'tab' => 'Свойства книги'
            ],
            [
                'name' => 'publish_year',
                'label' => 'Год издания',
                'type' => 'number',
                'wrapper' => ['class' => 'form-group col-md-6'],
                'tab' => 'Свойства книги'
            ],
            [
                'name' => 'preview_pictures',
                'label' => 'Предпросмотр',
                'type' => 'browse_multiple',
                'wrapper' => ['class' => 'form-group col-md-10'],
                'tab' => 'Свойства книги'
            ],
        ];

        $sommelierInfo = [
            [
                'name' => 'sommeliers_note',
                'label' => 'Заметка',
                'type' => 'wysiwyg',
                'tab' => 'Сомелье',
            ],
            [
                'name' => 'sommelier_id',
                'label' => 'Автор заметки',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'type' => 'relationship',
                'inline_create' => true,
                'entity' => 'sommelier',
                'attribute' => 'first_name',
                'model' => "App\Models\Sommelier",
                'tab' => 'Сомелье',
            ],
        ];

        $productDescriptions = [
            [
                'name' => 'description',
                'label' => 'Описание',
                'type' => 'wysiwyg',
                'tab' => 'Описания',
            ]
        ];

        $integrations = [
            [
                'name' => 'show_on_site',
                'label' => 'Показывать на сайте',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'tab' => 'Интеграции с сервисами',
            ],
            [
                'name' => 'show_in_google_merchant',
                'label' => 'Показывать в Google merchant ',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'tab' => 'Интеграции с сервисами',
            ],
            [
                'name' => 'show_in_search_io',
                'label' => 'Показывать Search_io',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'tab' => 'Интеграции с сервисами',
            ],
            [
                'name' => 'show_in_allo',
                'label' => 'Показывать в Алло',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'tab' => 'Интеграции с сервисами',
            ],
            [
            'name' => 'show_in_alcobiz',
            'label' => 'Показывать в Alco.biz',
            'wrapper' => ['class' => 'form-group col-md-3'],
            'tab' => 'Интеграции с сервисами',
        ]
        ];

        $seo = [
            ['name' => 'page_slug', 'type' => 'text', 'label' => 'Slug', 'tab' => 'SEO'],
            ['name' => 'page_title', 'type' => 'text', 'label' => 'Page title', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name' => 'page_description', 'label' => 'Page description', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
        ];

        CRUD::addFields($this->getMainProductFields($productType));
        CRUD::addFields($prices);
        CRUD::addFields($categoriesAndTags);
        CRUD::addFields($images);

        if ($productType === Product::ALCOHOL_TYPE || $productType === Product::ACCESSORIES_TYPE) {
            CRUD::addFields($manufactoringInfo);
        }
        if ($productType === Product::ALCOHOL_TYPE) {
            CRUD::addFields($alcoholProperties);
        }
        if ($productType === Product::BOOK_TYPE) {
            CRUD::addFields($bookProperties);
        }

        CRUD::addFields($sommelierInfo);
        CRUD::addFields($productDescriptions);
        CRUD::addFields($integrations);
        CRUD::addFields($seo);
        CRUD::addFields($accessories);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function fetchBrand()
    {
        return $this->fetch(\App\Models\Brand::class);
    }

    public function fetchCategories()
    {
        return $this->fetch(\App\Models\Category::class);
    }

    public function fetchTags()
    {
        return $this->fetch(\App\Models\Tag::class);
    }

    public function fetchTag()
    {
        return $this->fetch(\App\Models\Tag::class);
    }
    public function fetchSigns()
    {
        return $this->fetch(\App\Models\Sign::class);
    }

    public function fetchSugar_contents()
    {
        return $this->fetch(\App\Models\Sugar_content::class);
    }

    public function fetchGrape_sorts()
    {
        return $this->fetch(\App\Models\Grape_sort::class);
    }

    public function fetchRegion()
    {
        return $this->fetch(\App\Models\Region::class);
    }

    public function fetchCountry()
    {
        return $this->fetch(\App\Models\Country::class);
    }

    public function fetchAlcohol_style()
    {
        return $this->fetch(\App\Models\Alcohol_style::class);
    }

    public function fetchWine_type()
    {
        return $this->fetch(\App\Models\Wine_type::class);
    }

    public function fetchSommelier()
    {
        return $this->fetch(\App\Models\Sommelier::class);
    }

    public function fetchCombinations()
    {
        return $this->fetch(\App\Models\Combination::class);
    }

    public function fetchMarking()
    {
        return $this->fetch(\App\Models\Marking::class);
    }

    public function fetchPack()
    {
        return $this->fetch(\App\Models\Pack::class);
    }

    public function fetchProducts()
    {
        return $this->fetch(\App\Models\Product::class);
    }

    public function fetchPublisher()
    {
        return $this->fetch(\App\Models\Publisher::class);
    }

    public function fetchLanguage()
    {
        return $this->fetch(\App\Models\Language::class);
    }

    public function fetchAuthors()
    {
        return $this->fetch(\App\Models\Author::class);
    }

    public function show($id)
    {
        /* CRUD::addColumns([

         ]);*/
        $content = $this->traitShow($id);
        return $content;
    }

    private function getMainProductFields($productType = Product::ALCOHOL_TYPE)
    {
        $mainProductFields = [
            [
                'name' => 'name',
                'label' => 'Название',
                'type' => 'text',
                'wrapper' => ['class' => 'form-group col-md-6'],
//                'attributes' => $this->getAttributesAccordingToRole(),
                'tab' => 'Основная информация',
            ],
            [
                'name' => 'translit',
                'label' => 'Транслит',
                'type' => 'text',
                'wrapper' => ['class' => 'form-group col-md-6'],
                'tab' => 'Основная информация'
            ],
            [
                'name' => 'barcode',
                'label' => '<i class="las la-barcode"></i> Штрихкод',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'attributes' => $this->getAttributesAccordingToRole(),
                'tab' => 'Основная информация'
            ],
            [
                'name' => 'sku',
                'label' => 'SKU',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'attributes' => $this->getAttributesAccordingToRole(),
                'tab' => 'Основная информация'
            ],


        ];

        $volume = [
            'name' => 'volume',
            'label' => 'Объем',
            'wrapper' => ['class' => 'form-group col-md-2'],
            'type' => 'number',
            'attributes' => $this->getAttributesAccordingToRole(),
            'tab' => 'Основная информация'
        ];
        if ($productType !== Product::BOOK_TYPE) {
            array_push($mainProductFields, $volume);
        }


        $mainProductFields2 = [

            [
                'name' => 'quantity',
                'label' => 'Количество',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'attributes' => $this->getAttributesAccordingToRole(),
                'tab' => 'Основная информация'
            ],
            [
                'name' => 'quantity_in_pack',
                'label' => 'Шт. в упаковке',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'type' => 'number',
                'tab' => 'Основная информация'
            ],
            [
                'name' => 'seller',
                'label' => 'Продавец',
                'wrapper' => ['class' => 'form-group col-md-6'],

                'attributes' => $this->getAttributesAccordingToRole(),
                'tab' => 'Основная информация'
            ],
            [
                'name' => 'popularity',
                'label' => 'Популярность',
                'type' => 'number',
                'wrapper' => ['class' => 'form-group col-md-6'],
                'tab' => 'Основная информация'
            ],
            [
                'name' => 'isPresent',
                'label' => 'Подарок <i class="las la-gift"></i>',
                'type' => 'checkbox',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'tab' => 'Основная информация'
            ],
	        [
                'name' => 'new',
                'label' => 'New',
                'type' => 'checkbox',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'tab' => 'Основная информация'
            ],
	        [
                'name' => 'top',
                'label' => 'Top',
                'type' => 'checkbox',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'tab' => 'Основная информация'
            ],
	        [
                'name' => 'hot',
                'label' => 'Hot ',
                'type' => 'checkbox',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'tab' => 'Основная информация'
            ],
	        [
	        	'name'=>'available_on_request',
		        'label'=>'Доступно по предзаказу',
		        'type' => 'checkbox',
		        'wrapper' => ['class' => 'form-group col-md-4'],
		        'tab' => 'Основная информация'
	        ],
	        [
	        	'name'=>'out_of_production',
		        'label'=>'Снято с производства',
		        'type' => 'checkbox',
		        'wrapper' => ['class' => 'form-group col-md-4'],
		        'tab' => 'Основная информация'
	        ],
	        /*[
		        'name' => 'signs',
		        'label' => 'Признаки',
		        'type' => 'relationship',
		        'entity' => 'signs',
		        'inline_create' => ['entity' => 'sign'],
		        'attribute' => 'name',
		        'model' => "App\Models\Sign",
		        'tab' => 'Основная информация',
	        ],*/
        ];
        $mainProductFields = array_merge($mainProductFields, $mainProductFields2);
        return $mainProductFields;
    }

    private function setWidgets()
    {

        Widget::add()->to('before_content')->type('div')->class('row')->content([
           /* Widget::make()
                ->type('progress_white')
                ->class('card mb-2 text-primary')
                ->value('Всего ' . Product::all()->count() . ' товаров.'),
            Widget::make()
                ->type('progress_white')
                ->class('card mb-2 text-danger')
                ->value(Product::where('quantity', 0)->count() . ' товаров не в наличии.'),
            Widget::make()
                ->type('progress_white')
                ->class('card mb-2 text-danger')
                ->value(Product::where('show_on_site', 0)->count() . ' выключенных товаров.'),*/
            Widget::make()
                ->type('alert')
                ->class('row text-info')
                ->content('
                    <ul>
                        <li>Для того чтоб найти пустые поля на русском нужно в текстовое поле фильтра ввести ru. Для того чтоб найти на украинском ввести ua</li>
                        <li>Для того чтоб найти товары, которых нет в наличии нужно в фильтре количество min и max ввести 0 и нажать enter</li>
                    </ul> 
                    '
                ),

        ]);
    }

    private function setFilters()
    {
        $this->crud->addFilter([
            'name' => 'product_type',
            'type' => 'select2',
            'label' => 'Тип продукта'
        ], function () {
            return [Product::ALCOHOL_TYPE=>'Алкоголь',Product::ACCESSORIES_TYPE=>'Аксессуары',Product::BOOK_TYPE=>'Книги'];
        }, function ($value) { // if the filter is active
            $this->crud->addClause('where', 'product_type', $value);

        });

        $this->crud->addFilter([
            'type'  => 'simple',
            'name'  => 'show_on_site',
            'label' => 'Выключенные с сайта'
        ],
            false,
            function() { // if the filter is active
                 $this->crud->addClause('turnedOf'); // apply the "active" eloquent scope
            } );

        $this->crud->addFilter([
            'name' => 'brand_id',
            'type' => 'select2',
            'label' => 'Бренд'
        ], function () {
            return array_merge(['-'], Brand::all()->pluck('name', 'id')->toArray());
        }, function ($value) { // if the filter is active
            if (intval($value) === 0) {
                $this->crud->addClause('where', 'brand_id', null);
            } else {
                $this->crud->addClause('where', 'brand_id', $value);
            }

        });

        $this->crud->addFilter([
            'name' => 'category_id',
            'type' => 'select2_multiple',
            'label' => 'Категория'
        ], function () {
            return array_merge(['-'], Category::all()->keyBy('id')->pluck('name', 'id')->toArray());
        }, function ($values) { // if the filter is active
            foreach (json_decode($values) as $key => $value) {
                $this->crud->query = $this->crud->query->whereHas('categories', function ($query) use ($value) {
                    $query->where('category_id', $value);
                });
            }

        });
        $this->crud->addFilter([
            'name' => 'price',
            'type' => 'range',
            'label' => 'Цена',
            'label_from' => 'min',
            'label_to' => 'max'
        ],
            false,
            function ($value) { // if the filter is active
                $range = json_decode($value);
                if ($range->from) {
                    $this->crud->addClause('where', 'price', '>=', (float)$range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'price', '<=', (float)$range->to);
                }
            });
        $this->crud->addFilter([
            'name' => 'quantity',
            'type' => 'range',
            'label' => 'Количество',
            'label_from' => 'min',
            'label_to' => 'max'
        ],
            false,
            function ($value) { // if the filter is active
                $range = json_decode($value);
                if ($range->from === $range->to) {
                    $this->crud->addClause('where', 'quantity', '=', intval($range->from));
                } else {
                    if ($range->from) {
                        $this->crud->addClause('where', 'quantity', '>=', intval($range->from));
                    }
                    if ($range->to) {
                        $this->crud->addClause('where', 'quantity', '<=', intval($range->to));
                    }
                }

            });
        $this->crud->addFilter([
            'name' => 'pack_id',
            'type' => 'select2',
            'label' => 'Упаковка'
        ], function () {
            return array_merge(['-'], Pack::all()->pluck('name', 'id')->toArray());
        }, function ($value) { // if the filter is active
            if (intval($value) === 0) {
                $this->crud->addClause('where', 'pack_id', null);
            } else {
                $this->crud->addClause('where', 'pack_id', $value);
            }

        });
        $this->crud->addFilter([
            'name' => 'region_id',
            'type' => 'select2',
            'label' => 'Регион'
        ], function () {
            return array_merge(['-'], Region::all()->sortBy('name')->pluck('name', 'id')->toArray());
        }, function ($value) { // if the filter is active
            if (intval($value) === 0) {
                $this->crud->addClause('where', 'region_id', null);
            } else {
                $this->crud->addClause('where', 'region_id', $value);
            }

        });
        $this->crud->addFilter([
            'name' => 'country_id',
            'type' => 'select2',
            'label' => 'Страна'
        ], function () {
            return array_merge(['-'], Country::all()->sortBy('name')->pluck('name', 'id')->toArray());
        }, function ($value) { // if the filter is active
            if (intval($value) === 0) {
                $this->crud->addClause('where', 'country_id', null);
            } else {
                $this->crud->addClause('where', 'country_id', $value);
            }

        });
        $this->crud->addFilter([
            'name' => 'alcohol_style_id',
            'type' => 'select2',
            'label' => 'Стиль'
        ], function () {
            return array_merge(['-'], Alcohol_style::all()->pluck('name', 'id')->toArray());
        }, function ($value) { // if the filter is active
            if (intval($value) === 0) {
                $this->crud->addClause('where', 'alcohol_style_id', null);
            } else {
                $this->crud->addClause('where', 'alcohol_style_id', $value);
            }

        });
        $this->crud->addFilter(
            [ // text filter
                'type' => 'select2_multiple',
                'name' => 'sugar_contents',
                'label' => 'С-е сахара',
            ],
            function () {
                return Sugar_content::all()->keyBy('id')->pluck('name', 'id')->toArray();
            },
            function ($values) { // if the filter is active
                foreach (json_decode($values) as $key => $value) {
                    $this->crud->query = $this->crud->query->whereHas('sugar_contents', function ($query) use ($value) {
                        $query->where('sugar_content_id', $value);
                    });
                }
            });
        $this->crud->addFilter(
            [ // text filter
                'type' => 'select2_multiple',
                'name' => 'grape_sorts',
                'label' => 'Сорт винограда',
            ],
            function () {
                return Grape_sort::all()->keyBy('id')->pluck('name', 'id')->toArray();
            },
            function ($values) { // if the filter is active
                foreach (json_decode($values) as $key => $value) {
                    $this->crud->query = $this->crud->query->whereHas('grape_sorts', function ($query) use ($value) {
                        $query->where('grape_sort_id', $value);
                    });
                }
            });
        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'harvest_year',
                'label' => 'Год сбора',
            ],
            false,
            function ($value) { // if the filter is active
                if ($value === '-') {
                    $this->crud->addClause('where', 'harvest_year', null);
                } else {
                    $this->crud->addClause('where', 'harvest_year', 'LIKE', "%$value%");
                }
            }
        );
        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'description',
                'label' => 'Описание',
                'key' => 'descr_ru',
            ],
            false,
            function ($value) { // if the filter is active
                $value=strtolower($value);
                if (in_array($value, config('app.available_locales'))) {
                    $this->crud->addClause('where', 'description->'.$value, null);
                } else {
                    $this->crud->addClause('where', 'description->ru', 'LIKE', "%$value%");
                }
            }
        );
        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'sommeliers_note',
                'label' => 'Заметка сомелье',
            ],
            false,
            function ($value) { // if the filter is active
                $value=strtolower($value);
                if (in_array($value, config('app.available_locales'))) {
                    $this->crud->addClause('where', 'sommeliers_note->'.$value, null);
                } else {
                    $this->crud->addClause('where', 'sommeliers_note', 'LIKE', "%$value%");
                }
            }
        );
        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'name',
                'label' => 'Название',
            ],
            false,
            function ($value) { // if the filter is active
                $value=strtolower($value);
                if (in_array($value, config('app.available_locales'))) {
                    $this->crud->addClause('where', 'name->'.$value, null);
                } else {
                    $this->crud->addClause('where', 'name', 'LIKE', "%$value%");
                }
            }
        );
        $this->crud->addFilter(
            [ // text filter
                'type' => 'text',
                'name' => 'translit',
                'label' => 'Транслит',
            ],
            false,
            function ($value) { // if the filter is active
                $value=strtolower($value);
                if (in_array($value, config('app.available_locales'))) {
                    $this->crud->addClause('where', 'translit->'.$value, null);
                } else {
                    $this->crud->addClause('where', 'translit', 'LIKE', "%$value%");
                }
            }
        );
    }

    public function store()
    {
        $response = $this->traitStore();
        $this->updateLinksAndFiles();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
       $this->updateLinksAndFiles();
        return $response;
    }

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}

    private function updateLinksAndFiles(){
        $productObj = new Product();
        $productObj->addConnectionsByVolums($this->crud->getCurrentEntry());
        $entry= $this->crud->getCurrentEntry();
        $dbFileWizard = new DbToFileWizard();
        if (!$entry['show_on_site']){
            $dbFileWizard->removeFromClientFiles(Product::ID_PREFIX.$entry['id']);
        }

        $dbFileWizard->getDb();
    }

    private function getAttributesAccordingToRole(){
	    $attributes=[
		    "step" => "any",
		    'readonly' =>'readonly',
		    'disabled' => 'disabled',
	    ];
	    if ( backpack_user()->hasRole('super_admin')){
		    $attributes=["step" => "any",];
	    }

	    return $attributes;
    }
}
