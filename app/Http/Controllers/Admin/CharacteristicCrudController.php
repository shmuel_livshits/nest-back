<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CharacteristicRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CharacteristicCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CharacteristicCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Characteristic::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/characteristic');
        CRUD::setEntityNameStrings('characteristic', 'characteristics');
        $this->crud->setTitle('Характеристики (Только для программистов)'); // set the Title for the create action
        $this->crud->setHeading('Характеристики'); // set the Heading for the create action
//        $this->crud->denyAccess('create');
        $this->crud->denyAccess('show');
        $this->crud->denyAccess('delete');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            ['name' => 'name', 'label' => 'Название'],
            ['name' => 'code_name', 'label' => 'Кодовое название'],

        ]);

    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(CharacteristicRequest::class);

	    $readOnly=false;
	    $disabled=false;
	    if ( !backpack_user()->hasRole('super_admin')){
		    $readOnly='readonly';
		    $disabled='disabled';
	    }

        CRUD::addFields([
            ['name' => 'name', 'label' => 'Название','type'=>'text', 'wrapper' => ['class' => 'form-group col-md-6'] ],
            [
                'name' => 'code_name',
                'label' => 'Кодовое название',
                'type'=>'text',
                'wrapper' => ['class' => 'form-group col-md-6'],
                'attributes' => [
	                $readOnly => '',
                ],
            ],
            ['name' => 'suffix', 'label' => 'Суфикс','type'=>'text', 'wrapper' => ['class' => 'form-group col-md-6']],
            [
                'name' => 'type',
                'label' => 'Тип',
                'type'=>'select2_from_array',
                'allows_null' => false,
                'options'=> [ 'id' => 'id','number'=>'number','string'=>'string'],
                'wrapper' => ['class' => 'form-group col-md-6'],
                'attributes' => [
                    $readOnly => '',
                    $disabled => '',
                ],
            ],
	        [
		        'name' => 'chars_category_id',
		        'label' => 'Размерная категория',
		        'type' => 'relationship',
		        'entity' => 'chars_category',
		        'inline_create' => true,
		        'model' => 'App\Models\Chars_category',
		        'attribute' => 'name',
		        'wrapper' => ['class' => 'form-group col-md-4'],
	        ],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();

        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}

	public function fetchChars_category()
	{
		return $this->fetch(\App\Models\Chars_category::class);
	}
}
