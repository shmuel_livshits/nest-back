<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use App\Models\Characteristic;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation {
        destroy as traitDestroy;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation { reorder as traitReorder; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation {
        show as traitShow;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Category::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/category');
        CRUD::setEntityNameStrings('Категория', 'Категории');
        $this->crud->setTitle('Категории'); // set the Title for the create action
        $this->crud->setHeading('Категории'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
	                $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
                }
            ],

        ]);
    }

    protected function setupReorderOperation()
    {
        CRUD::set('reorder.label', 'name');
        CRUD::set('reorder.max_level', 10);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(CategoryRequest::class);

        CRUD::addFields([
            [
                'name' => 'published',
                'label' => 'Опубликовать',
                'type' => 'toggle',
                'view_namespace' => 'toggle-field-for-backpack::fields',
                'tab' => 'Общая информация'
            ],
            ['name' => 'name', 'label' => 'Название', 'type' => 'text', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'Общая информация'],
            [
                'label' => 'Parent',
                'type' => 'select',
                'name' => 'parent_id',
                'entity' => 'parent',
                'attribute' => 'name',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'tab' => 'Общая информация'
            ],
            [
                'name' => 'cover',
                'label' => 'Cover',
                'type' => 'upload',
                'upload' => true,
                'wrapper' => ['class' => 'form-group col-md-4'],
                'tab' => 'Общая информация',
            ],

            ['name' => 'google_merchant_category', 'label' => 'Номер категории в google merchant', 'type' => 'number', 'wrapper' => ['class' => 'form-group col-md-4'], 'tab' => 'Google merchant info'],
            ['name' => 'google_product_type', 'label' => 'Название типа в google merchant', 'type' => 'text', 'wrapper' => ['class' => 'form-group col-md-4'], 'tab' => 'Google merchant info'],


            //characteristics
            [
                'name' => 'characteristics',
                'label' => 'Характеристики',
                'type' => 'repeatable',
                'tab' => 'Характеристики и фильтры',
                'wrapper' => ['class' => 'form-group col-md-6'],
                'fields' => [
                    [
                        'name' => 'characteristic',
                        'type' => 'select2',
                        'label' => 'Name',
                        'model' => Characteristic::class,
                        'attribute' => 'name',
                        'entity' => 'characteristic'
                    ],
                    [
                        'name' => 'main',
                        'label' => 'Главный',
                        'type' => 'checkbox'
                    ],
                    [
                        'name' => 'filter',
                        'label' => 'Фильтровать',
                        'type' => 'checkbox'
                    ],

                ],
            ],

            //SEO
            ['name' => 'page_slug', 'label' => 'Slug', 'tab' => 'SEO'],
            ['name' => 'page_title', 'type' => 'text', 'label' => 'Page title', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name' => 'page_description', 'label' => 'Page description', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name' => 'description', 'label' => 'Описание Страницы (SEO тексты)', 'type' => 'wysiwyg', 'tab' => 'SEO'],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function show($id)
    {
        CRUD::addColumns([
            ['name' => 'name', 'label' => 'Название'],
            //SEO
            ['name' => 'page_slug', 'label' => 'Slug'],
            ['name' => 'page_title', 'label' => 'Page title'],
            ['name' => 'page_description', 'label' => 'Page description'],
        ]);
        $content = $this->traitShow($id);
        return $content;
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $entry = $this->crud->getCurrentEntry();
        if (!$entry['published']) {
            $this->unpublishChildren($entry);
        }
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}

    public function reorder()
    {

        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $this->traitReorder();
    }

    private function unpublishChildren($entry)
    {
        $clientId=$entry::ID_PREFIX.$entry['id'];
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->removeFromClientFiles($clientId);
        $entry['published'] = false;
        $entry->save();
        $children = $entry->children;
        foreach ($children as $child) {
            $this->unpublishChildren($child);
        }

    }
}
