<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\IpostCityRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class IpostCityCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class IpostCityCrudController extends CrudController
{
	use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
	use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
	use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation{
		show as traitShow;
	}

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\IpostCity::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ipost-city');
	    CRUD::setEntityNameStrings('Город', 'Город');
	    $this->crud->setTitle('Город'); // set the Title for the create action
	    $this->crud->setHeading('Город'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
	    CRUD::addColumns([
		    [
			    'name' => 'name',
			    'label' => 'Название',
			    'searchLogic' => function ($query, $column, $searchTerm) {
				    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
			    }
		    ],
	    ]);
    }

	/*protected function setupReorderOperation()
	{
		CRUD::set('reorder.label', 'name');
		CRUD::set('reorder.max_level', 1);
	}*/

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
	    $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(IpostCityRequest::class);
	    CRUD::addField(
		    ['name' => 'name','type'=>'text', 'label' => 'Название', 'wrapper' => ['class' => 'form-group col-md-6']]
	    );

    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

	public function store()
	{
		$response = $this->traitStore();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function update()
	{
		$response = $this->traitUpdate();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}
}
