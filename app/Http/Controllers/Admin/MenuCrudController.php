<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MenuRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MenuCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MenuCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
		store as traitStore;
	}
	use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
		update as traitUpdate;
	}
	use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation {
		destroy as traitDestroy;
	}
	use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation { reorder as traitReorder; }
	use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Menu::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/menu');
        CRUD::setEntityNameStrings('menu', 'menus');
	    $this->crud->setTitle('Меню'); // set the Title for the create action
	    $this->crud->setHeading('Меню'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
	    CRUD::addColumns([
		    [
			    'name' => 'name',
			    'label' => 'Название',
			    'searchLogic' => function ($query, $column, $searchTerm) {
				    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
			    }
		    ],
		    [
			    'name' => 'link',
			    'label' => 'ссылка',
		    ],

	    ]);
    }

	protected function setupReorderOperation()
	{
		CRUD::set('reorder.label', 'name');
		CRUD::set('reorder.max_level', 10);
	}

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MenuRequest::class);
	    CRUD::addFields([
		    [
			    'name' => 'published',
			    'label' => 'Опубликовать',
			    'type' => 'toggle',
			    'view_namespace' => 'toggle-field-for-backpack::fields',

		    ],
		    ['name' => 'name', 'label' => 'Название', 'type' => 'text', 'wrapper' => ['class' => 'form-group col-md-6']],
		    ['name' => 'link', 'label' => 'Ссылка', 'type' => 'text', 'wrapper' => ['class' => 'form-group col-md-6']],
		    [
			    'label' => 'Parent',
			    'type' => 'select',
			    'name' => 'parent_id',
			    'entity' => 'parent',
			    'attribute' => 'name',
			    'wrapper' => ['class' => 'form-group col-md-4'],
		    ],



	    ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

	public function store()
	{
		$response = $this->traitStore();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function update()
	{
		$response = $this->traitUpdate();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}
}
