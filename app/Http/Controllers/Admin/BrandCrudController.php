<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BrandRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BrandCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BrandCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation {
        show as traitShow;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Brand::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/brand');
        CRUD::setEntityNameStrings('brand', 'brands');
        $this->crud->setTitle('Бренды'); // set the Title for the create action
        $this->crud->setHeading('Бренды'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
	                $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
                }
            ],
//            ['name' => 'name_ua', 'label' => 'Название UA'],
            ['name' => 'logo', 'label' => 'Лого', 'type' => 'image'],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(BrandRequest::class);

        CRUD::addFields([
            ['name' => 'name', 'label' => 'Название', 'type' => 'text', 'wrapper' => ['class' => 'form-group col-md-4'], 'tab' => 'Общая информация'],
            [
                'name' => 'producer_id',
                'label' => 'Производитель',
                'type' => 'relationship',
                'entity' => 'producer',
                'inline_create' => true,
                'model' => 'App\Models\Producer',
                'attribute' => 'name',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'tab' => 'Общая информация'
            ],

            ['name' => 'official_website', 'label' => 'Сайт бренда', 'type' => 'url', 'tab' => 'Общая информация', 'wrapper' => ['class' => 'form-group col-md-4'],],
            [
                'name' => 'logo',
                'label' => 'Лого',
                'type' => 'upload',
                'upload' => true,
                'wrapper' => ['class' => 'form-group col-md-4'],
                'tab' => 'Общая информация',
            ],
            [
                'name' => 'cover',
                'label' => 'Cover',
                'type' => 'upload',
                'upload' => true,
                'wrapper' => ['class' => 'form-group col-md-4'],
                'tab' => 'Общая информация',
            ],

            //SEO
            ['name' => 'page_slug', 'label' => 'Slug', 'tab' => 'SEO'],
            ['name' => 'page_title', 'type' => 'text', 'label' => 'Page title', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name' => 'page_description', 'label' => 'Page description', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name' => 'description', 'label' => 'Описание Страницы (SEO тексты)', 'type' => 'wysiwyg', 'tab' => 'SEO'],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function fetchProducer()
    {
        return $this->fetch(\App\Models\Producer::class);
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();

        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}
}


