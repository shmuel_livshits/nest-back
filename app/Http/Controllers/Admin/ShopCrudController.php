<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ShopRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ShopCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ShopCrudController extends CrudController
{
	use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
	use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
	use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation{
		show as traitShow;
	}

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Shop::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/shop');
        CRUD::setEntityNameStrings('Магазин', 'Магазины');
        $this->crud->setTitle('Магазины'); // set the Title for the create action
        $this->crud->setHeading('Магазины'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
                }
            ],
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(ShopRequest::class);

        CRUD::addFields([
            ['name' => 'name', 'label' => 'Название', 'type' => 'text', 'wrapper' => ['class' => 'form-group col-md-5']],
            [
                'name' => 'main_delivery_city_id',
                'label' => 'Город',
                'wrapper' => ['class' => 'form-group col-md-3'],
                'type' => 'relationship',
                'inline_create' => true,
                'entity' => 'main_delivery_city',
                'attribute' => 'name',
                'model' => "App\Models\Main_delivery_city",
            ],
            ['name' => 'address', 'label' => 'Адрес', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-4']],
            [   // repeatable
                'name' => 'phones',
                'label' => 'Телефоны',
                'type' => 'repeatable',
                'wrapper' => ['class' => 'form-group col-md-4'],
                'fields' => [
                    [
                        'name' => 'phone',
                        'type' => 'text',
                        'label' => 'Телефон',
                    ],
                ],

                // optional
                'new_item_label' => 'телефон', // customize the text of the button

            ],
            ['name' => 'schedule', 'label' => 'Рассписание работы', 'type' => 'wysiwyg', 'wrapper' => ['class' => 'form-group col-md-8']],
            ['name' => 'description', 'label' => 'Описание Страницы (SEO тексты)', 'type' => 'wysiwyg'],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

	public function store()
	{
		$response = $this->traitStore();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function update()
	{
		$response = $this->traitUpdate();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}

    public function fetchMain_delivery_city()
    {
        return $this->fetch(\App\Models\Main_delivery_city::class);
    }
}
