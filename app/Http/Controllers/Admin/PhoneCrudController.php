<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PhoneRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PhoneCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PhoneCrudController extends CrudController
{
	use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
	use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
	use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation{
		show as traitShow;
	}


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Phone::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/phone');
        CRUD::setEntityNameStrings('телефон', 'телефоны');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
	    CRUD::addColumns([
		    [
			    'name' => 'phone',
			    'label' => 'Телефон'
		    ],
		    [
			    'name' => 'name',
			    'label' => 'Название',
			    'searchLogic' => function ($query, $column, $searchTerm) {
				    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
			    }
		    ],
	    ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
	    $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(PhoneRequest::class);
	    CRUD::addFields([
		    ['name' => 'phone', 'label' => 'Телефон', 'type'=>'text', 'wrapper' => ['class' => 'form-group col-md-6']],
		    ['name' => 'name', 'label' => 'Название', 'type'=>'text', 'wrapper' => ['class' => 'form-group col-md-6']],

		    [
			    'name' => 'shop_regions',
			    'label' => 'Регион',
			    'wrapper' => ['class' => 'form-group col-md-4'],
			    'type'=> 'relationship',
//			    'inline_create'=>true,
			    'ajax' => true,
			    'data_source' => url("api/ajax-search/shop-regions"),
			    'entity'=>'shop_regions',
			    'attribute'=>'name',
			    'model'=> "App\Models\ShopRegion",
		    ],

	    ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

	public function fetchShop_regions()
	{
		return $this->fetch(\App\Models\ShopRegion::class);
	}

	public function store()
	{
		$response = $this->traitStore();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function update()
	{
		$response = $this->traitUpdate();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}
}
