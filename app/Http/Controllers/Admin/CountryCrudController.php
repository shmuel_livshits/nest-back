<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CountryRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CountryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CountryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation{
        show as traitShow;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Country::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/country');
        CRUD::setEntityNameStrings('Страна', 'страны');
        $this->crud->setTitle('Страны'); // set the Title for the create action
        $this->crud->setHeading('Страны'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
                }
            ],
            ['name'=>'iso','label'=>'iso']
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(CountryRequest::class);

        CRUD::addFields([
            ['name' => 'name', 'label' => 'Название', 'type'=>'text', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'Общая информация'],
            ['name'=>'iso','label'=>'iso', 'type'=>'text', 'wrapper' => ['class' => 'form-group col-md-3'], 'tab' => 'Общая информация'],
            [
                'name'=>'flag',
                'label'=>'Флаг',
                'type' => 'upload',
                'upload' => true,
                'wrapper' => ['class' => 'form-group col-md-3'],
                'tab' => 'Общая информация'],

            //SEO
            ['name' => 'page_slug', 'label' => 'Slug', 'type'=>'text', 'tab' => 'SEO'],
            ['name' => 'page_title', 'label' => 'Page title', 'type'=>'text', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
            ['name' => 'page_description', 'label' => 'Page description', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function show($id)
    {
        CRUD::addColumns([
            ['name' => 'name', 'label' => 'Название'],
            ['name' => 'country_id', 'label' => 'Страна',],
            //SEO
            ['name' => 'page_slug', 'label' => 'Slug'],
            ['name' => 'page_title', 'label' => 'Page title'],
            ['name' => 'page_description', 'label' => 'Page description'],
        ]);
        $content = $this->traitShow($id);
        return $content;
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();

        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}
}
