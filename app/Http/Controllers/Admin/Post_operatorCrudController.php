<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Post_operatorRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class Post_operatorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class Post_operatorCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation {
        show as traitShow;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Post_operator::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/post_operator');
        CRUD::setEntityNameStrings('Почтовый оператор', 'Почтовые операторы');
        $this->crud->setTitle('Почтовые операторы'); // set the Title for the create action
        $this->crud->setHeading('Почтовые операторы'); // set the Heading for the create action
        /*$this->crud->denyAccess('create');
        $this->crud->denyAccess('show');
        $this->crud->denyAccess('delete');*/
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumns([
            [
                'name' => 'name',
                'label' => 'Название',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
                }
            ],
        ]);
    }
    protected function setupReorderOperation()
    {
        CRUD::set('reorder.label', 'name');
        CRUD::set('reorder.max_level', 1);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(Post_operatorRequest::class);

        CRUD::setValidation(Post_operatorRequest::class);
        CRUD::addFields([
            ['name' => 'name', 'label' => 'Название', 'type' => 'text', 'wrapper' => ['class' => 'form-group col-md-4']],
            [
                'name' => 'api_name_param',
                'label' => 'api_name_param',
                'type' => 'text',
                'wrapper' => ['class' => 'form-group col-md-2'],
                'attributes' => [
//                    'readonly' => 'readonly',
                ]],
            ['name' => 'dispatch_time', 'label' => 'Время разделения', 'type' => 'time', 'wrapper' => ['class' => 'form-group col-md-2']],
            ['name' => 'price_for_free', 'label' => 'Цена разделения', 'type' => 'number', 'wrapper' => ['class' => 'form-group col-md-3']],
            [
                'name' => 'logo',
                'label' => 'Лого оператора',
                'type' => 'upload',
                'upload' => true,
                'wrapper' => ['class' => 'form-group col-md-6']
            ],
            ['name' => 'message', 'type' => 'text', 'label' => 'Сообщение(отправим сегодня)', 'wrapper' => ['class' => 'form-group col-md-6']],
            ['name' => 'free_message', 'type' => 'text', 'label' => 'Сообщение если бесплатно', 'wrapper' => ['class' => 'form-group col-md-6']],
            ['name' => 'message_later', 'type' => 'text', 'label' => 'Сообщение (отправим завтра)', 'wrapper' => ['class' => 'form-group col-md-6']],
            ['name' => 'not_free_message', 'type' => 'text', 'label' => 'Сообщение если не бесплатно', 'wrapper' => ['class' => 'form-group col-md-6']],
            [
                'name' => 'isActive',
                'label' => 'Вкл/Выкл',
                'type' => 'toggle',
                'view_namespace' => 'toggle-field-for-backpack::fields',
            ],

        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}
}
