<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ShopRegionRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
/**
 * Class ShopRegionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ShopRegionCrudController extends CrudController
{
	use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
		store as traitStore;
	}
	use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
		update as traitUpdate;
	}
	use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation {
		destroy as traitDestroy;
	}
	use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
//	use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation { reorder as traitReorder; }
	use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation {
		show as traitShow;
	}


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ShopRegion::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/shop_regions');
        CRUD::setEntityNameStrings('Регион', 'Регионы');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
	    CRUD::addColumns([
		    [
			    'name' => 'name',
			    'label' => 'Название',
			    'searchLogic' => function ($query, $column, $searchTerm) {
				    $query->orWhereRaw("LOWER(JSON_EXTRACT(name, '$.ru')) like '%".mb_strtolower($searchTerm)."%'");
			    }
		    ],
		    [
			    'name'  => 'city_type',
			    'label'=>'Тип города'
		    ]
	    ]);
    }

	/* закоменторавал реордеринг руками потому что там 7000 городов и сел. Все зависнет
	 * protected function setupReorderOperation()
	{
		CRUD::set('reorder.label', 'name');
		CRUD::set('reorder.max_level', 10);
	}*/

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
	    $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(ShopRegionRequest::class);

	    CRUD::addFields([
		    ['name' => 'name', 'label' => 'Название', 'type'=>'text', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'Общая информация'],
		    [
			    'label' => 'Parent',
			    'type' => 'relationship',
			    'name' => 'parent_id',
			    'entity' => 'parent',
			    'ajax' => true,
			    'data_source' => url("api/ajax-search/shop-regions"),
			    'attribute' => 'name',
			    'wrapper' => ['class' => 'form-group col-md-4'],
			    'tab' => 'Общая информация'
		    ],
		    [
			    'name'  => 'city_type',
			    'label' => 'Тип города',
			    'type'  => 'select_from_array',
			    'options'     => [0 => 'Обычный', 1 => 'Популярный',2=>'Главный'],
			    'allows_null' => false,
			    'default'     => 0,
                'wrapper' => ['class' => 'form-group col-md-4'],
			    'tab' => 'Общая информация'
		    ],


		    //SEO
		    ['name' => 'page_slug', 'label' => 'Slug', 'tab' => 'SEO'],
		    ['name' => 'page_title', 'type'=>'text', 'label' => 'Page title', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
		    ['name' => 'page_description', 'label' => 'Page description', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO'],
	    ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

	public function store()
	{
		$response = $this->traitStore();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function update()
	{
		$response = $this->traitUpdate();
		$dbFileWizard = new DbToFileWizard();
		$dbFileWizard->getDb();
		return $response;
	}

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}

}
