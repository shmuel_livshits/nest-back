<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SommelierRequest;
use App\Nest\Service\DbToFileWizard;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SommelierCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SommelierCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation {
        show as traitShow;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Sommelier::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/sommelier');
        CRUD::setEntityNameStrings('sommelier', 'sommeliers');
        $this->crud->setTitle('Сомелье'); // set the Title for the create action
        $this->crud->setHeading('Сомелье'); // set the Heading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
	        CRUD::addColumns([
            [
                'name' => 'last_name',
                'label' => 'Фамилия',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('last_name->ru', 'like', '%' . $searchTerm . '%');
                }
            ],
            [
                'name' => 'first_name',
                'label' => 'Имя',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->orWhere('first_name->ru', 'like', '%' . $searchTerm . '%');
                }
            ],
            ['name' => 'experience', 'label' => 'Опыт'],
            ['name' => 'avatar', 'label' => 'Фото', 'type' => 'image', 'height' => '100px', 'width' => '100px']
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setSubheading('добавить/редактировать'); // set the Subheading for the create action
        CRUD::setValidation(SommelierRequest::class);

        CRUD::addField(['name' => 'first_name', 'label' => 'Имя', 'type' => 'text', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'Информация о сомелье']);
        CRUD::addField(['name' => 'last_name', 'label' => 'Фамилия', 'type' => 'text', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'Информация о сомелье']);
        CRUD::addField([
            'name' => 'avatar',
            'label' => 'Фото',
            'type' => 'upload',
            'upload' => true,
            'wrapper' => ['class' => 'form-group col-md-10'],
            'tab' => 'Информация о сомелье'
        ]);
        CRUD::addField(['name' => 'experience', 'label' => 'Работает с года', 'type' => 'number', 'wrapper' => ['class' => 'form-group col-md-2'], 'tab' => 'Информация о сомелье']);
        CRUD::addField(['name' => 'description', 'label' => 'особенности', 'type' => 'tinymce', 'tab' => 'Информация о сомелье']);
        CRUD::addField(['name' => 'page_slug', 'label' => 'Slug', 'tab' => 'SEO']);
        CRUD::addField(['name' => 'page_title', 'label' => 'Page title', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO']);
        CRUD::addField(['name' => 'page_description', 'label' => 'Page description', 'type' => 'textarea', 'wrapper' => ['class' => 'form-group col-md-6'], 'tab' => 'SEO']);

    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $response = $this->traitStore();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

    public function update()
    {
        $response = $this->traitUpdate();
        $dbFileWizard = new DbToFileWizard();
        $dbFileWizard->getDb();
        return $response;
    }

	public function destroy($id)
	{
		$entry= $this->crud->getCurrentEntry();
		$dbFileWizard=new DbToFileWizard();
		$model=$this->crud->getModel();
		$dbFileWizard->removeFromClientFiles($model::ID_PREFIX.$entry->id);
		return $this->crud->delete($id);
	}

}
