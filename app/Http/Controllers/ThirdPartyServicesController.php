<?php

namespace App\Http\Controllers;

use App\ThirdPartyServices\ESputnik;
use App\ThirdPartyServices\SalesDrive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ThirdPartyServicesController extends Controller {
	public function salesDriveWebhook( Request $request ) {
		$salesDriveResponce = $request->all();
		$esputnik = new ESputnik();
		$phoneNumber=$salesDriveResponce['data']['contacts'][0]['phone'][0];
		$salesDriveOrderId=$salesDriveResponce['data']['externalId'];
		$esputnikMessageId =$esputnik->getTextSms($salesDriveResponce['info']['webhookEvent'], $salesDriveResponce['meta']['fields']['statusId']['options'][0]['text'],$salesDriveResponce);
		$salesDrive= new SalesDrive();
		$ttn=$salesDrive->getDeliveryNumber($salesDriveResponce);
		try{
			$esputnik->smartSendSms($salesDriveOrderId,$phoneNumber,$esputnikMessageId,$ttn);
		}
		catch (\Exception $exception){
			Log::error(json_encode($exception));
		}
		return response( 'success' );

	}




}
