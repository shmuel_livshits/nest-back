<?php

namespace App\Http\Controllers;

use App\Models\BlackListIp;
use App\Models\Category;
use App\Models\Characteristic;
use App\Models\Phone;
use App\Models\Product;
use App\Models\ShopRegion;
use App\Models\Sign;
use App\Models\Sommelier;
use App\Nest\Images\ImageService;
use App\Nest\Images\TinifyImages;
use App\Nest\Service\DbToFileWizard;
use App\Nest\Service\Feeds;
use App\ThirdPartyServices\ESputnik;
use App\ThirdPartyServices\NovaPochta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class TestController extends Controller {
	public function __construct( Request $request ) {

		if ( in_array( $request->lang, config( 'app.available_locales' ) ) ) {
			app()->setLocale( $request->lang );
		}
	}

	public function test( Request $request ) {
		dd(Carbon::now()->timestamp);



	}

	public function cleanImage() {
		$originals=collect(Storage::disk('winelibrary')->allFiles('originals'));
		$preparedAll=collect(Storage::disk('winelibrary')->allFiles());
		$neededToFixCollection=collect([]);
		foreach ($originals as  $original){
			$isExits=$preparedAll->filter(function ($item) use ($original){
				if (strpos($original,$item)!==false){
					return $item;
				}
			});
			if ($isExits->count()===1){
				$neededToFixCollection->push($original);
			}
		}
		foreach ($neededToFixCollection as $imgToFix){
			$imagesProps=config('filesystems.disks.winelibrary.product');
			$watermarkProps=config('filesystems.disks.winelibrary.watermark');
			$imageName=substr($imgToFix,strpos($imgToFix,'/')+1);
			$filename=substr($imageName,0,strpos($imageName,'.'));
			$originalExtension=substr($imageName,strpos($imageName,'.')+1);
			foreach ($imagesProps as $imageProperty) {
				$sourceImg=Storage::disk('winelibrary')->get($imgToFix);
				$rightSizeImage = Image::make($sourceImg)->resize($imageProperty['width'], $imageProperty['height'], function ($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				});
				if ($watermarkProps !== null) {
					$waterMarkWidth=$imageProperty['height'] * $watermarkProps['width'];
					$imagService= new ImageService();
					$imagService->insertWatermark($waterMarkWidth,$rightSizeImage);
				}
				$tinifyImage = new TinifyImages();
				$minifiedImage = $tinifyImage->compressImage($rightSizeImage);
				Storage::disk('winelibrary')->put($imageProperty['shortName'].$filename.'.'.$originalExtension, $minifiedImage->stream($originalExtension));
				Storage::disk('winelibrary')->put($imageProperty['shortName'].$filename.'.webp', $minifiedImage->stream('webp'));
			}
		}

	}

	public function cleanProductsImages(Request $request){

	}

}
