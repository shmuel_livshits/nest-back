<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Nest\Service\DbToFileWizard;
use App\Nest\Service\Slug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StorageSynchronizationController extends Controller
{
    public function __construct(Request $request)
    {
        if (in_array($request->lang, config('app.available_locales'))) {
            app()->setLocale($request->lang);
        }
    }
    public function saveProductsInfo(Request $request)
    {
        if ($request->apikey !== '1Ldj7sA#K_?S@hs*dkU3Hg<%!') {
            return response('wrong key', 403);
        }
        $newProducts = $request->products;

        foreach ($newProducts as $newProduct) {
            $product = Product::where('sku', $newProduct['sku'])->first();
            if ($product === null) {
                $product = new Product();
	            $product->page_title = mb_substr($newProduct['name_ru'], 0, 69, "utf-8");
	            $product->page_slug = Slug::translitAndSlugify(mb_substr(Slug::translit($newProduct['name_ru']), 0, 69, "utf-8"));
            }

            $product->name = $newProduct['name_ru'];
            $product->product_type=$newProduct['category'];
            $product->sku = $newProduct['sku'];
            $product->barcode = $newProduct['barcode'];
            $product->quantity = $newProduct['quantity'];
            $product->seller = $newProduct['seller'];
            $product->price = $newProduct['price'];
	        $product->old_price = ($newProduct['old_price']===null)?null : $newProduct['old_price'];

            if ($newProduct['category'] !== Product::BOOK_TYPE) {
                $product->quantity_in_pack = $newProduct['quantity_in_pack'];
            }
            if ($newProduct['vol'] !== null) {
                $product->volume = doubleval($newProduct['vol']);
            }
            try {
                $product->save();

            } catch (\Exception $e) {
            	Log::error($e);
                return $e;
            }
        }
        $dbFile= new DbToFileWizard();
        $dbFile->getDb();
        return response('success', 200);

    }
}
