<?php

namespace App\Http\Controllers;

use App\Nest\Service\Feeds;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FeedsController extends Controller
{
    private $lang;
    private $feeds;

    public function __construct(Request $request)
    {
        if (in_array($request->lang, config('app.available_locales'))) {
            app()->setLocale($request->lang);
        }
        $this->feeds= new Feeds();
    }

    public function alloFeeds(){
        $feeds= new Feeds();
        if (Storage::exists($feeds->alloPath)){
            $xmlFile=Storage::get($feeds->alloPath);
            return response($xmlFile,200)->header('Content-Type','application/xml');
        }
        else{
            return response('Xml не сгенерирован. Зайдите через 15 минут');
        }
    }

    public function salesDriveFeeds(){
        $feeds= new Feeds();
        if (Storage::exists($feeds->salesDivePath)){
            $xmlFile=Storage::get($feeds->salesDivePath);
            return response($xmlFile,200)->header('Content-Type','application/xml');
        }
        else{
            return response('Xml не сгенерирован. Зайдите через 15 минут');
        }
    }

    public function alcoBizFeeds(){
        $feeds= new Feeds();
        if (Storage::exists($feeds->alcoBizPath)){
            $xml=Storage::get($feeds->alcoBizPath);
            return response($xml,200)->header('Content-Type','application/xml');
        }
        else{
            return response('Xml не сгенерирован. Зайдите через 15 минут');
        }
    }

    public function multisearchFeeds($lang=null){
        if (Storage::exists($this->feeds->searchPath)){
            $xmlFile=Storage::get($this->feeds->searchPath);
            return response($xmlFile,200)->header('Content-Type','application/xml');
        }
        else{
            return response('Xml не сгенерирован. Зайдите через 15 минут');
        }
    }

    public function googleMerchantFeeds(){
        if (Storage::exists($this->feeds->googleMerchantPath)){
            $xmlFile=Storage::get($this->feeds->googleMerchantPath);
            return response($xmlFile,200)->header('Content-Type','application/xml');
        }
        else{
            return response('Xml не сгенерирован. Зайдите через 15 минут');
        }

    }
    public function facebookMerchantFeeds(){
        if (Storage::exists($this->feeds->facebookMerchantPath)){
            $xmlFile=Storage::get($this->feeds->facebookMerchantPath);
            return response($xmlFile,200)->header('Content-Type','application/xml');
        }
        else{
            return response('Xml не сгенерирован. Зайдите через 15 минут');
        }
    }
}
