<?php

namespace App\Http\Controllers;

use App\Models\Main_delivery_city;
use App\Models\Post_operator;
use App\Models\Shop;
use App\ThirdPartyServices\Justin;
use App\ThirdPartyServices\NovaPochta;
use Illuminate\Http\Request;

class PostOperatorsController extends Controller
{
    private $novaPoshta;
    private $justin;
    private $locale;
    public function __construct(Request $request)
    {
        if (in_array($request->lang, config('app.available_locales'))) {
            app()->setLocale($request->lang);
        }
        $this->locale=app()->getLocale();
        $this->novaPoshta = new NovaPochta($this->locale);
        $this->justin = new Justin($this->locale);
    }

    public function getCitiesList()
    {
        return $this->novaPoshta->getCitiesNames();
    }

    public function getMainCitiesList()
    {
        $cites = Main_delivery_city::getItems();
        $preparedNames=[];
        foreach ($cites as $city){
            array_push($preparedNames,$city->name);
        }
        return response($preparedNames);
        }

    public function getWarehouses(Request $request)
    {
        if ($request->postOperator === 'np') { // эти значения я сам добавляю в базу
            return response($this->novaPoshta->getWarehouses($request->cityName));
        }elseif ($request->postOperator === 'npk') { // эти значения я сам добавляю в базу
            return response($this->novaPoshta->getNovaPoshtaCourier($request->cityName));
        } elseif ($request->postOperator === 'justin') {
            return response($this->justin->getWarehouses($request->cityName));
        }elseif ($request->postOperator==='self'){
            return response(Post_operator::getPickUpPlaces($request->cityName));
        }
        elseif ($request->postOperator==='shop_delivery'){
            return response(Post_operator::isDeliveryWorks($request->cityName));
        }
        elseif ($request->postOperator==='sam'){
            return response(Post_operator::isPickupWorks($request->cityName));
        }
        elseif ($request->postOperator==='ipost'){
	        return response(Post_operator::isIpostWorksInCity($request->cityName));
        }

    }

}
