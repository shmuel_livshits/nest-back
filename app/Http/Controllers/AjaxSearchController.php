<?php

namespace App\Http\Controllers;

use App\Models\ShopRegion;
use Illuminate\Http\Request;

class AjaxSearchController extends Controller
{
	private $lang='';
	public function __construct(Request $request)
	{
		if (in_array($request->lang, config('app.available_locales'))) {
			app()->setLocale($request->lang);
			$this->lang=$request->lang;
		}
		else{
			$this->lang=config('app.fallback_locale');
		}
	}
	public function shopRegions(Request $request)
	{
		$search_term = $request->input('q');

		if ($search_term)
		{
			$results = ShopRegion::where('name->'.$this->lang, 'LIKE', $search_term.'%')->paginate(10);
		}
		else
		{
			$results = ShopRegion::paginate(10);
		}

		return response()->json($results,200,[],JSON_UNESCAPED_UNICODE);
	}
}
