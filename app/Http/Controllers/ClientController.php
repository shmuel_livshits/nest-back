<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Site_Page;
use App\Nest\Service\DbToFileWizard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    public function __construct(Request $request)
    {
        if (in_array($request->lang, config('app.available_locales'))) {
            app()->setLocale($request->lang);
        }
    }

    public function getDb()
    {
    	$filePath=DbToFileWizard::dirName.app()->getLocale().'/'.DbToFileWizard::fileName;
        $mainArray = Storage::disk('dbfiles')->get($filePath);
        return response($mainArray, 200);
    }
}
