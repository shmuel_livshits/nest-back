<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class PhoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    $id =request()->request->get('id');
	    $phone=request()->request->get('phone');
	    return [
		    'name' => 'min:2|max:50|unique:App\Models\Phone,name,'.$id,
		    'phone' => 'required|min:2|max:50|unique:App\Models\Phone,name,'.$phone,
	    ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
	    return  config('formRequests.formErrorsMessages');
    }
}
