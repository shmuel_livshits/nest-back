<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id =request()->request->get('id');
        return [
            'quantity_in_pack'=>'digits_between:0,100000',
            'aging_type'=>'max:255',
            'degree' => 'max:7',
            'page_title' => 'required',
            'page_slug' => 'required|min:1|max:255|regex:/^[A-Za-z0-9 _-]{1,100}$/i|unique:App\Models\Product,page_slug,'.$id,
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return  config('formRequests.formErrorsMessages');
    }
}
