<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id =request()->request->get('id');
        return [
            'name' => 'required|min:2|max:50|unique:App\Models\Author,name,'.$id,

            'page_title' => 'required|min:1|max:70',
            'page_description'=>'max:255',
            'page_slug' => 'required|min:1|max:255|regex:/^[A-Za-z0-9 _-]{1,100}$/i|unique:App\Models\Author,page_slug,'.$id,
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return  config('formRequests.formErrorsMessages');
    }
}
