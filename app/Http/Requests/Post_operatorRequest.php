<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class Post_operatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id =request()->request->get('id');
        $required=null;
        if ($id===null){
            $required='required|';
        }
        $logo='';
        if (request()->request->get('logo')!==null){
            $logo='mimes:jpg,svg,png';
        }

        return [
            'name' => 'required|min:5|max:255|unique:App\Models\Post_operator,name,'.$id,
            'logo'=> $required.$logo,
            'dispatch_time'=>'required',
            'price_for_free'=>'required',
            'api_name_param'=>'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return  config('formRequests.formErrorsMessages');
    }
}
