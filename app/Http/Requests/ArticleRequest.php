<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id =request()->request->get('id');
        $cover='';
        if (request()->request->get('cover')!==null){
            $cover='mimes:jpg,svg,png';
        }
        return [
            'name' => 'required|min:2|max:50|unique:App\Models\Site_Page,name,'.$id,
            'description'=>'required',
            'page_title' => 'required|min:1|max:70',
            'cover'=>$cover,
            'page_description'=>'max:255',
//            'page_slug' => 'required|min:1|max:255|regex:/^[A-Za-z0-9 _-]{1,100}$/i|unique:App\Models\Site_Page,page_slug,'.$id,
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return  config('formRequests.formErrorsMessages');
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
