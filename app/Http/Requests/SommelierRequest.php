<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class SommelierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id =request()->request->get('id');
        $required=null;
        if ($id===null){
            $required='required|';
        }
        return [
            'first_name' => 'required|min:2|max:50',
            'last_name' => 'required|min:2|max:50',
            'avatar' => $required.'file|mimes:jpg,png',
            'description' => 'required',
            'page_title' => 'required|min:1|max:70',
            'page_description'=>'max:255',
            'page_slug' => 'required|min:1|max:255|regex:/^[A-Za-z0-9 _-]{1,100}$/i|unique:App\Models\Sommelier,page_slug,'.$id,
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return  config('formRequests.formErrorsMessages');
    }
}
