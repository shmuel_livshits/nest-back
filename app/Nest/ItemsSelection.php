<?php

namespace App\Nest;

use App\Models\Alcohol_style;
use App\Models\Author;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Characteristic;
use App\Models\Combination;
use App\Models\Country;
use App\Models\Grape_sort;
use App\Models\Marking;
use App\Models\Pack;
use App\Models\Producer;
use App\Models\Product;
use App\Models\Provider;
use App\Models\Publisher;
use App\Models\Region;
use App\Models\ShopRegion;
use App\Models\Sign;
use App\Models\Sommelier;
use App\Models\Sugar_content;
use App\Models\Tag;
use App\Models\Wine_type;
use Illuminate\Support\Facades\Schema;

class ItemsSelection {
	private $mappingArr;

	public function __construct() {
		$this->mappingArr = [
			'N'     => 'name',
			'S'     => ['page_slug','slug'],
			'D'=>'description',
			'T'     => 'page_title',
			'K'     => 'page_description',
			'U'     => ['popularity','lft','city_type'],
			'$'     => 'price',
			'%'     => 'old_price',
			'iso'   => 'iso',
			'A'     => 'sku',
			'vol'=>'volume',
			'Q'     => 'quantity',
			'J'=>['suffix','phone'],
			'V'=>'type',
			'trans' => 'translit',
			'L'=>'link',

			'G'   => 'isPresent',
			'Y'=>'new',
			'TOP'=>'top',
			'H'=>'hot',
			'seller' => 'seller',
			'abv'    => 'degree',
			'age'    => 'alcohol_aging',
			'at'     => 'aging_type',
			'hy'     => 'harvest_year',
			'CN'=>'code_name', // значение из характеристик

			'py'             => 'publish_year',
			'lang'           => 'language',
			'prev'           => 'preview_pictures',
			'vis'            => 'published',
			'exp'            => 'experience',
			'isOwnImport'    => 'isOwnImport',
			//Параметры для почты
			'fname'          => 'first_name',
			'lname'          => 'last_name',
			'msg_price_more' => 'free_message',
			'msg_price_less' => 'not_free_message',
			'msg_after'      => 'message_later',
			'msg_before'     => 'message',
			'time'           => 'dispatch_time',
			'price_for_free' => 'price_for_free',
			'api_name_param' => 'api_name_param', //для определения способа отправки
		];

	}

	public function getItems(
		$obj, $dataUpdateTime = null, array $ids = [],
		bool $isFullInfo = false
	) {
		if ( count( $ids ) > 0 ) {
			$items = $obj::whereIn( 'id', $ids )->get();
		} elseif ( $dataUpdateTime === null ) {

			if ( Schema::hasColumn( $obj->getTable(), 'published' ) ) {
				$items = $obj::where( 'published', true )->get();
			} else {
				$items = $obj::all();
			}

		} else {

			$items = $obj::where( 'updated_at', '>=', $dataUpdateTime )->get();

		}

		return $this->getItemsCommon( $items, $obj, $isFullInfo );
	}

	public function getProducts( $obj, $dataUpdateTime = null, array $ids = [], bool $isFullInfo = false ) {
		if ( count( $ids ) > 0 ) {
			$items = $obj::whereIn( 'id', $ids )->orderBy('popularity', 'desc')
                         ->orderBy( 'id' )->get();
		} elseif ( $dataUpdateTime === null ) {
			$items = $obj::where( 'show_on_site', true )->orderBy('popularity', 'desc')->orderBy( 'id' )->get();

		} else {
			$items = $obj::where(
				[
					[ 'updated_at', '>=', $dataUpdateTime ],
					[ 'show_on_site', true ]
				] )
				->orderBy('popularity', 'desc')->orderBy( 'id' )->get();
		}

		return $this->getItemsCommon( $items, $obj, $isFullInfo );
	}

	public function getItemsCommon( $items, $selectedObject ) {
		$preparedItems = [];

		foreach ( $items as $item ) {
			if (isset($item['published']) && $item['published']===false ){
				continue;
			}
			$preparedItem      = [];
			$preparedItem['I'] = $selectedObject::ID_PREFIX . $item['id'];
			foreach ( $this->mappingArr as $key => $value ) {
				$char=Characteristic::where('code_name',$key)->first();
				if($char!==null){
					$key=$char->code_name;
				}
				if (gettype($value)!=='array'){
					if ( isset( $item[ $value ] ) && $item[ $value ] != false ) {
						$preparedItem[ $key ] = $item[ $value ];
					}
				}
				else{
					foreach ($value as $valueItem){
						if ( isset( $item[ $valueItem ] ) && $item[ $valueItem ] != false ) {
							$preparedItem[ $key ] = $item[ $valueItem ];
						}
					}
				}
			}
			if ( $item['alcohol_style_id'] !== null ) {
				$char=Characteristic::where('code_name',Alcohol_style::ID_PREFIX)->first();
				$preparedItem[ $char->code_name] = Alcohol_style::ID_PREFIX . $item['alcohol_style_id'];
			}
			if ( $item::CLIENT_FILE_ID === Category::CLIENT_FILE_ID ) {
				$preparedItem['X'] = $this->getCategoryChars( $item->characteristics );
			}
			if ( $item->tags !== null ) {
				$preparedItem[ Tag::CLIENT_FILE_ID ] = Tag::prepareIdsForClient( $item->tags->all() );
			}
			if ( $item['available_on_request'] == true && $item['quantity'] == 0 ) {
				$preparedItem['req'] = $item['available_on_request'];
			}
			if ( $item['out_of_production'] == true ) {
				unset( $preparedItem['$'] );
				unset( $preparedItem['%'] );
				$preparedItem['Q'] = - 1;
			}
			if ( $item['country_id'] !== null ) {
				$char=Characteristic::where('code_name',Country::CLIENT_FILE_ID)->first();
				$preparedItem[$char->code_name ] = Country::ID_PREFIX . $item['country_id'];
			}
			if ( $item['region_id'] ) {
				$char=Characteristic::where('code_name',Region::CLIENT_FILE_ID)->first();
				$preparedItem[$char->code_name] = Region::ID_PREFIX . $item['region_id'];
			}
			if ( $item['brand_id'] !== null ) {
				$char=Characteristic::where('code_name',Brand::ID_PREFIX)->first();
				$preparedItem[ $char->code_name ] = Brand::ID_PREFIX . $item['brand_id'];
			}
			if ( $item['serving_temperature_from'] !== null && $item['serving_temperature_to'] !== null ) {
				$char=Characteristic::where('code_name','st')->first();
				$preparedItem[ $char->code_name] = $item['serving_temperature_from'] . ' - ' . $item['serving_temperature_to'];
			}
			if ( $item['marking_id'] !== null ) {
				$char=Characteristic::where('code_name',Marking::CLIENT_FILE_ID)->first();
				$preparedItem[ $char->code_name ] = Marking::ID_PREFIX . $item['marking_id'];
			}
			if ( $item['pack_id'] !== null ) {
				$char=Characteristic::where('code_name',Pack::CLIENT_FILE_ID)->first();
				$preparedItem[ $char->code_name ] = Pack::ID_PREFIX . $item['pack_id'];
			}
			if ( $item['wine_type_id'] !== null ) {
				$char= Characteristic::where('code_name',Wine_type::CLIENT_FILE_ID)->first();
				if ($char!==null){
					$preparedItem[$char->code_name] = Wine_type::ID_PREFIX . $item['wine_type_id'];
				}
			}
			if ( $item['publisher_id'] !== null ) {
				$char=Characteristic::where('code_name',Publisher::ID_PREFIX)->first();
				$preparedItem[ $char->code_name ] = Publisher::ID_PREFIX . $item['publisher_id'];
			}
			$sugar           = ItemsSelection::getAttrIds( $items, $item, new Sugar_content() );
			$grape           = ItemsSelection::getAttrIds( $items, $item, new Grape_sort() );
			$combine         = ItemsSelection::getAttrIds( $items, $item, new Combination() );
			$autors          = ItemsSelection::getAttrIds( $items, $item, new Author() );
			$dirs            = ItemsSelection::getAttrIds( $items, $item, new Category() );
			$signs=ItemsSelection::getAttrIds( $items, $item, new Sign() );
			$connectionsVols = $this->getLinked( $item, 'volums' );
			$accessories     = $this->getLinked( $item, 'accessories' );
			$analogs         = $this->getLinked( $item, 'analogs' );
			if ($signs!=null){
				$signsIds=collect($signs)->map(function ($value){
					return  (int)filter_var($value, FILTER_SANITIZE_NUMBER_INT);
				});
				$signsObjs=Sign::whereIn('id',$signsIds)->get();
				foreach ($signsObjs as $sign){
					$preparedItem[$sign->code_name]=1;
				}
			}
			if ( $analogs !== null ) {
				$preparedItem[ Product::LINKED_PRODUCTS ]['anlgs'] = $analogs;
			}
			if ( $connectionsVols !== null ) {
				$preparedItem[ Product::LINKED_PRODUCTS ]['vols'] = $this->sortByVolsDesc( $connectionsVols );
			}
			if ( $accessories !== null ) {
				$preparedItem[ Product::LINKED_PRODUCTS ]['accs'] = $accessories;
			}
			if ( $dirs !== null ) {
				$preparedItem[ Category::CLIENT_FILE_ID ] = $dirs[0];
			}
			if ( $sugar !== null ) {
				$char=Characteristic::where('code_name',Sugar_content::CLIENT_FILE_ID)->first();
				$preparedItem[$char->code_name  ] = $sugar;
			}
			if ( $grape !== null ) {
				$char=Characteristic::where('code_name',Grape_sort::CLIENT_FILE_ID)->first();
				$preparedItem[ $char->code_name ] = $grape;
			}
			if ( $combine !== null ) {
				$char=Characteristic::where('code_name',Combination::CLIENT_FILE_ID)->first();
				$preparedItem[ $char->code_name] = $combine;
			}
			if ( $autors !== null ) {
				$char=Characteristic::where('code_name',Author::CLIENT_FILE_ID)->first();
				$preparedItem[ $char->code_name] = $autors;
			}

			if ( isset( $item['provider_id'] ) ) {
				$preparedItem[ Provider::CLIENT_FILE_ID ] = Provider::ID_PREFIX . $item['provider_id'];
			}
			if ( isset( $item['producer_id'] ) ) {
				$preparedItem[ Producer::CLIENT_FILE_ID ] = Producer::ID_PREFIX . $item['producer_id'];
			}
			if ( isset( $item['shop_regions_id'] ) ) {
				$preparedItem[ ShopRegion::CLIENT_FILE_ID ] = ShopRegion::ID_PREFIX . $item['shop_regions_id'];
			}

			if ( ! empty( $item['image'] ) ) {
				$preparedItem['M'] = [ ItemsSelection::getImageExtension( $item['image'] ) ];
			}
			if ( ! empty( $item['flag'] ) ) {
				$preparedItem['flag'] = ItemsSelection::getImageExtension( $item['flag'] );
			}
			if ( ! empty( $item['logo'] ) ) {
				$preparedItem['LG'] = ItemsSelection::getImageExtension( $item['logo'] );
			}
			if ( ! empty( $item['cover'] ) ) {
				$preparedItem['CV'] = ItemsSelection::getImageExtension( $item['cover'] );
			}
			if ( ! empty( $item['avatar'] ) ) {
				$preparedItem['ava'] = ItemsSelection::getImageExtension( $item['avatar'] );
			}
			if ( isset( $item['parent_id'] ) && $item['parent_id'] !== 0 ) {
				$preparedItem['P'] = Category::ID_PREFIX . $item['parent_id'];
			}


			$sommelierId   = ( $item['sommelier_id'] !== null ) ? Sommelier::ID_PREFIX . $item['sommelier_id'] : null;
			if ( $item['sommeliers_note'] !== null && trim( $item['sommeliers_note'] ) !== '' ) {
				$preparedItem[ Sommelier::CLIENT_FILE_ID ]['note'] = $item['sommeliers_note'];
				if ( $sommelierId !== null ) {
					$preparedItem[ Sommelier::CLIENT_FILE_ID ]['id'] = $sommelierId;
				}
			}
			$preparedItems[ $selectedObject::ID_PREFIX . $item['id'] ] = $preparedItem;
		}

		return $preparedItems;
	}

	private function getImageExtension( $imagePath ) {
		$lastDot   = strrpos( $imagePath, '.' );
		$extension = substr( $imagePath, $lastDot + 1 );

		return $extension;
	}

	private function getAttrIds( $products, $product, $attr ) {
		$att1      = new $attr();
		$className = $att1->getClass();
		$items = $products->find( $product['id'] )->{$className};
		if ( $items === null ) {
			return null;
		}
		$ids = [];
		foreach ( $items as $item ) {
			$id = $att1::ID_PREFIX . $item['id'];
			array_push( $ids, $id );
		}
		return ( $ids === [] ) ? null : $ids;
	}

	private function getLinked( $product, $linkedType ) {
		$items = $product->{$linkedType};
		$ids   = collect([]);

		if ( $items !== null ) {
			foreach ( $items as $item ) {
				if ( $item->show_on_site == true ) {
					$ids->push(Product::ID_PREFIX . $item->id);
				}
			}
			if ( $linkedType === 'volums' && $ids->isNotEmpty() ) {
				$ids->push(Product::ID_PREFIX . $product->id);
			}
		}
		$ids = $ids->unique();

		return ( $ids->isEmpty() ) ? null : $ids->toArray();

	}

	private function sortByVolsDesc( $connectionsVols ) {
		$volsArr = collect( [] );
		foreach ( $connectionsVols as $item ) {
			$idNumber         = (int) filter_var( $item, FILTER_SANITIZE_NUMBER_INT );
			$itemVol          = Product::where( 'id', $idNumber )->first( 'volume' )->volume;
			$volsArr[ $item ] = $itemVol;
		}
		$sortedItems = $volsArr->sort();

		return $sortedItems->keys()->toArray();


	}


	public static function getFromProducts( $products, $obj, string $clientFileId ) {
		$prods = collect( $products );
		if ( $prods->isEmpty() ) {
			return null;
		}
		$ids = collect( [] );
		foreach ( $prods as $prod ) {
			if ( isset( $prod[ $clientFileId ] ) ) {
				if ( gettype( $prod[ $clientFileId ] ) === 'string' ) {//если один к одному
					$id = (int) filter_var( $prod[ $clientFileId ], FILTER_SANITIZE_NUMBER_INT );
					$ids->add( $id );
				} elseif ( gettype( $prod[ $clientFileId ] ) === "array" ) {//если один ко многим
					foreach ( $prod[ $clientFileId ] as $singleID ) {
						$id = (int) filter_var( $singleID, FILTER_SANITIZE_NUMBER_INT );
						$ids->add( $id );
					}
				}
			}

		}

		if ( $ids->count() !== 0 ) {
			$ids = $ids->unique();

			return $obj::getItems( null, $ids->toArray() );
		}

	}

	public static function getNamesList( array $items = [] ) {
		if ( count( $items ) === 0 ) {
			return null;
		}
		$names = '';
		foreach ( $items as $key => $item ) {
			$names .= $item->name;
			if ( $key !== count( $items ) - 1 ) {
				$names .= ', ';
			}
		}

		return $names;
	}

	private function getCategoryChars( $chars ) {
		$chars      = json_decode( $chars );
		$readyChars = [];
		if ( $chars !== null ) {
			foreach ( $chars as $dirChar ) {
				$char = Characteristic::where( 'id', intval( $dirChar->characteristic ) )->first();
				if ( $char !== null ) {
					$readyChar = [
					];
					if ( boolval( $dirChar->main ) === true ) {
						$readyChar['M'] = 1;
					}
					if ( boolval( $dirChar->filter ) === true ) {
						$readyChar['F'] = 1;
					}
					$readyChars[ $char->code_name] = $readyChar;
				}
			}
			return $readyChars;
		}
		return null;
	}

}