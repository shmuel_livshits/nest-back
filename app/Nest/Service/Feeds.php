<?php

namespace App\Nest\Service;

use App\Models\Alcohol_style;
use App\Models\Author;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Characteristic;
use App\Models\Country;
use App\Models\Grape_sort;
use App\Models\Language;
use App\Models\Marking;
use App\Models\Pack;
use App\Models\Product;
use App\Models\Publisher;
use App\Models\Region;
use App\Models\Sugar_content;
use App\Nest\Images\ImageService;
use App\Nest\ItemsSelection;
use Illuminate\Support\Facades\Storage;
use Spatie\ArrayToXml\ArrayToXml;

class Feeds
{
    private $lang;
    private $mainArray = [];
    private $rootElement = [];
    public $alloPath;
    public $alcoBizPath;
    public $googleMerchantPath;
    public $searchPath;
    public $salesDivePath;
    public const dirName = '/Feeds/';

    public function __construct()
    {
        $this->lang = app()->getLocale();
        $this->alloPath = Feeds::dirName . $this->lang . '/allo.xml';
        $this->salesDivePath = Feeds::dirName . $this->lang . '/salesDrive.xml';
        $this->alcoBizPath = Feeds::dirName . $this->lang . '/alcobiz.xml';
        $this->googleMerchantPath = Feeds::dirName . $this->lang . '/google_merchant.xml';
        $this->searchPath = Feeds::dirName . $this->lang . '/multisearch.xml';
        $this->facebookMerchantPath = Feeds::dirName . $this->lang . '/facebook_merchant.xml';
        $this->mainArray = [
            'name' => 'Wine Library',
            'date' => $this->getProductsUpdateTime(),
            'url' => 'https://winelibrary.com.ua/',
            'currencies' => [
                'currency' => [
                    '_attributes' => [
                        'id' => 'UAH',
                        'rate' => 1
                    ],
                ]
            ],
            'categories' => [
                'category' => [
                    //     $categories

                ],
            ],
            'offers' => [
                'offer' => [
//                    $offers
                ]
            ]
        ];

        $this->rootElement = [
            'rootElementName' => 'yml_catalog',
            '_attributes' => [
                'date' => $this->getProductsUpdateTime()
            ]
        ];
    }

    public function generateAlloFeeds()
    {
        $products = Product::where('show_in_allo', true)->get();
        $offers = [];
        $mainArray = [
            'shop' => [
                'name' => 'Wine Library',
                'date' => $this->getProductsUpdateTime(),
                'url' => 'https://winelibrary.com.ua/',
                'currencies' => [
                    'currency' => [
                        '_attributes' => [
                            'id' => 'UAH',
                            'rate' => 1
                        ],
                    ]
                ],
                'categories' => [
                    'category' => [
                        //     $categories

                    ],
                ],
                'offers' => [
                    'offer' => [
//                    $offers
                    ]
                ]
            ]
        ];

        $rootElement = [
            'rootElementName' => 'yml_catalog',
            '_attributes' => [
                'date' => $this->getProductsUpdateTime()
            ]
        ];
        foreach ($products as $rawProduct) {
            $offer = $this->buildAlloProductFeed($rawProduct);
            array_push($offers, $offer);
        }
        $mainArray['shop']['offers']['offer'] = $offers;
        $result = ArrayToXml::convert($mainArray,
            $rootElement
        );
        Storage::put($this->alloPath, $result);
        return;
    }

    private function buildAlloProductFeed($product)
    {
        $isAvaliable = ($product->quantity > 0) ? true : false;
        $brand = Brand::where('id', $product->brand_id)->first();
        $brand_name = ($brand !== null) ? $brand->name : null;
        $categoryId = null;
        $category = $product->categories->first();
        if ($category !== null) {
            $categoryId = $category->id;
        }
        $offer = [
            '_attributes' => ['avaliable' => $isAvaliable],
            'id' => $product->sku,
            'code' => $product->sku,
            'available' => $product->quantity,
            'categoryId' => $categoryId,
            'vendor' => $brand_name,
            'name' => $product->name,
            'volume' => $product->volume,
            'price' => $product->price,
            'currencyId' => 'UAH'
        ];

        if ($product->old_price !== null && $product->old_price!=0) {
            $offer['oldprice'] = $product->old_price;
        }

        $params = [];
        if ($product->marking_id !== null) {
            $mark = Marking::where('id', $product->marking_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Marking::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $mark->name,
            ];
            array_push($params, $param);

        }
        if ($product->pack_id !== null) {
            $pack = Pack::where('id', $product->pack_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Pack::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $pack->name,
            ];
            array_push($params, $param);
        }
        if ($product->degree !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'abv')->first('name')->name],
                '_value' => strval($product->degree)
            ];
            array_push($params, $param);
        }
        if (isset($product->alcohol_aging)) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'age')->first('name')->name],
                '_value' => $product->alcohol_aging
            ];
            array_push($params, $param);
        }
        if ($product->region_id !== null) {
            $region = Region::where('id', $product->region_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Region::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $region->name,
            ];
            array_push($params, $param);
        }
        if ($product->country_id !== null) {
            $country = Country::where('id', $product->country_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Country::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $country->name,
            ];
            array_push($params, $param);
        }
        if ($product->alcohol_style_id !== null) {
            $style = Alcohol_style::where('id', $product->alcohol_style_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Alcohol_style::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $style->name,
            ];
            array_push($params, $param);

        }
        if ($product->harvest_year !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'hy')->first('name')->name],
                '_value' => strval($product->harvest_year)
            ];
            array_push($params, $param);
        }
        if ($product->serving_temperature) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'st')->first('name')->name],
                '_value' => $product->serving_temperature
            ];
            array_push($params, $param);
        }
        if ($product->sugar_contents !== null) {
            $sugarContentNames = ItemsSelection::getNamesList($product->sugar_contents->all());
            if ($sugarContentNames !== null) {
                $param = [
                    '_attributes' => ['name' => Characteristic::where('code_name', Sugar_content::CLIENT_FILE_ID)->first('name')->name],
                    '_value' => $sugarContentNames
                ];
                array_push($params, $param);
            }
        }
        if ($product->grape_sorts !== null) {
            $grapeSorstsNames = ItemsSelection::getNamesList($product->grape_sorts->all());
            if ($grapeSorstsNames !== null) {
                $param = [
                    '_attributes' => ['name' => Characteristic::where('code_name', Grape_sort::CLIENT_FILE_ID)->first('name')->name],
                    '_value' => $grapeSorstsNames
                ];
                array_push($params, $param);
            }
        }

        if ($product->publish_year !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'py')->first('name')->name],
                '_value' => $product->publish_year
            ];
            array_push($params, $param);
        }
        if ($product->language !== null) {
            $language = Language::where('id', $product->language_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Language::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $language->name
            ];
            array_push($params, $param);
        }
        if ($product->publisher_id !== null) {
            $publisher = Publisher::where('id', $product->publisher_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Publisher::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $product->publisher->name
            ];
            array_push($params, $param);
        }
        if ($product->authors !== null) {
            $autors = ItemsSelection::getNamesList($product->authors->all());
            if ($autors !== null) {
                $param = [
                    '_attributes' => ['name' => Characteristic::where('code_name', Author::CLIENT_FILE_ID)->first('name')->name],
                    '_value' => $autors
                ];
                array_push($params, $param);
            }
        }
	    $urlParamLocale=$this->getUrlParamLocale();
        $offer['url'] = 'https://winelibrary.com.ua/'.$urlParamLocale . $product->page_slug . '/' . Product::ID_PREFIX . $product->id;
        $offer['picture'] = 'https://back.winelibrary.com.ua/uploads/imgs/' .ImageService::getImageNameOriginalExt(Product::ID_PREFIX . $product->id,'winelibrary');
        $offer['param'] = $params;
        return $offer;
    }

	public function generateSalesDriveFeeds()
	{
		$products = Product::where('show_on_site', true)->get();
		$offers = [];
		$mainArray = [
			'shop' => [
				'name' => 'Wine Library',
				'date' => $this->getProductsUpdateTime(),
				'url' => 'https://winelibrary.com.ua/',
				'currencies' => [
					'currency' => [
						'_attributes' => [
							'id' => 'UAH',
							'rate' => 1
						],
					]
				],
				'categories' => [
					'category' => [
						//     $categories

					],
				],
				'offers' => [
					'offer' => [
//                    $offers
					]
				]
			]
		];

		$rootElement = [
			'rootElementName' => 'yml_catalog',
			'_attributes' => [
				'date' => $this->getProductsUpdateTime()
			]
		];
		foreach ($products as $rawProduct) {
			$offer = $this->buildSalesDriveFeedsProductFeed($rawProduct);
			array_push($offers, $offer);
		}
		$mainArray['shop']['offers']['offer'] = $offers;
		$mainArray['shop']['categories']['category'] = $this->getCategories();
		$result = ArrayToXml::convert($mainArray,
			$rootElement
		);
		Storage::put($this->salesDivePath, $result);
		return;
	}

	private function buildSalesDriveFeedsProductFeed($product)
	{
		$isAvaliable = ($product->quantity > 0) ? true : false;
		$brand = Brand::where('id', $product->brand_id)->first();
		$brand_name = ($brand !== null) ? $brand->name : null;
		$categoryId = null;
		$categoryId = $product->categories->first();
		if ($categoryId !== null) {
			$categoryId =$categoryId->id;
		}
		$offer = [
			'_attributes' =>
				[
					'avaliable' => $isAvaliable,
					'id'=>$product->id,
				],
			'id' => $product->sku,
			'code' => $product->sku,
			'available' => $product->quantity,
			'categoryId' => $categoryId,
			'vendor' => $brand_name,
			'name' => $product->name,
			'volume' => $product->volume,
			'price' => $product->price,
			'currencyId' => 'UAH'
		];

		if ($product->old_price !== null && $product->old_price!=0) {
			$offer['oldprice'] = $product->old_price;
		}

		$params = [];
		if ($product->marking_id !== null) {
			$mark = Marking::where('id', $product->marking_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Marking::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $mark->name,
			];
			array_push($params, $param);

		}
		if ($product->pack_id !== null) {
			$pack = Pack::where('id', $product->pack_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Pack::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $pack->name,
			];
			array_push($params, $param);
		}
		if ($product->degree !== null) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'abv')->first('name')->name],
				'_value' => strval($product->degree)
			];
			array_push($params, $param);
		}
		if (isset($product->alcohol_aging)) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'age')->first('name')->name],
				'_value' => $product->alcohol_aging
			];
			array_push($params, $param);
		}
		if ($product->region_id !== null) {
			$region = Region::where('id', $product->region_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Region::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $region->name,
			];
			array_push($params, $param);
		}
		if ($product->country_id !== null) {
			$country = Country::where('id', $product->country_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Country::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $country->name,
			];
			array_push($params, $param);
		}
		if ($product->alcohol_style_id !== null) {
			$style = Alcohol_style::where('id', $product->alcohol_style_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Alcohol_style::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $style->name,
			];
			array_push($params, $param);

		}
		if ($product->harvest_year !== null) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'hy')->first('name')->name],
				'_value' => strval($product->harvest_year)
			];
			array_push($params, $param);
		}
		if ($product->serving_temperature) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'st')->first('name')->name],
				'_value' => $product->serving_temperature
			];
			array_push($params, $param);
		}
		if ($product->sugar_contents !== null) {
			$sugarContentNames = ItemsSelection::getNamesList($product->sugar_contents->all());
			if ($sugarContentNames !== null) {
				$param = [
					'_attributes' => ['name' => Characteristic::where('code_name', Sugar_content::CLIENT_FILE_ID)->first('name')->name],
					'_value' => $sugarContentNames
				];
				array_push($params, $param);
			}
		}
		if ($product->grape_sorts !== null) {
			$grapeSorstsNames = ItemsSelection::getNamesList($product->grape_sorts->all());
			if ($grapeSorstsNames !== null) {
				$param = [
					'_attributes' => ['name' => Characteristic::where('code_name', Grape_sort::CLIENT_FILE_ID)->first('name')->name],
					'_value' => $grapeSorstsNames
				];
				array_push($params, $param);
			}
		}

		if ($product->publish_year !== null) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'py')->first('name')->name],
				'_value' => $product->publish_year
			];
			array_push($params, $param);
		}
		if ($product->language !== null) {
			$language = Language::where('id', $product->language_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Language::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $language->name
			];
			array_push($params, $param);
		}
		if ($product->publisher_id !== null) {
			$publisher = Publisher::where('id', $product->publisher_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Publisher::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $product->publisher->name
			];
			array_push($params, $param);
		}
		if ($product->authors !== null) {
			$autors = ItemsSelection::getNamesList($product->authors->all());
			if ($autors !== null) {
				$param = [
					'_attributes' => ['name' => Characteristic::where('code_name', Author::CLIENT_FILE_ID)->first('name')->name],
					'_value' => $autors
				];
				array_push($params, $param);
			}
		}
		$urlParamLocale=$this->getUrlParamLocale();
		$offer['url'] = 'https://winelibrary.com.ua/'.$urlParamLocale . $product->page_slug . '/' . Product::ID_PREFIX . $product->id;
		$offer['picture'] = 'https://back.winelibrary.com.ua/uploads/imgs/' .ImageService::getImageNameOriginalExt(Product::ID_PREFIX . $product->id,'winelibrary');
		$offer['param'] = $params;
		return $offer;
	}

	public function generateAlcoBizFeeds()
	{
		$products = Product::where('show_in_alcobiz', true)->get();
		$offers = [];
		$mainArray = [
			'shop' => [
				'name' => 'Wine Library',
				'date' => $this->getProductsUpdateTime(),
				'url' => 'https://winelibrary.com.ua/',
				'currencies' => [
					'currency' => [
						'_attributes' => [
							'id' => 'UAH',
							'rate' => 1
						],
					]
				],
				'categories' => [
					'category' => [
						//     $categories

					],
				],
				'offers' => [
					'offer' => [
//                    $offers
					]
				]
			]
		];

		$rootElement = [
			'rootElementName' => 'yml_catalog',
			'_attributes' => [
				'date' => $this->getProductsUpdateTime()
			]
		];
		foreach ($products as $rawProduct) {
			$offer = $this->buildAlcoBizFeed($rawProduct);
			array_push($offers, $offer);
		}
		$mainArray['shop']['offers']['offer'] = $offers;
		$result = ArrayToXml::convert($mainArray,
			$rootElement
		);
		Storage::put($this->alcoBizPath, $result);
		return;
	}

	private function buildAlcoBizFeed($product)
	{
		$isAvaliable = ($product->quantity > 0) ? true : false;
		$brand = Brand::where('id', $product->brand_id)->first();
		$brand_name = ($brand !== null) ? $brand->name : null;
		$categoryName = null;
		$category = $product->categories->first();
		if ($category !== null) {
			$categoryName = $category->name;
		}
		$offer = [
			'_attributes' => ['avaliable' => $isAvaliable],
			'id' => $product->sku,
			'code' => $product->sku,
			'available' => $product->quantity,
			'category' => $categoryName,
			'vendor' => $brand_name,
			'name' => $product->name,
			'volume' => $product->volume,
			'price' => $product->price,
			'currencyId' => 'UAH'
		];

		if ($product->old_price !== null && $product->old_price!=0) {
			$offer['oldprice'] = $product->old_price;
		}

		$params = [];
		if ($product->marking_id !== null) {
			$mark = Marking::where('id', $product->marking_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Marking::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $mark->name,
			];
			array_push($params, $param);

		}
		if ($product->pack_id !== null) {
			$pack = Pack::where('id', $product->pack_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Pack::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $pack->name,
			];
			array_push($params, $param);
		}
		if ($product->degree !== null) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'abv')->first('name')->name],
				'_value' => strval($product->degree)
			];
			array_push($params, $param);
		}
		if (isset($product->alcohol_aging)) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'age')->first('name')->name],
				'_value' => $product->alcohol_aging
			];
			array_push($params, $param);
		}
		if ($product->region_id !== null) {
			$region = Region::where('id', $product->region_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Region::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $region->name,
			];
			array_push($params, $param);
		}
		if ($product->country_id !== null) {
			$country = Country::where('id', $product->country_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Country::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $country->name,
			];
			array_push($params, $param);
		}
		if ($product->alcohol_style_id !== null) {
			$style = Alcohol_style::where('id', $product->alcohol_style_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Alcohol_style::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $style->name,
			];
			array_push($params, $param);

		}
		if ($product->harvest_year !== null) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'hy')->first('name')->name],
				'_value' => strval($product->harvest_year)
			];
			array_push($params, $param);
		}
		if ($product->serving_temperature) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'st')->first('name')->name],
				'_value' => $product->serving_temperature
			];
			array_push($params, $param);
		}
		if ($product->sugar_contents !== null) {
			$sugarContentNames = ItemsSelection::getNamesList($product->sugar_contents->all());
			if ($sugarContentNames !== null) {
				$param = [
					'_attributes' => ['name' => Characteristic::where('code_name', Sugar_content::CLIENT_FILE_ID)->first('name')->name],
					'_value' => $sugarContentNames
				];
				array_push($params, $param);
			}
		}
		if ($product->grape_sorts !== null) {
			$grapeSorstsNames = ItemsSelection::getNamesList($product->grape_sorts->all());
			if ($grapeSorstsNames !== null) {
				$param = [
					'_attributes' => ['name' => Characteristic::where('code_name', Grape_sort::CLIENT_FILE_ID)->first('name')->name],
					'_value' => $grapeSorstsNames
				];
				array_push($params, $param);
			}
		}

		if ($product->publish_year !== null) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'py')->first('name')->name],
				'_value' => $product->publish_year
			];
			array_push($params, $param);
		}
		if ($product->language !== null) {
			$language = Language::where('id', $product->language_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Language::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $language->name
			];
			array_push($params, $param);
		}
		if ($product->publisher_id !== null) {
			$publisher = Publisher::where('id', $product->publisher_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Publisher::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $product->publisher->name
			];
			array_push($params, $param);
		}
		if ($product->authors !== null) {
			$autors = ItemsSelection::getNamesList($product->authors->all());
			if ($autors !== null) {
				$param = [
					'_attributes' => ['name' => Characteristic::where('code_name', Author::CLIENT_FILE_ID)->first('name')->name],
					'_value' => $autors
				];
				array_push($params, $param);
			}
		}
		$urlParamLocale=$this->getUrlParamLocale();
		$offer['url'] = 'https://winelibrary.com.ua/'.$urlParamLocale . $product->page_slug . '/' . Product::ID_PREFIX . $product->id;
		$offer['picture'] = 'https://back.winelibrary.com.ua/uploads/imgs/' .ImageService::getImageNameOriginalExt(Product::ID_PREFIX . $product->id,'winelibrary');
		$offer['param'] = $params;
		return $offer;
	}

	public function generateFacebookFeeds()
	{
		$products = Product::where('show_on_site', true)->get();
		$offers = [];
		$mainArray = [
			'channel' => [
				'title' => 'Wine Library',
//				'date' => $this->getProductsUpdateTime(),
				'link' => 'https://winelibrary.com.ua/',
				/*'currencies' => [
					'currency' => [
						'_attributes' => [
							'id' => 'UAH',
							'rate' => 1
						],
					]
				],*/
			]
		];

		$rootElement = [
			'rootElementName' => 'rss',
			'_attributes' => [
				'xmlns:g' => 'http://base.google.com/ns/1.0',
				'version'=>'2.0'
			]
		];
		foreach ($products as $rawProduct) {
			$offer = $this->buildFacebookFeed($rawProduct);
			array_push($offers, $offer);
		}
		$mainArray['channel']['item'] = $offers;
		$result = ArrayToXml::convert($mainArray,
			$rootElement
		);
		Storage::put($this->facebookMerchantPath, $result);
		return;
	}

	private function buildFacebookFeed($product)
	{
		$isAvaliable = ($product->quantity > 0) ? 'in stock' : 'out of stock';
		$brand = Brand::where('id', $product->brand_id)->first();
		$brand_name = ($brand !== null) ? $brand->name : null;
		$categoryName = null;
		$category = $product->categories->first();
		if ($category !== null) {
			$categoryName = $category->name;
		}
		$google_merchant_category=null;
		if ($category !== null) {
			$categoryId = $category->id;
			$google_merchant_category=$category['google_merchant_category'];
		}
		$offer = [
			'g:id' => $product->sku,
			'g:availability'=>$isAvaliable,
			'g:title' => $product->name,
			'g:condition'=>'new',
			'g:category' => $categoryName,
			'g:google_product_category' => $google_merchant_category,
			'g:brand' => $brand_name,
			'g:size' => $product->volume,
			'g:age_group'=>'adult',
//			'currencyId' => 'UAH'
		];

		if ($product->old_price !== null && $product->old_price!=0) {
			$offer['g:price'] = $product->old_price .  'UAH';
			$offer['g:sale_price']=$product->price . 'UAH';
		}
		else{
			$offer['g:price'] =$product->price . 'UAH';
		}

		/*$params = [];
		if ($product->marking_id !== null) {
			$mark = Marking::where('id', $product->marking_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Marking::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $mark->name,
			];
			array_push($params, $param);

		}
		if ($product->pack_id !== null) {
			$pack = Pack::where('id', $product->pack_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Pack::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $pack->name,
			];
			array_push($params, $param);
		}
		if ($product->degree !== null) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'abv')->first('name')->name],
				'_value' => strval($product->degree)
			];
			array_push($params, $param);
		}
		if (isset($product->alcohol_aging)) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'age')->first('name')->name],
				'_value' => $product->alcohol_aging
			];
			array_push($params, $param);
		}
		if ($product->region_id !== null) {
			$region = Region::where('id', $product->region_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Region::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $region->name,
			];
			array_push($params, $param);
		}
		if ($product->country_id !== null) {
			$country = Country::where('id', $product->country_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Country::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $country->name,
			];
			array_push($params, $param);
		}
		if ($product->alcohol_style_id !== null) {
			$style = Alcohol_style::where('id', $product->alcohol_style_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Alcohol_style::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $style->name,
			];
			array_push($params, $param);

		}
		if ($product->harvest_year !== null) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'hy')->first('name')->name],
				'_value' => strval($product->harvest_year)
			];
			array_push($params, $param);
		}
		if ($product->serving_temperature) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'st')->first('name')->name],
				'_value' => $product->serving_temperature
			];
			array_push($params, $param);
		}
		if ($product->sugar_contents !== null) {
			$sugarContentNames = ItemsSelection::getNamesList($product->sugar_contents->all());
			if ($sugarContentNames !== null) {
				$param = [
					'_attributes' => ['name' => Characteristic::where('code_name', Sugar_content::CLIENT_FILE_ID)->first('name')->name],
					'_value' => $sugarContentNames
				];
				array_push($params, $param);
			}
		}
		if ($product->grape_sorts !== null) {
			$grapeSorstsNames = ItemsSelection::getNamesList($product->grape_sorts->all());
			if ($grapeSorstsNames !== null) {
				$param = [
					'_attributes' => ['name' => Characteristic::where('code_name', Grape_sort::CLIENT_FILE_ID)->first('name')->name],
					'_value' => $grapeSorstsNames
				];
				array_push($params, $param);
			}
		}
		if ($product->publish_year !== null) {
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', 'py')->first('name')->name],
				'_value' => $product->publish_year
			];
			array_push($params, $param);
		}
		if ($product->language !== null) {
			$language = Language::where('id', $product->language_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Language::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $language->name
			];
			array_push($params, $param);
		}
		if ($product->publisher_id !== null) {
			$publisher = Publisher::where('id', $product->publisher_id)->first();
			$param = [
				'_attributes' => ['name' => Characteristic::where('code_name', Publisher::CLIENT_FILE_ID)->first('name')->name],
				'_value' => $product->publisher->name
			];
			array_push($params, $param);
		}
		if ($product->authors !== null) {
			$autors = ItemsSelection::getNamesList($product->authors->all());
			if ($autors !== null) {
				$param = [
					'_attributes' => ['name' => Characteristic::where('code_name', Author::CLIENT_FILE_ID)->first('name')->name],
					'_value' => $autors
				];
				array_push($params, $param);
			}
		}
		$offer['param'] = $params;*/
		$urlParamLocale=$this->getUrlParamLocale();
		$offer['g:link'] = 'https://winelibrary.com.ua/'.$urlParamLocale . $product->page_slug . '/' . Product::ID_PREFIX . $product->id;
		$offer['g:image_link'] = 'https://back.winelibrary.com.ua/uploads/imgs/' .ImageService::getImageNameOriginalExt(Product::ID_PREFIX . $product->id,'winelibrary');

		return $offer;
	}

    public function generateMultisearchFeeds()
    {
        $allProducts = Product::where('show_in_search_io', true)->get();
        $offers = [];
        foreach ($allProducts as $product) {
            $offer = $this->buildProductFeedForSearch($product);
            array_push($offers, $offer);
        }


        $this->mainArray['offers']['offer'] = $offers;
//            $this->mainArray['categories']['category']=$this->getAllCategories();
        $result = ArrayToXml::convert($this->mainArray,
            $this->rootElement
        );
        Storage::put($this->searchPath, $result);

    }

    private function buildProductFeedForSearch($product)
    {
        $offer = null;
        $categoryId = null;
        $category = $product->categories->first();
        if ($category !== null) {
            $categoryId = $category->id;
        }

        $brand = Brand::where('id', $product->brand_id)->first();
        $isAvaliable = ($product->quantity > 0) ? 'true' : 'false';
        $id = Product::ID_PREFIX . $product->id;

        $brand_name = ($brand !== null) ? $brand->name : null;
        $presence = ($product->quantity > 0) ? 'Є в наявності' : 'Немає в наявності';
        if ($this->lang === 'ru') {
            $presence = ($product->quantity > 0) ? 'Есть в наличии' : 'Нет в наличии';
        }
	    $urlParamLocale=$this->getUrlParamLocale();
        $offer = [
            '_attributes' => [
                'avaliable' => $isAvaliable,
                'id' => $id
            ],
            'code' => $product->sku,
            'presence' => $presence,
            'description' => $product->description,
            'categoryId' => $categoryId,
            'vendor' => $brand_name,
            'name' => $product->name,
            'volume' => $product->volume,
            'price' => $product->price,
            'currencyId' => 'UAH',
            'createdAt' => $product->created_at,
            'snippet' => 'Код: ' . $product->sku,
            'vendorCode' => $brand_name,
            'ordering' => $product->popularity,
            'url' => 'https://winelibrary.com.ua/'.$urlParamLocale . $product->page_slug . '/' . Product::ID_PREFIX . $product->id,
            'picture' => 'https://winelibrary.com.ua/uploads/imgs/' . $product->image,
        ];

        if ($product->old_price !== null  && $product->old_price!=0) {
            $offer['oldprice'] = $product->old_price;
        }

        $params = [];
        if ($product->marking_id !== null) {
            $mark = Marking::where('id', $product->marking_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Marking::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $mark->name
            ];
            array_push($params, $param);
        }
        if ($product->pack_id !== null) {
            $pack = Pack::where('id', $product->pack_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Pack::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $pack->name
            ];
            array_push($params, $param);
        }
        if ($product->degree !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'abv')->first('name')->name],
                '_value' => strval($product->degree)
            ];
            array_push($params, $param);
        }
        if ($product->region_id !== null) {
            $region = Region::where('id', $product->region_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Region::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $region->name
            ];
            array_push($params, $param);
        }
        if ($product->alcohol_style_id !== null) {
            $style = Alcohol_style::where('id', $product->alcohol_style_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Alcohol_style::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $style->name
            ];
            array_push($params, $param);
        }
        if ($product->alcohol_aging !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'age')->first('name')->name],
                '_value' => $product->alcohol_aging
            ];
            array_push($params, $param);
        }
        if ($product->harvest_year !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'hy')->first('name')->name],
                '_value' => $product->harvest_year
            ];
            array_push($params, $param);
        }
        if ($product->serving_temperature) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'st')->first('name')->name],
                '_value' => $product->serving_temperature
            ];
            array_push($params, $param);
        }
        $sugarContentNames = ItemsSelection::getNamesList($product->sugar_contents->all());
        if ($sugarContentNames !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Sugar_content::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $sugarContentNames
            ];
            array_push($params, $param);

        }
        $grapeSorstsNames = ItemsSelection::getNamesList($product->grape_sorts->all());
        if ($grapeSorstsNames !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Grape_sort::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $grapeSorstsNames
            ];
            array_push($params, $param);
        }
        $tags = $product->tags->all();
        if (count($tags) > 0) {
            foreach ($tags as $tag) {
                $param = [
                    '_attributes' => ['name' => 'keyword'],
                    '_value' => $tag->name
                ];
                array_push($params, $param);
            }
        }

        if ($product->publish_year !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', 'py')->first('name')->name],
                '_value' => strval($product->publish_year)
            ];
            array_push($params, $param);
        }
        if ($product->language_id !== null) {
            $language = Language::where('id', $product->language_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Language::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $language->name
            ];
            array_push($params, $param);
        }
        if ($product->publisher_id !== null) {
            $publisher = Publisher::where('id', $product->publisher_id)->first();
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Publisher::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $publisher->name
            ];
            array_push($params, $param);
        }
        $autors = Author::getNamesList($product->authors->all());
        if ($autors !== null) {
            $param = [
                '_attributes' => ['name' => Characteristic::where('code_name', Author::CLIENT_FILE_ID)->first('name')->name],
                '_value' => $autors
            ];
            array_push($params, $param);
        }


        $offer['param'] = $params;
        return $offer;
    }

    private function getProductsUpdateTime()
    {
        return strval(Product::get(['updated_at'])->max()->updated_at->toDateTimeString());
    }

    public function generateGoogleFeeds()
    {
        $allProducts = Product::where('show_in_google_merchant', true)->get();
        $offers = [];
        foreach ($allProducts as $rawProduct) {
            $offer = $this->buildGoogleMerchantProductFeed($rawProduct);
            array_push($offers, $offer);
        }
        $googleMainArray = [
            'title' => 'Wine Library',
            'updated' => $this->getProductsUpdateTime(),
        ];

        $GoogleRootElement = [
            'rootElementName' => 'feed',
            '_attributes' => [
                'xmlns' => 'http://www.w3.org/2005/Atom',
                'xmlns:g' => 'http://base.google.com/ns/1.0'
            ]
        ];

        $googleMainArray['entry'] = $offers;
        $result = ArrayToXml::convert($googleMainArray,
            $GoogleRootElement
        );
        Storage::put($this->googleMerchantPath, $result);

    }

    private function buildGoogleMerchantProductFeed($product)
    {
        $isAvaliable = ($product->quantity > 0) ? 'In stock' : 'out of stock';
        $brand = Brand::where('id', $product->brand_id)->first();
        $brand_name = ($brand !== null) ? $brand->name : null;
        $categoryId = null;
        $category = $product->categories->first();
        $google_merchant_category=null;
        if ($category !== null) {
            $categoryId = $category->id;
            $google_merchant_category=$category['google_merchant_category'];
        }

        $offer = [
            'g:id' => $product->sku,
            'g:gtin' => $product->barcode,
            'g:title' => $product->name,
            'g:availability' => $isAvaliable,
            'g:google_product_category' => $google_merchant_category,
            'g:brand' => $brand_name,

        ];
	    if ($product->old_price !== null && $product->old_price!=0) {
		    $offer['g:price'] = $product->old_price .  'UAH';
		    $offer['g:sale_price']=$product->price . 'UAH';
	    }
	    else{
		    $offer['g:price'] =$product->price . 'UAH';
	    }

        if ($product->description !== null) {
            $offer['g:description'] = $product->description;
        }
		$urlParamLocale=$this->getUrlParamLocale();
        $offer['g:link'] = 'https://winelibrary.com.ua/'.$urlParamLocale . $product->page_slug . '/' . Product::ID_PREFIX . $product->id;
        $offer['g:image_link'] = 'https://back.winelibrary.com.ua/uploads/imgs/' .ImageService::getImageNameOriginalExt(Product::ID_PREFIX . $product->id,'winelibrary');
        return $offer;
    }

    public function getCategories(){
    	$categories =Category::all();
	    $preparedCategories=[];
    	foreach ($categories as $category){
    		$preparedCetegory=[
			    '_attributes' => ['id' => $category->id],
			    '_value'=>$category->name
		    ];
		    array_push($preparedCategories,$preparedCetegory);
	    }
	    return $preparedCategories;
    }

    public function getUrlParamLocale(){
    	return (app()->getLocale()===config('app.available_locales')[0])?null:app()->getLocale().'/';
    }


}