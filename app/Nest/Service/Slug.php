<?php

namespace App\Nest\Service;


use Cocur\Slugify\Slugify;

class Slug
{
    public static function translit($phrase)
    {
        $phrase = (string)$phrase; // преобразуем в строковое значение
        $phrase = trim($phrase); // убираем пробелы в начале и конце строки
        $phrase = function_exists('mb_strtolower') ? mb_strtolower($phrase) : strtolower($phrase); // переводим строку в нижний регистр (иногда надо задать локаль)
        $phrase = strtr($phrase, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j',
            'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch',
            'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => '','і'=>'i','ї'=>'y','ґ'=>'g','є'=>'e'));
        return $phrase;
    }

    public static function slugify($phrase){
        $slugify = new Slugify();
        return $slugify->slugify($phrase);
    }

    public static function translitAndSlugify($phrase){
        return Slug::slugify(Slug::translit($phrase));
    }

}