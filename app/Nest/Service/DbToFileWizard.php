<?php

namespace App\Nest\Service;

use App\Models\Alcohol_style;
use App\Models\Article;
use App\Models\Author;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Characteristic;
use App\Models\Combination;
use App\Models\Country;
use App\Models\Grape_sort;
use App\Models\Language;
use App\Models\Marking;
use App\Models\Menu;
use App\Models\Pack;
use App\Models\Phone;
use App\Models\Post_operator;
use App\Models\Producer;
use App\Models\Product;
use App\Models\Provider;
use App\Models\Publisher;
use App\Models\Region;
use App\Models\ShopRegion;
use App\Models\Sign;
use App\Models\Site_Page;
use App\Models\Sommelier;
use App\Models\Sugar_content;
use App\Models\Tag;
use App\Models\Wine_type;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DbToFileWizard {
	public const dirName = '/content/';
	public const fileName = 'db.json';
	public $dbFilePathes;
	private $maxQuantity = 50;

	public function __construct() {
		foreach ( config( 'app.available_locales' ) as $locale ) {
			$this->dbFilePathes[ $locale ] = DbToFileWizard::dirName . $locale;
		}
	}


	/**
	 * взять все товары и все зависимости
	 */
	public function getDb( bool $updateDb = false ) {
		foreach ( $this->dbFilePathes as $lang => $filePath ) {
			app()->setLocale( $lang );
			$fullFileName   = $filePath . '/' . DbToFileWizard::fileName;
			$fileUpdateTime = ( $updateDb === false ) ? $this->getDbFileUpdateTime( $fullFileName ) : null;
			$product        = new Product();
			$items          = $product->getItems( $fileUpdateTime );
			$shopRegions = ShopRegion::getItems($fileUpdateTime);
			$signs=Sign::getItems($fileUpdateTime);
			$chars          = Characteristic::getItems( $fileUpdateTime );
			$sommeliers     = Sommelier::getItems( $fileUpdateTime );
			$providers      = Provider::getItems( $fileUpdateTime );
			$producers      = Producer::getItems( $fileUpdateTime );
			$brands         = Brand::getItems( $fileUpdateTime );
			$tags           = Tag::getItems( $fileUpdateTime );
			$countries      = Country::getItems( $fileUpdateTime );
			$regions        = Region::getItems( $fileUpdateTime );
			$sugar_content  = Sugar_content::getItems( $fileUpdateTime );
			$grape_sorts    = Grape_sort::getItems( $fileUpdateTime );
			$alcohol_slyles = Alcohol_style::getItems( $fileUpdateTime );
			$combinations   = Combination::getItems( $fileUpdateTime );
			$markings       = Marking::getItems( $fileUpdateTime );
			$packs          = Pack::getItems( $fileUpdateTime );
			$categories     = Category::getItems( $fileUpdateTime );
			$alcTypes       = Wine_type::getItems( $fileUpdateTime );
			$publishers     = Publisher::getItems( $fileUpdateTime );
			$authors        = Author::getItems( $fileUpdateTime );
			$postOperators  = Post_operator::getItems( $fileUpdateTime );
			$languages      = Language::getItems( $fileUpdateTime );
			$sitePages      = Site_Page::getItems( $fileUpdateTime );
			$acticles       = Article::getItems( $fileUpdateTime );
			$menu           = Menu::getItems( $fileUpdateTime );
			$phones= Phone::getItems($fileUpdateTime);

			$mainArray      = array_merge(
				$sommeliers,
				$providers,
				$producers,
				$brands,
				$tags,
				$countries,
				$regions,
				$sugar_content,
				$alcTypes,
				$grape_sorts,
				$alcohol_slyles,
				$categories,
				$combinations,
				$markings,
				$packs,
				$publishers,
				$authors,
				$postOperators,
				$languages,
				$sitePages,
				$acticles,
				$items,
				$chars,
				$menu,
				$signs,
				$shopRegions,
				$phones
			);
			$isExist        = Storage::disk('dbfiles')->exists( $fullFileName );
			if ( $isExist ) {
				$fileArray = Storage::disk('dbfiles')->get( $fullFileName );
				if ( $fileArray !== null ) {
					$fileArray = collect( json_decode( $fileArray ) );
					foreach ( $mainArray as $key => $value ) {
						if ( isset( $fileArray[ $key ] ) ) {
							$fileArray[ $key ] = $value;
						} else {
							$fileArray->put( $key, $value );
						}
					}
				}
//				$fileArray = $fileArray->sortByDesc( 'U' )->sortBy( 'lft' );

				Storage::disk('dbfiles')->delete($fullFileName);
				Storage::disk('dbfiles')->put( $fullFileName, json_encode( $fileArray, JSON_UNESCAPED_UNICODE ) );
			} else {
				try{
					Storage::disk('dbfiles')->delete($fullFileName);
				}
				catch (\Exception $exception){

				}
				Storage::disk('dbfiles')->put( $fullFileName, json_encode( $mainArray, JSON_UNESCAPED_UNICODE ) );
			}
		}
		if (strpos(strtolower(php_uname()),'linux')!==false){
			shell_exec('chown -R www-data:www-data /var/www/');
		}


	}


	public function getDbFileUpdateTime( $path ) {
		if ( Storage::disk('dbfiles')->exists( $path ) ) {
			$lastModified = Storage::disk('dbfiles')->lastModified( $path );

			return Carbon::createFromTimestamp( $lastModified, config( 'timezone' ) );
		}

		return null;
	}

	public function removeFromClientFiles( $clientId ) {
		foreach ( $this->dbFilePathes as $lang => $filePath ) {
			$fullFileNames = [ $filePath . '/' . DbToFileWizard::fileName, $filePath . '/' . DbToFileWizard::fileName ];
			foreach ( $fullFileNames as $fullFileName ) {
				$fileArray = Storage::disk('dbfiles')->get( $fullFileName );
				if ( $fileArray !== null ) {
					$fileArray = collect( json_decode( $fileArray ) );
					$fileArray = $fileArray->forget( $clientId );
					Storage::disk('dbfiles')->put( $fullFileName, json_encode( $fileArray ), JSON_UNESCAPED_UNICODE );
				}
			}

		}
	}

	public function compareItemsInFileAndDb() {
		$countItemsDb = Product::where( 'show_on_site', true )->count();
		foreach ( $this->dbFilePathes as $lang => $filePath ) {
			app()->setLocale( $lang );
			$fullFileName = $filePath . '/' . DbToFileWizard::fileName;
			try{
				$fileDb       = Storage::disk('dbfiles')->get( $fullFileName );
			}
			catch (\Exception $exception){
				Log::error($exception);
				$this->getDb(true);
				return;
			}

			$fileDb=collect(json_decode($fileDb));
			$itemsFromFile=$fileDb->filter(function ($value,$key){
				$typeId = preg_replace("/[^a-z]+/", "", $key);
				if($typeId ===Product::ID_PREFIX){
					return $value;
				}

			});
			if($countItemsDb !==$itemsFromFile->count()){
				$this->getDb(true);
			}
		}

	}

}