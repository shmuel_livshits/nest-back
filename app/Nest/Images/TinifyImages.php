<?php
/**
 * Created by PhpStorm.
 * User: shmue
 * Date: 27.01.2021
 * Time: 13:26
 */

namespace App\Nest\Images;


use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class TinifyImages
{
    private $apiKey;
    private  $tempPath;
    private $tempName ='temp.jpg';
    private $readyName='ready.jpg';

    public function __construct()
    {
        $this->apiKey=env('TINIFY_API_KEY');
        \Tinify\setKey($this->apiKey);
        $this->tempPath=storage_path('app/temp/');

    }

    public function compressImage($image){
        Storage::put('/temp/'.$this->tempName,$image->stream());
        $source=\Tinify\fromFile($this->tempPath.$this->tempName);
        $source->toFile($this->tempPath.$this->readyName);
        return Image::make($this->tempPath.$this->readyName);
    }

}