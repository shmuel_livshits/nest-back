<?php

namespace App\Nest\Images;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;


class ImageService {
	private const DISK = 'winelibrary';
	private const PATH = 'uploads/imgs/';
	private $watermarkPath;

	public function __construct() {
		$this->watermarkPath = public_path( '/uploads/other/watermark.png' );
	}

	public function prepareItemImages( $sourceImg, $imageName, $imagesProps, $originalExtension, $watermarkProps = null ) {

		$this->saveOriginal( $sourceImg, $imageName, $originalExtension );
		foreach ( $imagesProps as $imageProperty ) {
			$rightSizeImage = Image::make( $sourceImg )->resize( $imageProperty['width'], $imageProperty['height'], function ( $constraint ) {
				$constraint->aspectRatio();
				$constraint->upsize();
			} );
			if ( $watermarkProps !== null ) {
				$waterMarkWidth = $imageProperty['height'] * $watermarkProps['width'];
				$this->insertWatermark( $waterMarkWidth, $rightSizeImage );
			}
			$tinifyImage = new TinifyImages();
			$minifiedImage = $tinifyImage->compressImage( $rightSizeImage );
			$filename = $imageProperty['shortName'] . strtolower( $imageName );
			Storage::disk( $this::DISK )->put( $filename . '.' . $originalExtension, $minifiedImage->stream( $originalExtension ) );
			Storage::disk( $this::DISK )->put( $filename . '.webp', $minifiedImage->stream( 'webp' ) );
		}

		return $this::PATH . $filename . '.' . $originalExtension;
	}

	public function uploadFileToDisk( $file, $attribute_name, $newFileName, $imagesProps, $watermarkProps = null ) {
		$extension = $file->getClientOriginalExtension();
		if ( $extension === 'svg' ) {
			$newFileName = $newFileName . '.' . $extension;
			Storage::disk( $this::DISK )->put( $newFileName, file_get_contents( $file ) );
		} else {

			$this->prepareItemImages( $file, $newFileName, $imagesProps, $extension, $watermarkProps );
			$newFileName = $newFileName . '.' . $extension;
		}
		return $this::PATH . $newFileName;
	}

	public function removeImageIfExists( $name, $imagesProps ) {
		foreach ( $imagesProps as $imageProperty ) {
			$filename = $imageProperty['shortName'] . strtolower( $name );
			Storage::disk( $this::DISK )->delete( [
				$filename . '.jpg',
				$filename . '.jpeg',
				$filename . '.png',
				$filename . '.webp',
				$filename . '.svg'
			] );
		}
	}

	private function saveOriginal( $originalImage, $imageName, $extension ) {

		$originalImage = Image::make( $originalImage );
		Storage::disk( $this::DISK )->put( '/originals/' . $imageName . '.' . $extension, $originalImage->stream( $extension ) );

		return;

	}

	public function insertWatermark( $width, $image ) {
		$watermarkOrigin = Image::make( $this->watermarkPath );
		$watermark       = $watermarkOrigin->resize( $width, null, function ( $constraint ) {
			$constraint->aspectRatio();
		} );
		$image->insert( $watermark, 'bottom', 0, 20 );
	}

	public static function getImageNameOriginalExt( string $productId, $disk ) {
		$posiibleImageExtnsions = config( 'images.possibleExtensions' );
		foreach ( $posiibleImageExtnsions as $extnsion ) {
			if ( Storage::disk( $disk )->exists( $productId . '.' . $extnsion ) ) {
				return $productId . '.' . $extnsion;
			}
		}

		return false;
	}
}