<?php

namespace App\Models;

use App\Nest\Images\ImageService;
use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use CrudTrait;
    use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'products';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['name','description','aging_type','language','translit','sommeliers_note','page_title','page_description'];
    public const ID_PREFIX = 'i';
    public const CLIENT_FILE_ID='i';
    public const LINKED_PRODUCTS='@';
    public const ALCOHOL_TYPE='alc';
    public const ACCESSORIES_TYPE='acs';
    public const BOOK_TYPE='book';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getItems($dataUpdateTime = null, array $ids = [], bool $isFullInfo = false)
    {
        $itemSelection = new ItemsSelection();
        return $itemSelection->getProducts(new Product(),$dataUpdateTime,$ids,$isFullInfo);
    }


   /* public function getPopularProductsIdsArr(int $qty)
    {
        $productsIdArr = Product::where('show_on_site',true)->orderBy('popularity', 'desc')->get(['id'])->take($qty);
        return $productsIdArr->map(function ($item) {
            return  $item->id;
        });
    }*/

    /*public function sortItemsByPopularity($items){
        $items = $items->sortByDesc(['pop']);
        return $items;

    }*/


    public function turnOffProductNoImage()
    {
        $rawProductsId = Product::where('show_on_site', true)->get('id');
        $images = collect(Storage::disk('winelibrary')->allFiles());
        $imagesNames = $images->map(function ($value) {
            $dotNumber = strpos($value, '.');
            return strtolower(substr($value, 0, $dotNumber));
        });
        $imagesNames = $imagesNames->unique()->toArray();
        foreach ($rawProductsId as  $item) {
            $isExistImage = array_search(Product::ID_PREFIX.$item->id, $imagesNames);
            if ($isExistImage === false) {
                Product::where('id', $item->id)->update(['show_on_site' => false, 'show_in_google_merchant' => false, 'show_in_search_io' => false, 'show_in_allo' => false]);
            }

        }
    }

    public function addConnectionsByVolums($product){
        $connectedProds=$product->volums;
        foreach ($connectedProds as $item){
            $connectedProds =$connectedProds->merge($item->volums);
        }

        $connectedProds =$connectedProds->push($product);
        $connectedProds=$connectedProds->unique();
        $connectedByIds=$connectedProds->map(function ($item){
            return $item->id;
        });

        foreach ($connectedProds as $item){
            $idsToInsert=$connectedByIds->map(function ($itemId)use($item){
                if ($itemId!==$item->id){
                    return $itemId ;
                }
            });
            $idsToInsert=$idsToInsert->whereNotNull();
            foreach ($idsToInsert as $idToInsert){
                $itemIds=$item->volums->map(function ($item){
                    return $item->id ;
                });
                $itemIds=$itemIds->toArray();
                if (!in_array($idToInsert,$itemIds)){
                    $item->volums()->attach($idToInsert);
                }
            }
        }
    }

    /*public function checkIfActive($id){
        return Product::where('id',$id)->first('show_on_site')->show_on_site ;

    }*/
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }


    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function alcohol_style()
    {
        return $this->belongsTo(Alcohol_style::class);
    }


    public function sommelier()
    {
        return $this->belongsTo(Sommelier::class);
    }

    public function marking()
    {
        return $this->belongsTo(Marking::class);
    }

    public function pack()
    {
        return $this->belongsTo(Pack::class);
    }

    public function wine_type()
    {
        return $this->belongsTo(Wine_type::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_product');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'product_tag');
    }
	public function signs()
	{
		return $this->belongsToMany(Sign::class, 'product_sign');
	}

    public function combinations()
    {
        return $this->belongsToMany(Combination::class, 'combination_product');
    }

    public function grape_sorts()
    {
        return $this->belongsToMany(Grape_sort::class, 'grape_sort_product');
    }

    public function sugar_contents()
    {
        return $this->belongsToMany(Sugar_content::class, 'product_sugar_content');
    }

    public function accessories()
    {
        return $this->belongsToMany(Product::class,'product_product','product_acs_id');
    }
    public function analogs()
    {
        return $this->belongsToMany(Product::class,'analogs_pivot','product_anlg_id');
    }

    public function volums()
    {
        return $this->belongsToMany(Product::class,'volums_connections_pivot','product_vol_id');
    }

    public function publisher(){
        return $this->belongsTo(Publisher::class);
    }

    public function authors(){
        return $this->belongsToMany(Author::class,'author_product');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeTurnedOf($query)
    {
        return $query->where('show_on_site', 0);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $imagesProps=config('filesystems.disks.winelibrary.product');
        $waterMarkProps=config('filesystems.disks.winelibrary.watermark');
        $this->attributes[$attribute_name] =  $this->setImage($attribute_name,$imagesProps,$value,'',$waterMarkProps);
    }


    private function setImage($attribute_name, $imagesProps, $file, $fileNameSuffix,$waterMarkProps)
    {
        $imageService = new ImageService();
        $id = $this->id;
        if ($id === null) {//если это добавление нового объекта, а не редактирование - нужно взять в таблице значение слудующего автоинкремента
            $statement = DB::select("SHOW TABLE STATUS LIKE '" . $this->table . "'");
            $nextId = $statement[0]->Auto_increment;
            $id = $nextId;
        }
        $newName = $this::ID_PREFIX . $id . $fileNameSuffix;
        if ($file == null) {
            $this->attributes[$attribute_name] = null;
            $imageService->removeImageIfExists($newName, $imagesProps);
            return;
        }
        if (!filter_var($file, FILTER_VALIDATE_URL)) {// если к нам зашла не картинка а url (так как файл был сохранен ранее то мы ничего не делаем, а если картинка - то заходим в функцию и обрабатываем )
            return $imageService->uploadFileToDisk($file, $attribute_name, $newName, $imagesProps,$waterMarkProps);
        }
    }
}
