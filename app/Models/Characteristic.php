<?php

namespace App\Models;

use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

class Characteristic extends Model
{
    use CrudTrait;
    use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'characteristics';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['name','suffix'];
	public const ID_PREFIX = 'ch';
	public const CLIENT_FILE_ID='chars';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getItems( $dataUpdateTime = null, array $ids = [], bool $isFullInfo = false)
    {
        /*$itemSelection = new ItemsSelection();
        return $itemSelection->getItems(new Characteristic(), $dataUpdateTime, $ids, $isFullInfo);*/
        //"ch10":{"I":"ch10","N":"Стиль алкоголя","V":"id","CN":"as"}
        $chars=Characteristic::all();
        $preparedItems=[];
        foreach ($chars as $char){
        	$preparedItem=[];
        	$preparedItem['I']=$char['code_name'];
        	$preparedItem['N']=$char['name'];
	        $preparedItem['V']=$char['type'];
	        if ($char['suffix']!==null && !empty($char['suffix'])){
		        $preparedItem['J']=$char['suffix'];
	        }


	        $preparedItems[$char['code_name'] ] = $preparedItem;

        }
        return $preparedItems;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
	public function chars_category()
	{
		return $this->belongsTo(Chars_category::class);
	}
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
