<?php

namespace App\Models;

use App\Nest\Images\ImageService;
use App\Nest\ItemsSelection;
use Illuminate\Support\Facades\DB;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;


class Country extends Model
{
    use CrudTrait;
    use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'countries';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['name','description','page_title','page_description'];
    public const ID_PREFIX = 'ctry';
    public const CLIENT_FILE_ID='ctry';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getItems($dataUpdateTime = null, array $ids = [], bool $isFullInfo = false)
    {
        $itemSelection= new ItemsSelection();
        return $itemSelection->getItems(new Country(), $dataUpdateTime, $ids, $isFullInfo);
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setFlagAttribute($value)
    {
        $attribute_name = "flag";
        $imagesProps=config('filesystems.disks.winelibrary.flag');
        $imageService = new ImageService();
        $id=$this->id;
        if ($id===null){//если это добавление нового объекта, а не редактирование - нужно взять в таблице значение слудующего автоинкремента
            $statement = DB::select("SHOW TABLE STATUS LIKE '".$this->table."'");
            $nextId = $statement[0]->Auto_increment;
            $id=$nextId;
        }
        $newName=$this::ID_PREFIX.$id.'_flag';

        if ($value==null) {
            $this->attributes[$attribute_name] = null;
            $imageService->removeImageIfExists($newName,$imagesProps);
            return;
        }
        if (!filter_var($value, FILTER_VALIDATE_URL)) {// если к нам зашла не картинка а url (так как файл был сохранен ранее то мы ничего не делаем, а если картинка - то заходим в функцию и обрабатываем )

            $this->attributes[$attribute_name] = $imageService->uploadFileToDisk($value, $attribute_name, $newName, $imagesProps);
        }
    }
}
