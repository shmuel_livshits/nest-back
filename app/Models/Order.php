<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'orders';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
		public static function getOrdersIdsOfUser(User $user){
			$ordersRaw=Order::where('user_id',$user['id'])->get(['products']);
			if (!$ordersRaw->isEmpty()){
				$productsIds=[];
				foreach ($ordersRaw as $order){
					foreach (json_decode($order['products']) as $parsedOrder){
						array_push($productsIds,$parsedOrder->id);
					}
				}
				return array_values(array_unique($productsIds));
			}
			return [];
		}

		public static function getUsersOrders(User $user,int $skip,int $take){
			$ordersRaw=Order::where('user_id',$user['id'])->skip($skip)->take($take)->get();
			$orders=[];
			if (!$ordersRaw->isEmpty()){
				foreach ($ordersRaw as $orderRaw){
					$orderProducts=collect(json_decode($orderRaw['products'],true));
					$order=[];
					$order['date']=$orderRaw['updated_at']->timestamp;
					$order['total']=$orderRaw['total'];
					$order['cart']=$orderProducts->map(function($item){
						return [
							"I"=>$item['id'],
							"Q"=>$item['qty']
						];
					})->toArray();
					array_push($orders,$order);
				}
				return array_values($orders);
			}
			return [];
		}
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function user(){
        return $this->belongsTo(User::class);
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
