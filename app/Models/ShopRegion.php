<?php

namespace App\Models;

use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

class ShopRegion extends Model
{
    use CrudTrait;
	use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'shop_regions';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
	protected $translatable = ['name','page_title','page_description'];
	public const ID_PREFIX='r';
	public const CLIENT_FILE_ID='R';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
	/**
	 * @param null $dataUpdateTime
	 *
	 * @return array
	 * Эта функция не связана с классом ItemSelection потому что она будет работать крайне редко и я не хочу услажнять из-за нее общую функцию
	 */
	public static function getItems($dataUpdateTime = null)
	{
		$shopRegions=ShopRegion::recentlyUpdated($dataUpdateTime)->popularCities()->orderBy('name')->get();
		$preparedItems=[];
		foreach ($shopRegions as $shop_region){
			$preparedItem=[];
			$preparedItem['I']=ShopRegion::ID_PREFIX. $shop_region['parent_id'];
			$preparedItem['N']=$shop_region['name'];
			$preparedItem['S']=$shop_region['page_slug'];
			$preparedItem['U']=$shop_region['city_type'];
			if($shop_region['parent_id']){
				$preparedItem['P']=ShopRegion::ID_PREFIX. $shop_region['parent_id'];
			}
			$preparedItems[ ShopRegion::ID_PREFIX . $shop_region['id'] ] = $preparedItem;
		}
		return $preparedItems ;
	}
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
	public function parent()
	{
		return $this->belongsTo(ShopRegion::class, 'parent_id');
	}

	public function children()
	{
		return $this->hasMany(ShopRegion::class, 'parent_id');
	}
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
	public function scopeFirstLevelItems($query)
	{
		return $query->where('depth', '1')
		             ->orWhere('depth', null)
		             ->orderBy('lft', 'ASC');
	}

	public function scopeRecentlyUpdated($query,$dataUpdateTime){
		if($dataUpdateTime!==null){
			return $query->where('updated_at', '>=', $dataUpdateTime);
		}

	}

	public function scopePopularCities($query){
		return $query->where('city_type','>=',1);
	}
    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
