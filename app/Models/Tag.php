<?php

namespace App\Models;

use App\Nest\Images\ImageService;
use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    use CrudTrait;
    use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'tags';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['name', 'description','page_title','page_description'];
    public const ID_PREFIX='h';
    public const CLIENT_FILE_ID='#';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getItems( $dataUpdateTime=null, array $ids=[], bool $isFullInfo = false){
        $itemSelection = new ItemsSelection();
        return $itemSelection->getItems(new Tag(),$dataUpdateTime,$ids,$isFullInfo);
    }

    public static function prepareIdsForClient(array $tags){
        $preparedIds=[];
        foreach ($tags as $tag){
            $preparedId=Tag::ID_PREFIX.$tag->id;
            array_push($preparedIds,$preparedId);
        }
        return ($preparedIds===[])?null:$preparedIds;

    }

	public function getClass()
	{
		return $this->table;
	}
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setCoverAttribute($value){

        $attribute_name = "cover";
        $imagesProps=config('filesystems.disks.winelibrary.brand_cover');
        $this->attributes[$attribute_name]=$this->setImage($attribute_name,$imagesProps,$value,'_cover');
    }

    private function setImage($attribute_name, $imagesProps, $file, $fileNameSuffix)
    {
        $imageService = new ImageService();
        $id = $this->id;
        if ($id === null) {//если это добавление нового объекта, а не редактирование - нужно взять в таблице значение слудующего автоинкремента
            $statement = DB::select("SHOW TABLE STATUS LIKE '" . $this->table . "'");
            $nextId = $statement[0]->Auto_increment;
            $id = $nextId;
        }
        $newName = $this::ID_PREFIX . $id . $fileNameSuffix;
        if ($file == null) {
            $this->attributes[$attribute_name] = null;
            $imageService->removeImageIfExists($newName, $imagesProps);
            return;
        }
        if (!filter_var($file, FILTER_VALIDATE_URL)) {// если к нам зашла не картинка а url (так как файл был сохранен ранее то мы ничего не делаем, а если картинка - то заходим в функцию и обрабатываем )
            return $imageService->uploadFileToDisk($file, $attribute_name, $newName, $imagesProps);
        }
    }
}
