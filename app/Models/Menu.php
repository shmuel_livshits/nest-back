<?php

namespace App\Models;

use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

class Menu extends Model
{
    use CrudTrait;
	use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'menus';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
	protected $translatable = ['name'];
	public const ID_PREFIX = 'm';
	public const CLIENT_FILE_ID='menu';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
	public static function getItems($dataUpdateTime = null, array $ids = [], bool $isFullInfo = false)
	{
		$itemSelection = new ItemsSelection();
		$categories = $itemSelection->getItems(new Menu(), $dataUpdateTime, $ids, $isFullInfo);
		$categories=collect($categories) ->sortBy('U');
		$categories=$categories->where('vis',1);
		return $categories->toArray() ;
	}
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
	public function parent()
	{
		return $this->belongsTo(Menu::class, 'parent_id');
	}

	public function children()
	{
		return $this->hasMany(Menu::class, 'parent_id');
	}
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
	public function scopeFirstLevelItems($query)
	{
		return $query->where('depth', '1')
		             ->orWhere('depth', null)
		             ->orderBy('lft', 'ASC');
	}
    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
