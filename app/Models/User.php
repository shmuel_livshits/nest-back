<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable {
	use HasFactory, Notifiable, HasApiTokens;
	use CrudTrait;
	use HasRoles;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
		'created_at',
		'updated_at'
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	public function prepareForOutDoor(  ) {
		$prepUser        = [];
		$prepUser['email'] = $this['email'];
		$prepUser['fname'] = $this['name'];
		$prepUser['lname'] = $this['last_name'];
		$prepUser['phone'] = $this['phone'];
		$prepUser['news'] = $this['news_subscribe'];
		$prepUser['shippings']=( $this['shippings']===null )? [] :json_decode($this['shippings']);
		$prepUser['payments']=($this['payments']===null)? [] :json_decode($this ['payments']);
		return $prepUser;
	}

	public function updateUserInfo( array $updatedUserInfo ) {

		$this['name']=$updatedUserInfo['fname'];
		$this['last_name']=$updatedUserInfo['lname'];
		$this['phone']=$updatedUserInfo['phone'];
		$this['news_subscribe']=$updatedUserInfo['news'];
		$this['shippings']=$updatedUserInfo['shippings'];
		$this['payments']=$updatedUserInfo['payments'];
		return $this->save();

	}

	public function updateSessionData(array $sessionDataInfo){
		$this['cart']=$sessionDataInfo['cart'];
		$this['favorites']=$sessionDataInfo['favorites'];
		$this['viewed']=$sessionDataInfo['viewed'];
		return $this->save();
	}

	public function getSessionData(){
		$sessionData=[];
		$sessionData['cart']=json_decode($this['cart']);
		$sessionData['favorites']=json_decode($this['favorites']);
		$sessionData['viewed']=json_decode($this['viewed']);
		$sessionData['ordered']=Order::getOrdersIdsOfUser($this);
		return $sessionData;
	}


	public function auth_providers() {
		return $this->hasMany( AuthProvider::class, 'user_id', 'id' );
	}
}
