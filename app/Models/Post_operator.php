<?php

namespace App\Models;

use App\Nest\Images\ImageService;
use App\Nest\ItemsSelection;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Support\Facades\DB;

class Post_operator extends Model
{
    use CrudTrait;
    use HasTranslations;


    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'post_operators';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $translatable = ['name','message','free_message','not_free_message','message_later',];
    public const ID_PREFIX='post';
    public const CLIENT_FILE_ID='post';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function getItems($dataUpdateTime=null)
    {
        $itemSelection = new ItemsSelection();
        $postOperators= $itemSelection->getItems(new Post_operator(),$dataUpdateTime,[],false);
        $postOperators=collect($postOperators) ->sortBy('lft');
        return $postOperators->toArray();
    }

    public static function getNameFromClientId($clientId){
        $id = (int)filter_var($clientId, FILTER_SANITIZE_NUMBER_INT);
        $operator=Post_operator::where('id',$id)->first();
        if ($operator!==null){
            return $operator->name;
        }
        return null;

    }

    public static function getPickUpPlaces($cityName){
        $city=Main_delivery_city::where('name->'.app()->getLocale(),$cityName)->first();
        if ($city!==null){
            $shops=Shop::where('main_delivery_city_id',$city->id)->get();
            $preparedShops=collect([]);
            if ($shops!==null){
                foreach ($shops as $shop){
                    $preparedShop=collect([]);
                    $preparedShop['name']=$shop['name'];
                    $preparedShops->push($preparedShop);
                }
                return $preparedShops;
            }
        }
        return 0;
    }
    public static function isDeliveryWorks($cityName){
        $city= Main_delivery_city::where('name->'.app()->getLocale(),$cityName)->first();
        if ($city!==null){
            $shop= Shop::where('main_delivery_city_id',$city->id)->first();
            if ($shop!==null){
                return true;
            }
        }
       return 0;
    }

    public static function isPickupWorks($cityName){
    		$mainCity=Main_delivery_city::where('name->'.app()->getLocale(),$cityName)->first();
    		$shop=Shop::where('main_delivery_city_id',$mainCity->id)->first();
    		if ($shop!==null){
			    return [['number'=>'','name'=>$shop->address]];
		    }
		    return 0;
    }

	public static function isIpostWorksInCity($cityName){
		$city= IpostCity::where('name->'.app()->getLocale(),$cityName)->first();
		if ($city!==null){
			if ($city!==null){
				return true;
			}
		}
		return 0;
	}
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeFirstLevelItems($query)
    {
        return $query->where('depth', '1')
            ->orWhere('depth', null)
            ->orderBy('lft', 'ASC');
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setLogoAttribute($value)
    {
        $attribute_name = "logo";
        $imagesProps=config('filesystems.disks.winelibrary.post_logo');
        $imageService = new ImageService();
        $id=$this->id;
        if ($id===null){//если это добавление нового объекта, а не редактирование - нужно взять в таблице значение слудующего автоинкремента
            $statement = DB::select("SHOW TABLE STATUS LIKE '".$this->table."'");
            $nextId = $statement[0]->Auto_increment;
            $id=$nextId;
        }
        $newName=$this::ID_PREFIX.$id.'_logo';
        if ($value==null) {
            $this->attributes[$attribute_name] = null;
            $imageService->removeImageIfExists($newName,$imagesProps);
            return;
        }
        if (!filter_var($value, FILTER_VALIDATE_URL)) {// если к нам зашла не картинка а url (так как файл был сохранен ранее то мы ничего не делаем, а если картинка - то заходим в функцию и обрабатываем )
            $this->attributes[$attribute_name] = $imageService->uploadFileToDisk($value, $attribute_name, $newName, $imagesProps);
        }
    }
}
