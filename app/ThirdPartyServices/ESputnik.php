<?php
/**
 * Created by PhpStorm.
 * User: shmue
 * Date: 07.02.2021
 * Time: 20:33
 */

namespace App\ThirdPartyServices;


use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ESputnik {
	private $eSputnikUser;
	private $eSputnikPassword;
	private const URl = 'https://esputnik.com/api/v1';


	public function __construct() {
		$this->eSputnikUser     = env( 'ESPUTNIK_USER' );
		$this->eSputnikPassword = env( 'ESPUTNIK_PASSWORD' );
	}


	public function smartSendSms( $orderId, $phone, $messageId, $ttn ) {
		$smartSendUrl = '/message/' . $messageId . '/smartsend';
		$jsonParam    = [
			'orderId' => strval( $orderId ),
			'ttn'     => $ttn
		];
		$esputnikFullInfo   = [
			'email'      => false,
			"fromName"   => "WineLibrary",
			'recipients' => [
				'jsonParam' => json_encode($jsonParam),// Необходимо отдельно encode этот параметр для того чтоб он был стрингой в json объекте. Это требует система esputnik
				"locator"   => $phone,
			],
		];
		return $this->sendSmartSendRequest( $smartSendUrl, $esputnikFullInfo );
	}

	public function smartSendEmail(int $orderId, $messageId,string $firstName, string $lastName, string $email,int $total,$products,string $shippingProviderName,
		string $shippingDestination, string $city){
		$smartSendUrl = '/message/' . $messageId . '/smartsend';
		$jsonParam    = [
			'orderId' => strval( $orderId ),
			'firstName'=>$firstName,
			'lastName'=>$lastName,
			'total'=>strval($total),
			'orderDate'=>Carbon::now()->format('d.m.Y'),
			'products'=>$products,
			'shippingProvider'=>$shippingProviderName,
			'shippingDestination'=>$shippingDestination,
			'city'=>$city,
		];
		$esputnikFullInfo   = [
			'email'      => true,
			"fromName"   => "WineLibrary",
			'recipients' => [
				'jsonParam' => json_encode($jsonParam),// Необходимо отдельно encode этот параметр для того чтоб он был стрингой в json объекте. Это требует система esputnik
				"locator"   => $email,
			],
		];
//		return $esputnikFullInfo;
		return $this->sendSmartSendRequest( $smartSendUrl, $esputnikFullInfo );
	}

	/**
	 * @param string $event
	 * @param string $crmStatusText
	 * Нужно понять какой статус у заказа, рабочее или нерабочее время и прислать нужную смс
	 * И также имеет значение если это Почта или курьер и самовывоз
	 *
	 * @return mixed
	 */
	public function getTextSms( string $event, string $crmStatusText, $salesDriveResponce ) {
		$salesDrive = new SalesDrive();
		$ttn=$salesDrive->getDeliveryNumber($salesDriveResponce);
		if ( $event === 'new_order' ) {
			if ($ttn===null){
				$esputnikMessageId = env( 'ESPUTNIK_NEW_ORDER_CURIER' );
			}
			else{
				if ( $this->isWorkingHours() ) {
					$esputnikMessageId = env( 'ESPUTNIK_NEW_ORDER_MESSAGE_ID' );
				} else {
					$esputnikMessageId = env( 'ESPUTNIK_NEW_ORDER_NW_Hours_MSG_ID' );
				}
			}

		} elseif ( $event === 'status_change' && $crmStatusText === 'Отправлен' && $ttn!==null ) {
			$esputnikMessageId = env( 'ESPUTNIK_ORDER_STATUS_SENT_MESSAGE_ID' );
		}

		return $esputnikMessageId;
	}

	private function sendSmartSendRequest( $url, $json_value = [] ) {
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $json_value ) );
		curl_setopt( $ch, CURLOPT_HEADER, 1 );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Accept: application/json', 'Content-Type: application/json' ) );
		curl_setopt( $ch, CURLOPT_URL, $this::URl . $url );
		curl_setopt( $ch, CURLOPT_USERPWD, $this->eSputnikUser . ':' . $this->eSputnikPassword );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_SSLVERSION, 6 );
		$output = curl_exec( $ch );
		curl_close( $ch );
		return $output;
	}

	/**
	 * @return bool
	 * Нужно понять если рабочее или нерабочее время. В будущем функцию необходимо соединить с украинским календарем, но пока она
	 * проверяет если сейчас меньше чем 17:00 и не воскресенье
	 */
	private function isWorkingHours() {
		$dateTimeNow = Carbon::now( 'Europe/Kiev' );
		$hours       = intval( $dateTimeNow->format( "H" ) );
		if ( $dateTimeNow->dayOfWeek !== 7 && $hours < 17 ) {
			return true;
		}

		return false;
	}

}