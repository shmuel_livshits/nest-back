<?php
/**
 * Created by PhpStorm.
 * User: shmue
 * Date: 07.02.2021
 * Time: 20:17
 */

namespace App\ThirdPartyServices;


use App\Models\Country;
use App\Models\Product;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\Log;

class SalesDrive
{
    private $api_key;
    private $salesDriveUrl;
    public function __construct()
    {
        $this->api_key=env('SALES_DRIVE_API_KEY');
        $this->salesDriveUrl=env('SALES_DRIVE_URL');

    }

    public function createOrder($products, $phone, $firstName='',$lastName='',$email='', $shippingMethod='', $shippingAddress='', $city='',$paymentMethod='',$externalId, $comment='', $buyingType=false,
	    $utm_source='',$utm_campaign='',$utm_medium='',$utm_content='',$utm_term='')
    {
        $_salesdrive_values = [
            "form" => $this->api_key,
            "products" => $products, //Товары/Услуги
            "comment" => $comment, // Комментарий
            "fName" => $firstName, // Имя
            "lName" => $lastName, // Фамилия
            "mName" => "", // Отчество
            "phone" => $phone, // Телефон
            "email" => $email, // Email
            "shipping_address" => $city.', '. $shippingAddress, // Адрес доставки
            "shipping_method" => $shippingMethod, // Способ доставки
            "payment_method" => $paymentMethod, // Способ оплаты
            "externalId"=>$externalId,
	        "prodex24source"=>$utm_source,
	        "prodex24campaign"=>$utm_campaign,
	        "prodex24medium"=>$utm_medium,
	        "prodex24content"=>$utm_content,
	        "prodex24term"=>$utm_term,
	        "istocnik"=>($buyingType===true)?'1 клик':'Прямой'
            /* "novaposhta"=> [
                 "ServiceType" => "", // возможные значения: DoorsDoors, DoorsWarehouse, WarehouseWarehouse, WarehouseDoors
                 "area" => "", // область на русском или украинском языке, или Ref области в системе Новой почты
                 "city" => "", // название города на русском или украинском языке, или Ref города в системе Новой почты
                 "cityNameFormat" => "", // возможные значения: full (по умолчанию), short
                 "WarehouseNumber" => "", // отделение Новой Почты в одном из форматов: номер, описание, Ref
                 "Street" => "", // название и тип улицы, или Ref улицы в системе Новой почты
                 "BuildingNumber" => "", // номер дома
                 "Flat" => "", // номер квартиры
                 "backwardDeliveryCargoType" => "" // возможные значения: None - без наложенного платежа, Money - с наложенным платежом
             ],*/
        ];

        $this->send($_salesdrive_values);
        return 'Ваша заявка успешно отправлена';
    }

    private function send($_salesdrive_values){
        $_salesdrive_url =$this->salesDriveUrl ;
        $_salesdrive_ch = curl_init();
        curl_setopt($_salesdrive_ch, CURLOPT_URL, $_salesdrive_url);
        curl_setopt($_salesdrive_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($_salesdrive_ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($_salesdrive_ch, CURLOPT_SAFE_UPLOAD, true);
        curl_setopt($_salesdrive_ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($_salesdrive_ch, CURLOPT_POST, 1);
        curl_setopt($_salesdrive_ch, CURLOPT_POSTFIELDS, json_encode($_salesdrive_values));
        curl_setopt($_salesdrive_ch, CURLOPT_TIMEOUT, 10);

        $_salesdrive_res = curl_exec($_salesdrive_ch);
        $_salesdriveerrno = curl_errno($_salesdrive_ch);
        $_salesdrive_error = 0;
        if ($_salesdriveerrno or $_salesdrive_res != "") {
            $_salesdrive_error = 1;
        }
    }

    public function prepareProductsToSalesDrive($products){
        $preparedProducts=[];
        foreach ($products as $rawProduct){
            $product=Product::where('sku',$rawProduct['sku'])->first();
            if ($product!==null){
                $preparedProduct= new \stdClass();
                $preparedProduct->id=$rawProduct['sku'];
                $preparedProduct->name=$product['name'];
                $preparedProduct->costPerItem=intval($product['price']);
                $preparedProduct->amount=$rawProduct['quantity'];
                $preparedProduct->sku=$rawProduct['sku'];
                $preparedProduct->categoryName=$rawProduct['category'];
                $preparedProduct->quantity=strval($rawProduct['quantity']); // если сделать числом в esputnik прибавляет ноль
	            $preparedProduct->image=env('APP_URL').$product['image'];
	            Log::info($preparedProduct->image);
                $preparedProduct->country=Country::where('id',$product['country_id'])->first()->name;
                array_push($preparedProducts,$preparedProduct);
            }
        }
        return $preparedProducts;
    }

	public function getDeliveryNumber($salesDriveResponce){
		if (isset($salesDriveResponce['data']['ord_novaposhta'])){
			return $salesDriveResponce['data']['ord_novaposhta']['EN'];
		}
		elseif (isset($salesDriveResponce['data']['ord_justin'])){
			return $salesDriveResponce['data']['ord_justin']['ttn'] ;
		}
		return null;

	}

	public function prepareProductsEmail($products){
    	$preparedProducts=[];
    	foreach ($products as $product){
    		$preparedProduct=[];

    		array_push($preparedProducts,$preparedProduct);
	    }

	    return $preparedProducts;
	}

}