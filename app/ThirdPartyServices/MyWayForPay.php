<?php

namespace App\ThirdPartyServices;

use WayForPay\SDK\Collection\ProductCollection;
use WayForPay\SDK\Credential\AccountSecretCredential;
use WayForPay\SDK\Domain\Card;
use WayForPay\SDK\Domain\CardToken;
use WayForPay\SDK\Domain\Client;
use WayForPay\SDK\Domain\Product;
use WayForPay\SDK\Exception\ApiException;
use WayForPay\SDK\Wizard\ChargeWizard;


class MyWayForPay
{
    private $credentials;
    public function __construct()
    {
        $this->credentials = new AccountSecretCredential(env('WFP_ACCOUNT'),env('WFP_SEKRET') );
    }

    public function pay(string $firstName, string $lastName, string $email, string $phone, $cardNumber,$cardMonth, $cardYear, $cardCvv, $cardHolderName,$sum,$orderId)
    {
        try {
            $response = ChargeWizard::get($this->credentials)
                ->setOrderReference(sha1(microtime(true)))
                ->setAmount($sum)
                ->setCurrency('UAH')
                ->setOrderDate(new \DateTime())
                ->setMerchantDomainName('https://winelibrary.com.ua/')
                ->setClient(new Client(
                    $firstName,
                    $lastName,
                    $email,
                    $phone,
                    'Ukraine'
                ))
                ->setProducts(new ProductCollection(array(
                    new Product(strval($orderId), $sum, 1)
                )))
                ->setCard(new Card($cardNumber, $cardMonth, $cardYear, $cardCvv, $cardHolderName))
                //->setCardToken(new CardToken('1aa11aaa-1111-11aa-a1a1-0000a00a00aa'))
                ->getRequest()
                ->send();

            dd(  $response->getTransaction() );
        } catch (ApiException $e) {
            dd("Exception: {$e->getMessage()}\n");
        }
    }

}