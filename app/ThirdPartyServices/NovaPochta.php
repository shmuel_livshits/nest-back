<?php
/**
 * Created by PhpStorm.
 * User: shmue
 * Date: 23.01.2021
 * Time: 18:35
 */

namespace App\ThirdPartyServices;


use App\Models\ShopRegion;
use App\Nest\Service\Slug;
use Illuminate\Support\Facades\Storage;
use LisDev\Delivery\NovaPoshtaApi2;

class NovaPochta {
	private const API_KEY = '26565db00a95950e1714246393fd367e';
	private $lang = '';
	private $npObj;
	private $filePath;

	public function __construct( string $lang = 'ru' ) {
		$this->lang     = ( $lang === 'ua' ) ? 'ua' : 'ru';
		$this->npObj    = new NovaPoshtaApi2( NovaPochta::API_KEY, $lang, true );
		$this->filePath = 'ClientFiles/' . $this->lang . '/citiesList.json';
	}

	public function getCitiesList() {
		if ( Storage::exists( $this->filePath ) ) {
			return Storage::get( $this->filePath );
		}
		$citiesRaw  = $this->npObj->getCities( 0 )['data'];
		$citiesPrep = collect( [] );
		foreach ( $citiesRaw as $cityRaw ) {
			$cityPrep         = collect( [] );
			$cityPrep['ref']  = $cityRaw['Ref'];
			$cityPrep['name'] = trim( ( $this->lang === 'ua' ) ? $cityRaw['Description'] : $cityRaw['DescriptionRu'] );
			$cityPrep['name'] = iconv( mb_detect_encoding( $cityPrep['name'], mb_detect_order(), true ), "UTF-8", $cityPrep['name'] );
			if ( trim( $cityPrep['name'] ) !== '' ) {
				$citiesPrep->add( $cityPrep );
			}
		}
		Storage::put( $this->filePath, $citiesPrep->toJson() );

		return Storage::get( $this->filePath );
	}

	public function getCity( string $cityStr ) {
		return $this->npObj->getCity( $cityStr );
	}

	public function getWarehouses( string $name ) {
		$citiesList = json_decode( $this->getCitiesList() );
		foreach ( $citiesList as $item ) {
			if ( trim( mb_strtolower( $item->name ) ) == trim( mb_strtolower( $name ) ) ) {
				return $this->findWarehousesByCityRef( $item->ref );
			}
		}
	}

	public function getNovaPoshtaCourier( string $name ) {
		$citiesList = json_decode( $this->getCitiesList() );
		foreach ( $citiesList as $item ) {
			if ( trim( mb_strtolower( $item->name ) ) == trim( mb_strtolower( $name ) ) ) {
				return true;
			}
		}

		return 0;
	}

	public function getCitiesNames() {
		$citiesFullList = $this->getCitiesList();
		$citiesFullList = json_decode( $citiesFullList );
		$namesList      = collect( [] );
		foreach ( $citiesFullList as $city ) {
			$namesList->add( $city->name );
		}

		return $namesList->toArray();
	}

	public function findWarehousesByCityRef( string $cityRef ) {

		$rawWarehouses  = $this->npObj->getWarehouses( $cityRef )['data'];
		$warehousesPrep = collect( [] );
		foreach ( $rawWarehouses as $warehouseRaw ) {
			$warehousePrep           = collect( [] );
			$warehousePrep['number'] = $warehouseRaw['Number'];
			$warehousePrep['name']   = ( $this->lang === 'ua' ) ? $warehouseRaw['Description'] : $warehouseRaw['DescriptionRu'];
			$warehousesPrep->add( $warehousePrep );
		}

		return $warehousesPrep->toJson();

	}

	public function saveRegionsAndCitiesFromNpDb() {
		$regions   = collect( $this->npObj->getAreas()['data'] );
		$citiesRaw = collect( $this->npObj->getCities( 0 )['data'] );
		foreach ( $regions as $key => $region ) {
			$shopRegion             = new ShopRegion();
			$shopRegion->page_slug  = Slug::translitAndSlugify( $region['Description'] );
			$shopRegion->name       = [ 'ru' => $region['DescriptionRu'], 'ua' => $region['Description'] ];
			$shopRegion->page_title = [ 'ru' => $region['DescriptionRu'], 'ua' => $region['Description'] ];
			$shopRegion->save();


			$citiesReg = $citiesRaw->filter( function ( $value, $key ) use ( $region ) {
				if ( $value['Area'] === $region['Ref'] ) {
					return $value;
				}
			} );
			foreach ( $citiesReg as $cityReg ) {
				$shopRegionCity             = new ShopRegion();
				$shopRegionCity->page_slug  = Slug::translitAndSlugify( $cityReg['Description'] );
				$shopRegionCity->name       = [ 'ru' => $cityReg['DescriptionRu'], 'ua' => $cityReg['Description'] ];
				$shopRegionCity->page_title = [ 'ru' => $cityReg['DescriptionRu'], 'ua' => $cityReg['Description'] ];
				$shopRegionCity->parent_id  = $shopRegion->id;
				$shopRegionCity->save();
			}

		}

	}

}