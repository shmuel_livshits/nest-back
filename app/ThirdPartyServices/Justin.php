<?php
/**
 * Created by PhpStorm.
 * User: shmue
 * Date: 26.01.2021
 * Time: 10:36
 */

namespace App\ThirdPartyServices;


use Illuminate\Support\Facades\Storage;

class Justin
{
    private const API_KEY = '309eeef3155a4243a51a6933ca27f84a';
    private const LOGIN = 'TOV_BibliotekaVina';
    private const PASSWORD = 'yiUP@Oy@';
    private $lang = '';
    private $justinObj;
    private $filePath;

    public function __construct(string $lang = 'ru')
    {
        $this->lang = ($lang === 'ua') ? 'ua' : 'ru';
        $this->justinObj = new \Justin\Justin(strtoupper($lang));
        $this->justinObj->setKey($this::API_KEY);
        $this->justinObj->setLogin($this::LOGIN)->setPassword($this::PASSWORD);
        $this->filePath='ClientFiles/'.$this->lang.'/citiesListJustin.json';
    }

    private function getCities()
    {
        if (Storage::exists($this->filePath)){
            return json_decode(Storage::get($this->filePath));
        }
        $prepared = collect([]);
        $rawList = $this->justinObj->listCities()->getData();
        foreach ($rawList as $item){
            $itemPrep=collect([]);
            $itemPrep['ref']=$item['fields']['uuid'];
            $itemPrep['name']=mb_strtolower($item['fields']['descr']);
            $prepared->add($itemPrep);
        }
        $prepared=$prepared->sortBy('name');
        Storage::put($this->filePath,$prepared->toJson());
        return $prepared;
    }

    private function getCityRef(string $cityName){
        $citiesList=$this->getCities();
        foreach ($citiesList as $city){
            if ($city->name===$cityName){
                return $city->ref;
            }
        }
        return false;
    }

    public function getWarehouses($cityName){

        $cityName=$this->cleanCityName($cityName);
        $cityRef=$this->getCityRef($cityName);
        if ($cityRef===false){
            return $cityRef;
        }
        $rawList=$this->justinObj->listDepartmentsLang()->getData();
        $preparedList = collect([]);
        foreach ($rawList as $item){
            if ($item['fields']['city']['uuid']==$cityRef){
                $itemPrep=collect([]);
                $itemPrep['number']=$item['fields']['departNumber'];
                $itemPrep['name']=$item['fields']['street']['descr'].' '.$item['fields']['houseNumber'];
                $preparedList->add($itemPrep);
            }

        }
        $preparedList->sortBy('number');
        return $preparedList->toJson();
    }

    private function cleanCityName($cityName){
        $cityName=mb_strtolower($cityName);

        $bracket=mb_strpos($cityName,'(');
        if ($bracket!==false){
            $cityName=mb_substr($cityName,0,$bracket);
        }

        $cityName=trim($cityName);
        return $cityName;
    }

}