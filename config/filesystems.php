<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
        ],
	    'dbfiles'=>[
		    'driver' => 'local',
		    'root' => env('DB_FILES_PATH'),
	    ],
        'winelibrary'=>[
            'driver' => 'local',
            'root' => public_path('uploads/imgs'),
            'url' => env('APP_URL').'/products_images',
            'product'=>[
                'standart'=>[
                    'shortName'=>'',
                    'width'=>800,
                    'height'=>800,
                ],
                'preview'=>[
                    'shortName'=>'p_',
                    'width'=>250,
                    'height'=>250,
                ]
            ],
            'watermark'=>[
                'height'=>28,
                'width'=>0.125,
            ],
            'brand_logo'=>[
                'standart'=>[
                    'shortName'=>'',
                    'width'=>300,
                    'height'=>120,
                ]
            ],
            'brand_cover'=>[
                'standart'=>[
                    'shortName'=>'',
                    'width'=>1800,
                    'height'=>1800,
                    ]

            ],
            'sommilier_avatar'=>[
                'standart'=>[
                    'shortName'=>'',
                    'width'=>100,
                    'height'=>100,
                    ]

            ],
            'flag'=>[
                'standart'=>[
                    'shortName'=>'',
                    'width'=>1600,
                    'height'=>1600,
                    ]

            ],
            'post_logo'=>[
                'standart'=>[
                    'shortName'=>'',
                    'width'=>1800,
                    'height'=>1800,
                ]
            ],

        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
